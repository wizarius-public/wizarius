package com.wizarius.utils.utils;

import com.wizarius.utils.models.ApiKeys;
import org.junit.Test;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

/**
 * @author shyslav
 * Date: 1/7/19
 * Time: 2:01 PM
 */
public class LazyHmacSHA256Test {
    @Test
    public void generateKeys() throws NoSuchAlgorithmException {
        ApiKeys apiKeys = LazyHmacSHA256.generateKeys();
        assertNotNull(apiKeys);
        assertNotNull(apiKeys.getApiKey());
        assertNotNull(apiKeys.getSecretKey());
    }

    @Test
    public void verify() throws NoSuchAlgorithmException, InvalidKeyException {
        ApiKeys s = LazyHmacSHA256.generateKeys();
        String encode = LazyHmacSHA256.encode("Hello World", s.getSecretKey());
        assertTrue(LazyHmacSHA256.verify(s.getSecretKey(), "Hello World", encode));
    }

    @Test
    public void generateHmacKey() throws NoSuchAlgorithmException {
        String s = LazyHmacSHA256.generateHmacKey();
        assertNotNull(s);
    }

    @Test
    public void encode() throws NoSuchAlgorithmException, InvalidKeyException {
        ApiKeys s = LazyHmacSHA256.generateKeys();
        String encode = LazyHmacSHA256.encode("Hello World", s.getSecretKey());
        assertNotNull(encode);
    }
}