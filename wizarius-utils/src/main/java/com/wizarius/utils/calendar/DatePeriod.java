package com.wizarius.utils.calendar;

import com.wizarius.utils.utils.LazyCalendar;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vladyslav Shyshkin on 17.03.17.
 */
public class DatePeriod {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    private Date startPeriod;
    private Date endPeriod;
    private int count;

    public DatePeriod(Date startPeriod, Date endPeriod) {
        this.startPeriod = startPeriod;
        this.endPeriod = endPeriod;
        this.count = 0;
    }

    public Date getStartPeriod() {
        return startPeriod;
    }

    public int getStartPeriodTimestamp() {
        return (int) (startPeriod.getTime() / 1000);
    }

    public int getEndPeriodTimestamp() {
        return (int) (endPeriod.getTime() / 1000);
    }

    public Date getEndPeriod() {
        return endPeriod;
    }

    public int getCount() {
        return count;
    }

    /**
     * Increase count by 1
     */
    public void increaseCount() {
        this.count++;
    }

    /**
     * Check if date falls in period
     *
     * @param date date to check
     * @return true if falls
     */
    public boolean dateFallsInPeriod(Date date) {
        return LazyCalendar.dateFallsInPeriod(startPeriod, endPeriod, date);
    }

    /**
     * Check if date falls in period
     *
     * @param timestamp date to check
     * @return true if falls
     */
    public boolean dateFallsInPeriod(int timestamp) {
        return LazyCalendar.dateFallsInPeriod(getStartPeriodTimestamp(), getEndPeriodTimestamp(), timestamp);
    }

    /**
     * Get time period in String
     *
     * @return time period string
     */
    public String getTimePeriod() {
        return dateFormat.format(startPeriod) + " - " + dateFormat.format(endPeriod);
    }

    /**
     * Get start time in string format
     *
     * @return start time int string format
     */
    public String getStartTime() {
        return dateFormat.format(startPeriod);
    }

    /**
     * Get end time in string format
     *
     * @return end time int string format
     */
    public String getEndTime() {
        return dateFormat.format(endPeriod);
    }
}
