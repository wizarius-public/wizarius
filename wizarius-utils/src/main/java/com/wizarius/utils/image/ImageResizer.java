package com.wizarius.utils.image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidParameterException;

/**
 * @author Mr. IP.
 */
public class ImageResizer {


    /**
     * Create thumbnail from buffered image.
     *
     * @param width  Required width for thumbnail
     * @param height Required height for thumbnail
     * @param source Source image
     * @return Thumbnail image
     */
    public static BufferedImage resizeImage(int width, int height, BufferedImage source) {

        float koef = (float) source.getWidth() / (float) source.getHeight();
        BufferedImage img;

        int new_width = 0;
        int new_height = 0;

        if (height < 0 && width > 0) {
            new_width = width;
            new_height = Math.round((float) width / koef);
        } else if (height > 0 && width < 0) {
            new_width = Math.round((float) height * koef);
            new_height = height;
        } else if (height > 0 && width > 0) {
            //find biggest edge and
            if (source.getWidth() >= source.getHeight()) {
                new_width = width;
                new_height = Math.round((float) width / koef);
            } else {
                new_width = Math.round((float) height * koef);
                new_height = height;
            }
        } else {
            throw new InvalidParameterException("width or height must be greater then zero");
        }

        img = new BufferedImage(
                new_width,
                new_height,
                source.getType()
        );


        Graphics2D g2 = img.createGraphics();
        g2.drawImage(
                source,
                0,
                0,
                img.getWidth(),
                img.getHeight(),
                null
        );

        return img;
    }

    /**
     * Resize image to different size
     *
     * @param width        Required width for new image
     * @param height       Required height for new image
     * @param imageContent Source image in bytes
     * @return Resized image
     */
    public static byte[] resizeImage(int width, int height, byte[] imageContent, ImageFormat format) throws IOException {
        BufferedImage sourceImage = ImageIO.read(new ByteArrayInputStream(imageContent));
        if (sourceImage == null) {
            return null;
        }
        BufferedImage targetImage = resizeImage(width, height, sourceImage);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(imageContent.length / 4);
        if (format == ImageFormat.PNG) {
            ImageIO.write(targetImage, "PNG", baos);
        }
        if (format == ImageFormat.JPG) {
            ImageIO.write(targetImage, "JPG", baos);
        }
        if (format == ImageFormat.GIF) {
            ImageIO.write(targetImage, "GIF", baos);
        }

        return baos.toByteArray();
    }
}
