package com.wizarius.utils.image;


import com.wizarius.utils.utils.LazyMimeType;

/**
 * @author Mr. IP.
 */
public enum ImageFormat {
    GIF, PNG, JPG;


    /**
     * Get image format from mime type
     *
     * @param type mime type
     * @return image format
     */
    public static ImageFormat getFromMimeType(String type) {
        if (!LazyMimeType.isImage(type)) {
            return null;
        } else {
            switch (type) {
                case "image/jpeg":
                    return JPG;
                case "image/png":
                    return PNG;
                case "image/gif":
                    return GIF;
            }
        }
        return JPG;
    }

    /**
     * Format to string type
     *
     * @return String type
     */
    public String formatToStringType() {
        switch (this) {
            case GIF: {
                return "gif";
            }
            case JPG: {
                return "jpeg";
            }
            case PNG: {
                return "png";
            }
        }
        return null;
    }
}
