package com.wizarius.utils.crypto.rsa;

/**
 * Created by Vladyslav Shyshkin on 11.09.17.
 */
public class RSAAlgorithmException extends Exception {
    public RSAAlgorithmException(String message) {
        super(message);
    }

    public RSAAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }
}
