package com.wizarius.utils.crypto.rsa;

import lombok.extern.slf4j.Slf4j;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * Created by Vladyslav Shyshkin on 11.09.17.
 */
@Slf4j
public class RSAKeysPair {
    private final PublicKey publicKey;
    private final PrivateKey privateKey;

    public RSAKeysPair(PublicKey publicKey, PrivateKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    /**
     * Generate key pair by String values
     *
     * @param publicKey  public key string
     * @param privateKey private key string
     */
    public RSAKeysPair(String publicKey, String privateKey) throws RSAAlgorithmException {
        //generate public key
        this.publicKey = convertPublicKey(publicKey);
        //generate private key
        this.privateKey = convertPrivateKey(privateKey);
    }

    /**
     * Get public key bytes
     *
     * @return byte array of public key
     */
    public byte[] getPublicBytes() {
        return Base64.getEncoder().encode(publicKey.getEncoded());
    }

    public byte[] getPrivateBytes() {
        return Base64.getEncoder().encode(privateKey.getEncoded());
    }

    public String getPublicString() {
        return new String(getPublicBytes());
    }

    public String getPrivateString() {
        return new String(getPrivateBytes());
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * Generate public key from string
     *
     * @param publicKey public key string
     * @return public key
     */
    public static PublicKey convertPublicKey(String publicKey) throws RSAAlgorithmException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSAAlgorithm.ALGORITHM_NAME);
            //generate public key
            byte[] publicBytes = Base64.getDecoder().decode(publicKey);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RSAAlgorithmException("Unable to generate public key from string " + publicKey + " . " + e.getMessage());
        }
    }

    /**
     * Generate private key
     *
     * @param privateKey private key string
     * @return private key
     */
    public static PrivateKey convertPrivateKey(String privateKey) throws RSAAlgorithmException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(RSAAlgorithm.ALGORITHM_NAME);
            //generate private key
            byte[] privateBytes = Base64.getDecoder().decode(privateKey);
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateBytes);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RSAAlgorithmException("Unable to generate private key from string " + privateKey + " . " + e.getMessage(), e);
        }
    }
}

