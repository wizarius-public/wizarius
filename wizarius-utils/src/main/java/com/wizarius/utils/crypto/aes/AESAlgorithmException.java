package com.wizarius.utils.crypto.aes;

/**
 * Created by Vladyslav Shyshkin on 11.09.17.
 */
public class AESAlgorithmException extends Exception {
    public AESAlgorithmException(String message) {
        super(message);
    }

    public AESAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }
}
