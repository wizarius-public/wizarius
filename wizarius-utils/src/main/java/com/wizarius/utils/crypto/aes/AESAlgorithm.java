package com.wizarius.utils.crypto.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * Created by Vladyslav Shyshkin on 11.09.17.
 */
public class AESAlgorithm {
    private final String defaultKey;
    private final String defaultVector; //ONLY 16 bytes IV

    public AESAlgorithm(String defaultKey, String defaultVector) {
        this.defaultKey = defaultKey;
        this.defaultVector = defaultVector;
    }

    /**
     * Encrypt (шифровать) message using AES algorithm
     *
     * @param message message to encrypt
     * @return encrypted message
     * @throws AESAlgorithmException
     */
    public String encrypt(String message) throws AESAlgorithmException {
        try {
            IvParameterSpec iv = new IvParameterSpec(defaultVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(defaultKey.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(message.getBytes());

            return new String(Base64.getEncoder().encode(encrypted));
        } catch (Exception ex) {
            throw new AESAlgorithmException("Unable to encrypt value by AES algorithm", ex);
        }
    }

    /**
     * Decrypt (расшифровать) message using AES algorithm
     *
     * @param encrypted encrypted message
     * @return decrypted message
     * @throws AESAlgorithmException
     */
    public String decrypt(String encrypted) throws AESAlgorithmException {
        try {
            IvParameterSpec iv = new IvParameterSpec(defaultVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(defaultKey.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));

            return new String(original);
        } catch (Exception ex) {
            throw new AESAlgorithmException("Unable to decrypt value by AES algorithm", ex);
        }
    }
}
