package com.wizarius.utils.crypto.rsa;

import com.wizarius.utils.utils.LazyBase64;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Vladyslav Shyshkin on 11.09.17.
 * This method generate public and private key using RSA algorithm
 * <p>
 * So basicly you divide the key length with 8 -11(if you have padding).
 * For example if you have a 2048bit key you can encrypt 2048/8 = 256 bytes (- 11 bytes if you have padding).
 * So, either use a larger key or you encrypt the data with a symmetric key,
 * and encrypt that key with rsa (which is the recommended approach).
 * <p>
 * 1. Generate a symmetric key (< 256 bytes)
 * 2. Encrypt symmetric key with RSA
 * 3. Transfer encrypted symmetric key
 * 4. Decrypt symmetric key with RSA
 * 5. Encrypt data (> 256 bytes) with symmetric key
 * 6. Transfer encrypted data
 * 7. Decrypt encrypted data with symmetric key
 */
@SuppressWarnings("all")
@Slf4j
public class RSAAlgorithm {
    public enum KEY_LENGTHS {
        KEY_1024(1024),
        KEY_2048(2048),
        KEY_4096(4096),
        KEY_8192(8192);

        private final int lenght;

        KEY_LENGTHS(int lenght) {
            this.lenght = lenght;
        }

        public int getLength() {
            return lenght;
        }
    }

    public enum SIGNATURE_TYPES {
        SHA256("SHA256withRSA"),
        SHA512("SHA512withRSA");

        private final String type;

        SIGNATURE_TYPES(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public final static String ALGORITHM_NAME = "RSA";
    private final int keyLenght;
    private final SIGNATURE_TYPES signatureType;

    public RSAAlgorithm(KEY_LENGTHS length) {
        this.keyLenght = length.getLength();
        this.signatureType = SIGNATURE_TYPES.SHA512;
    }

    public RSAAlgorithm(KEY_LENGTHS length, SIGNATURE_TYPES type) {
        this.keyLenght = length.getLength();
        this.signatureType = type;
    }

    /**
     * Generate new key pair
     *
     * @return new key pair
     * @throws NoSuchAlgorithmException
     */
    public RSAKeysPair generateNewKeyPair() throws RSAAlgorithmException {
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance(ALGORITHM_NAME);
        } catch (NoSuchAlgorithmException e) {
            throw new RSAAlgorithmException("Unable to generate new key pair ", e);
        }
        kpg.initialize(keyLenght);
        KeyPair kp = kpg.genKeyPair();
        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();
        return new RSAKeysPair(publicKey, privateKey);
    }

    /**
     * Encrypt (шифровать) message by private key
     *
     * @param privateKey user private key
     * @param message    message to encrypt
     * @return byte array of encrypted message
     * @throws Exception
     */
    public byte[] encrypt(PrivateKey privateKey, String message) throws RSAAlgorithmException {
        if (isDataLonger(message.length())) {
            log.trace("Split message, short key");
            ArrayList<byte[]> arrayList = new ArrayList<>();
            String[] strings = splitString(message);
            for (String string : strings) {
                byte[] encrypt = encrypt(privateKey, string);
                arrayList.add(encrypt);
            }
            return joinArrayOfBytes(arrayList);
        }
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            return cipher.doFinal(message.getBytes());
        } catch (BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new RSAAlgorithmException("Unable to encrypt message " + " . " + e, e);
        }
    }

    /**
     * Encrypt (шифровать) message by public key
     *
     * @param publicKey user public key
     * @param message   message to encrypt
     * @return byte array of encrypted message
     * @throws Exception
     */
    public byte[] encrypt(PublicKey publicKey, String message) throws RSAAlgorithmException {
        if (isDataLonger(message.length())) {
            log.trace("Split message, short key");
            ArrayList<byte[]> arrayList = new ArrayList<>();
            String[] strings = splitString(message);
            for (String string : strings) {
                byte[] encrypt = encrypt(publicKey, string);
                arrayList.add(encrypt);
            }
            return joinArrayOfBytes(arrayList);
        }
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher.doFinal(message.getBytes());
        } catch (BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new RSAAlgorithmException("Unable to encrypt message " + " . " + e, e);
        }
    }

    /**
     * Decrypt (расшифровать) message by public key
     *
     * @param publicKey user public key
     * @param encrypted bytes of message to decrypt
     * @return byte array of encrypted message
     * @throws Exception
     */
    public byte[] decrypt(PublicKey publicKey, byte[] encrypted) throws RSAAlgorithmException {
        if (encrypted.length > getEncryptArraySize()) {
            log.trace("Split message, short key");
            StringBuilder sb = new StringBuilder();
            ArrayList<byte[]> messages = splitBytes(encrypted);
            for (byte[] message : messages) {
                byte[] decrypt = decrypt(publicKey, message);
                sb.append(new String(decrypt));
            }
            return sb.toString().getBytes();
        }
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM_NAME);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            return cipher.doFinal(encrypted);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            throw new RSAAlgorithmException("Unable to decrypt message " + e.getMessage(), e);
        }
    }

    /**
     * Decrypt
     *
     * @param publicKey public key
     * @param encrypted encrypted message
     * @return original message
     * @throws RSAAlgorithmException
     */
    public byte[] decrypt(String publicKey, byte[] encrypted) throws RSAAlgorithmException {
        return this.decrypt(RSAKeysPair.convertPublicKey(publicKey), encrypted);
    }

    /**
     * Decrypt (расшифровать) message by private key
     *
     * @param privateKey user private key
     * @param encrypted  bytes of message to decrypt
     * @return byte array of encrypted message
     * @throws Exception
     */
    public byte[] decrypt(PrivateKey privateKey, byte[] encrypted) throws RSAAlgorithmException {
        if (encrypted.length > getEncryptArraySize()) {
            log.trace("Split message, short key");
            StringBuilder sb = new StringBuilder();
            ArrayList<byte[]> messages = splitBytes(encrypted);
            for (byte[] message : messages) {
                byte[] decrypt = decrypt(privateKey, message);
                sb.append(new String(decrypt));
            }
            return sb.toString().getBytes();
        }
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM_NAME);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return cipher.doFinal(encrypted);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            throw new RSAAlgorithmException("Unable to decrypt message ", e);
        }
    }

    /**
     * Check if message is longer then key maximum value
     *
     * @param length of message
     * @return true if need to splice
     */
    private boolean isDataLonger(int length) {
        return (keyLenght / 8) - 11 < length;
    }

    /**
     * Get encrypt array size
     *
     * @return size on encrypted array
     */
    private int getEncryptArraySize() {
        return keyLenght / 8;
    }

    /**
     * Split strings
     *
     * @param message message to split
     * @return array of strings
     */
    private String[] splitString(String message) {
        int size = (keyLenght / 8) - 11;
        int splits = (int) Math.ceil(message.length() / (float) size);
        String[] messages = new String[splits];
        for (int i = 0; i < splits; i++) {
            int start = i * size;
            int end = (i + 1) * size;
            if (end > message.length()) {
                end = message.length();
            }
            messages[i] = message.substring(start, end);
        }
        return messages;
    }

    /**
     * Split bytes to arraylist of bytes
     *
     * @param array full array to split
     * @return bytes array
     */
    private ArrayList<byte[]> splitBytes(byte[] array) {
        ArrayList<byte[]> arrayList = new ArrayList<>();
        int splitSize = getEncryptArraySize();
        for (int i = 0; i < array.length / splitSize; i++) {
            int start = i * splitSize;
            int end = (i + 1) * splitSize;
            arrayList.add(Arrays.copyOfRange(array, start, end));
        }
        return arrayList;
    }

    /**
     * Join array of bytes to one array
     *
     * @param array arraylist of bytes
     * @return byte array
     */
    private byte[] joinArrayOfBytes(ArrayList<byte[]> array) {
        int size = 0;
        for (byte[] bytes : array) {
            size += bytes.length;
        }
        byte[] result = new byte[size];
        int index = 0;
        for (byte[] bytes : array) {
            for (byte aByte : bytes) {
                result[index++] = aByte;
            }
        }
        return result;
    }

    /**
     * Sign
     *
     * @param plainText  plain text
     * @param privateKey private key
     * @return sign string
     * @throws Exception
     */
    public String sign(String plainText, PrivateKey privateKey) throws RSAAlgorithmException {
        try {
            Signature privateSignature = Signature.getInstance(signatureType.getType());
            privateSignature.initSign(privateKey);
            privateSignature.update(plainText.getBytes("utf-8"));

            byte[] signature = privateSignature.sign();
            return LazyBase64.encode(signature);
        } catch (Exception ex) {
            throw new RSAAlgorithmException("Unable to sign " + ex.getMessage(), ex);
        }
    }

    /**
     * Verify sign
     *
     * @param plainText plain text
     * @param signText  sign text
     * @param publicKey public key
     * @return true if sign is correct
     * @throws Exception
     */
    public boolean verify(String plainText, String signText, PublicKey publicKey) throws RSAAlgorithmException {
        try {
            Signature publicSignature = Signature.getInstance(signatureType.getType());
            publicSignature.initVerify(publicKey);
            publicSignature.update(plainText.getBytes("utf-8"));

            byte[] signatureBytes = LazyBase64.decodeToBytes(signText);
            return publicSignature.verify(signatureBytes);
        } catch (Exception ex) {
            throw new RSAAlgorithmException("Unable to verify " + ex.getMessage(), ex);
        }
    }

    /**
     * Verify sign
     *
     * @param plainText plain text
     * @param signText  sign text
     * @param publicKey public key
     * @return true if sign is correct
     * @throws Exception
     */
    public boolean verify(String plainText, byte[] signBytes, PublicKey publicKey) throws RSAAlgorithmException {
        try {
            Signature publicSignature = Signature.getInstance(signatureType.getType());
            publicSignature.initVerify(publicKey);
            publicSignature.update(plainText.getBytes("utf-8"));
            return publicSignature.verify(signBytes);
        } catch (Exception ex) {
            throw new RSAAlgorithmException("Unable to verify " + ex.getMessage(), ex);
        }
    }
}

