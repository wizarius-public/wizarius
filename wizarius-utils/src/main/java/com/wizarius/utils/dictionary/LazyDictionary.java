package com.wizarius.utils.dictionary;


import com.wizarius.utils.utils.LazyRandom;

import java.util.ArrayList;

public class LazyDictionary {
    /**
     * Get list of random words from dictionary
     *
     * @param dictionary list of words
     * @param minLength  min word length
     * @param maxLength  max word length
     * @param countWord  count of words
     * @return list of words
     * @throws LazyDictionaryException on get random words exception
     */
    public static ArrayList<String> getRandWords(ArrayList<String> dictionary, int minLength, int maxLength, int countWord) throws LazyDictionaryException {
        if (dictionary == null || dictionary.size() == 0) {
            throw new LazyDictionaryException("Incorrect array parameter: " + dictionary);
        }
        if (minLength < 1 || maxLength < 1 || minLength > maxLength) {
            throw new LazyDictionaryException("Incorrect word size parameters: min size = " + minLength + ", max size = " + maxLength);
        }
        if (countWord < 1) {
            throw new LazyDictionaryException("Incorrect word count parameter = " + countWord);
        }
        // filter dictionary
        ArrayList<String> filteredDictionary = new ArrayList<>();
        for (String word : dictionary) {
            if (word.length() >= minLength && word.length() <= maxLength) {
                filteredDictionary.add(word);
            }
        }
        if (filteredDictionary.size() < countWord) {
            throw new LazyDictionaryException("The maximum number of words satisfying the parameters = " + filteredDictionary.size());
        }
        // generate array of random indices
        int[] randomSet = new int[countWord];
        int minIndex = 0;
        int maxIndex = filteredDictionary.size() - 1;
        for (int i = 0; i < randomSet.length; i++) {
            randomSet[i] = LazyRandom.getInt(minIndex, maxIndex);
        }
        // fill result list
        ArrayList<String> resultList = new ArrayList<>();
        for (int i : randomSet) {
            resultList.add(filteredDictionary.get(i));
        }
        return resultList;
    }

    /**
     * Get random word from dictionary
     *
     * @param dictionary list of words
     * @param minLength  min word length
     * @param maxLength  max word length
     * @return random word
     * @throws LazyDictionaryException on get random words exception
     */
    public static String getRandWord(ArrayList<String> dictionary, int minLength, int maxLength) throws LazyDictionaryException {
        if (dictionary == null || dictionary.size() == 0) {
            throw new LazyDictionaryException("Incorrect array parameter: " + dictionary);
        }
        if (minLength < 1 || maxLength < 1 || minLength > maxLength) {
            throw new LazyDictionaryException("Incorrect word size parameters: min size = " + minLength + ", max size = " + maxLength);
        }
        // filter dictionary
        ArrayList<String> filteredDictionary = new ArrayList<>();
        for (String word : dictionary) {
            if (word.length() >= minLength && word.length() <= maxLength) {
                filteredDictionary.add(word);
            }
        }
        if (filteredDictionary.size() == 0) {
            throw new LazyDictionaryException("Word not found, try to change min/max word length parameter.");
        }
        // generate random index
        int minIndex = 0;
        int maxIndex = filteredDictionary.size() - 1;
        int randomIndex = LazyRandom.getInt(minIndex, maxIndex);

        return filteredDictionary.get(randomIndex);
    }

    /**
     * Get list of random words from dictionary
     *
     * @param dictionary list of words
     * @param begin      beginning of the word
     * @param minLength  min word length
     * @param maxLength  max word length
     * @param countWord  count of words
     * @return list of words
     * @throws LazyDictionaryException on get random words exception
     */
    public static ArrayList<String> getRandWordsStartAs(ArrayList<String> dictionary, String begin, int minLength, int maxLength, int countWord) throws LazyDictionaryException {
        if (dictionary == null || dictionary.size() == 0) {
            throw new LazyDictionaryException("Incorrect array parameter: " + dictionary);
        }
        if (minLength < 1 || maxLength < 1 || minLength > maxLength) {
            throw new LazyDictionaryException("Incorrect word size parameters: min size = " + minLength + ", max size = " + maxLength);
        }
        if (countWord < 1) {
            throw new LazyDictionaryException("Incorrect word count parameter = " + countWord);
        }
        if (begin == null || begin.isEmpty()) {
            throw new LazyDictionaryException("Incorrect word begin parameter = " + begin);
        }
        // filter dictionary
        ArrayList<String> filteredDictionary = new ArrayList<>();
        for (String word : dictionary) {
            if (word.startsWith(begin) && word.length() >= minLength && word.length() <= maxLength) {
                filteredDictionary.add(word);
            }
        }
        if (filteredDictionary.size() < countWord) {
            throw new LazyDictionaryException("The maximum number of words satisfying the parameters = " + filteredDictionary.size());
        }
        // generate array of random indices
        int[] randomSet = new int[countWord];
        int minIndex = 0;
        int maxIndex = filteredDictionary.size() - 1;
        for (int i = 0; i < randomSet.length; i++) {
            randomSet[i] = LazyRandom.getInt(minIndex, maxIndex);
        }
        // fill result list
        ArrayList<String> resultList = new ArrayList<>();
        for (int i : randomSet) {
            resultList.add(filteredDictionary.get(i));
        }
        return resultList;
    }
}
