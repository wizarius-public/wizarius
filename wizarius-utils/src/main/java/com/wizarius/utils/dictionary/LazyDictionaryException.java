package com.wizarius.utils.dictionary;

public class LazyDictionaryException extends Exception {

    public LazyDictionaryException(String message){
        super(message);
    }
}
