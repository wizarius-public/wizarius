package com.wizarius.utils.models;

/**
 * Created by Vladyslav Shyshkin on 18.06.2018.
 */
public class ApiKeys {
    private final String apiKey;
    private final String secretKey;

    public ApiKeys(String apiKey, String secretKey) {
        this.apiKey = apiKey;
        this.secretKey = secretKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    @Override
    public String toString() {
        return "ApiKeys{" +
                "apiKey='" + apiKey + '\'' +
                ", secretKey='" + secretKey + '\'' +
                '}';
    }
}
