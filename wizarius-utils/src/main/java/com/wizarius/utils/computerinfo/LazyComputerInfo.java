package com.wizarius.utils.computerinfo;


import com.wizarius.utils.utils.LazyBash;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by Vladyslav Shyshkin on 03.01.2018.
 */
@SuppressWarnings("unused")
public class LazyComputerInfo {
    private static final FileSystemView fsv = FileSystemView.getFileSystemView();

    /**
     * Get user name of computer
     *
     * @return current user name
     */
    public static String getUserName() {
        return System.getProperty("user.name");
    }

    /**
     * Get maximum size of ram memory for jvm
     *
     * @return size in bytes
     */
    public static long getJVMRamMemoryMaximum() {
        return Runtime.getRuntime().maxMemory();
    }

    /**
     * Get free size of ram memory for jvm
     *
     * @return free ram memory for jvm
     */
    public static long getJVMRamFreeMemory() {
        return Runtime.getRuntime().freeMemory();
    }

    /**
     * Get available processors in system
     *
     * @return available processors
     */
    public static int availableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * Get computer RAM size
     *
     * @return computer ram size
     */
    public static long getRAMSize() {
        return ((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
    }

    /**
     * Get free ram size
     *
     * @return free ram size
     */
    public static long getFreeRAMSize() {
        return ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getFreePhysicalMemorySize();
    }

    /**
     * Get computer name
     *
     * @return computer name
     */
    public static String getComputerName() {
        try {
            return String.valueOf(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get OS name
     *
     * @return os name
     */
    public static String getOSName() {
        return System.getProperty("os.name");
    }

    /**
     * Get OS version
     * <p>
     * Can use System.getProperty("os.version")
     *
     * @return oc version
     */
    public static String getOSVersion() {
        return ManagementFactory.getOperatingSystemMXBean().getVersion();
    }

    /**
     * Return computer architecture like x86_64 etc.
     *
     * @return architecture string
     */
    public static String getSystemArchitecture() {
        return System.getProperty("os.arch");
    }

    /**
     * Get application dir path
     *
     * @return path
     */
    public static String getCurrentDir() {
        return System.getProperty("user.dir");
    }

    /**
     * Get screen dimensions
     *
     * @return screen dimensions
     */
    public static Dimension getScreenSize() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }

    /**
     * Get disk folders array
     *
     * @return root folders
     */
    public static File[] getRootsFolder() {
        return fsv.getRoots();
    }

    /**
     * Get roots list
     *
     * @return roots array
     */
    public static File[] getRootsList() {
        return File.listRoots();
    }

    /**
     * Get driver info list
     *
     * @return driver info list
     */
    public static ArrayList<DriverInfo> getDriverInfo() {
        ArrayList<DriverInfo> list = new ArrayList();
        File[] f = getRootsList();
        for (File file : f) {
            DriverInfo driverInfo = new DriverInfo(
                    file.toString(),
                    fsv.getSystemDisplayName(file),
                    fsv.isDrive(file),
                    fsv.isFloppyDrive(file),
                    file.canRead(),
                    file.canWrite(),
                    file.getTotalSpace(),
                    file.getUsableSpace()
            );
            list.add(driverInfo);
        }
        return list;
    }

    /**
     * Get linux mouse devices
     *
     * @return mouse device array
     */
    public static ArrayList<String> getLinuxMouseDevice() {
        ArrayList<String> mouseDevice = new ArrayList();
        if (LazyComputerInfo.getOSName().equalsIgnoreCase("linux")) {
            File roots = new File("/dev/input");
            File[] files = roots.listFiles();
            if (files == null) {
                return mouseDevice;
            }
            for (File root : files) {
                if (root.getName().contains("mouse")) {
                    mouseDevice.add(root.getName());
                }
            }
        }
        return mouseDevice;
    }


    /**
     * Detect cpu name
     *
     * @return cpu name
     */
    public static String detectCPUName() {
        if (LazyComputerInfo.getOSName().contains("Linux")) {
            String executeCommand = LazyBash.executeCommands("/bin/sh", "-c", "cat /proc/cpuinfo | grep 'model name' | uniq");
            int index = executeCommand.indexOf(':') + 1;
            if (index >= 0) {
                return executeCommand.substring(index).trim();
            }
        } else if (LazyComputerInfo.getOSName().contains("Windows")) {
            String executeCommand = LazyBash.executeCommands("wmic cpu get name");
            executeCommand = executeCommand.trim().replace("Name", "").trim();
            return executeCommand;
        } else if (LazyComputerInfo.getOSName().contains("Mac")) {
            return LazyBash.executeCommands("/bin/sh", "-c", "sysctl -n machdep.cpu.brand_string");
        }
        return "Unknown";
    }
}
