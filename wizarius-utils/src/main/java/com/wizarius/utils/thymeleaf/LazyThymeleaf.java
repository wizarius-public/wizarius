package com.wizarius.utils.thymeleaf;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import java.util.Map;

/**
 * Created by Vladyslav Shyshkin on 20.11.2017.
 */
public class LazyThymeleaf {
    /**
     * Parse html by file in resources folder
     *
     * @param filename  filename in resources
     * @param objectMap objects map
     * @return html string
     */
    public static String parseHTMLResourcesFile(String filename, Map<String, Object> objectMap) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("HTML5");
        //resolver.setSuffix(".html");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);

        final Context context = new Context();
        if (objectMap != null) {
            context.setVariables(objectMap);
        }
        return templateEngine.process(filename, context);
    }

    /**
     * Parse html file in filesystem
     *
     * @param filename  filename
     * @param objectMap objects map
     * @return html string
     */
    public static String parseHTMLFile(String filename, Map<String, Object> objectMap) {
        FileTemplateResolver resolver = new FileTemplateResolver();
        resolver.setTemplateMode("HTML5");
        //resolver.setSuffix(".html");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);

        final Context context = new Context();
        if (objectMap != null) {
            context.setVariables(objectMap);
        }
        return templateEngine.process(filename, context);
    }

    /**
     * Parse html string
     *
     * @param htmlString html string
     * @param objectMap  object map
     * @return parsed html string
     */
    public static String parseHTMLString(String htmlString, Map<String, Object> objectMap) {
        StringTemplateResolver resolver = new StringTemplateResolver();
        resolver.setTemplateMode("HTML5");
        //resolver.setSuffix(".html");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);

        final Context context = new Context();
        if (objectMap != null) {
            context.setVariables(objectMap);
        }
        return templateEngine.process(htmlString, context);
    }
}
