package com.wizarius.utils.utils;

import java.util.Random;

/**
 * @author Vladyslav Shyshkin on 29.04.17.
 */
public class LazyRandom {
    private static final Random rand = new Random();

    /**
     * Get int random from diapason
     *
     * @param min minimum value
     * @param max maximum value
     * @return int value [min;max]
     */
    public static int getInt(int min, int max) {
        if (max <= min) {
            return 0;
        }
        return rand.nextInt(max - min + 1) + min;
    }


    /**
     * Get random double value
     *
     * @param min minimum value
     * @param max maximum value
     * @return double value [min;max]
     */
    public static double getDouble(double min, double max) {
        double random = new Random().nextDouble();
        return min + (random * (max - min));
    }

    /**
     * this method returns a random number n such that
     * 0.0 <= n <= 1.0
     *
     * @return random such that 0.0 <= random <= 1.0
     */
    public static double randomDouble() {
        return rand.nextInt(1000) / 1000.0;
    }

    /**
     * Get unique random array elements
     *
     * @param min           minimum value
     * @param max           maximum value
     * @param countElements unique elements
     * @return array of unique int values
     */
    public static int[] getUniqueInt(int min, int max, int countElements) {
        if (max <= 0 || max - min <= countElements) {
            return new int[countElements];
        }
        int array[] = new int[countElements];
        int currentSize = 0;
        while (true) {
            int number = getInt(min, max);
            if (!containsNumber(array, number)) {
                array[currentSize++] = number;
            }
            if (currentSize == countElements) {
                break;
            }
        }
        return array;
    }


    /**
     * Check if mas contains the number
     *
     * @param mas    arraylist of numbers
     * @param number search number
     * @return conteins or not
     */
    private static boolean containsNumber(int[] mas, int number) {
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == number) {
                return true;
            }
        }
        return false;
    }
}
