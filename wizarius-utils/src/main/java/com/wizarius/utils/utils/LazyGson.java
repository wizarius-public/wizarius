package com.wizarius.utils.utils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * @author Vladyslav Shyshkin on 9/26/16.
 */
public class LazyGson {
    private static final Gson gson = new Gson();

    public static String objectToGson(Object object) {
        return gson.toJson(object);
    }

    public static Object getJsonToObject(String json, Class clazz) {
        return gson.fromJson(json, clazz);
    }

    public static Object getJsonToObject(Object json, Class clazz) {
        return gson.fromJson(json.toString(), clazz);
    }

    public static Object getJsonToObject(String json, Type type) {
        return gson.fromJson(json, type);
    }
}
