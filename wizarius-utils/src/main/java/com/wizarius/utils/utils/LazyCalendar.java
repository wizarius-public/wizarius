package com.wizarius.utils.utils;


import com.wizarius.utils.calendar.DatePeriod;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

/**
 * @author Vladyslav Shyshkin on 10.03.17.
 */
@SuppressWarnings({"WeakerAccess", "SameParameterValue", "unused"})
public class LazyCalendar {
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Get count of days in mont
     *
     * @return count of days in current month
     */
    public static int daysInCurrentMonth() {
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        // Create a calendar object and set year and month
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay);

        // Get the number of days in that month
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * Date to time array
     *
     * @return current days time array
     */
    public static Date[] getDayByHour(Date date) {
        Date dateArray[] = new Date[25];
        // Create a calendar object and set year and month
        for (int i = 0; i < dateArray.length; i++) {
            if (i == dateArray.length - 1) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.set(Calendar.HOUR_OF_DAY, i - 1);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                dateArray[i] = calendar.getTime();
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.set(Calendar.HOUR_OF_DAY, i);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                dateArray[i] = calendar.getTime();
            }
        }
        return dateArray;
    }

    /**
     * Get day by hour
     *
     * @param timestamp date timestamp
     * @return date array
     */
    public static Date[] getDayByHour(int timestamp) {
        return getDayByHour(new Date(timestamp * 1000));
    }

    /**
     * Get day in hour period array
     *
     * @param date date to generate period array
     * @return DatePeriod array
     */
    public static ArrayList<DatePeriod> getDayByHourPeriod(Date date) {
        ArrayList<DatePeriod> period = new ArrayList<>();
        // Create a calendar object and set year and month
        for (int i = 0; i < 24; i++) {
            Calendar start = Calendar.getInstance();
            start.setTime(date);
            start.set(Calendar.HOUR_OF_DAY, i);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);

            Calendar end = Calendar.getInstance();
            if (i == 23) {
                end.setTime(date);
                end.set(Calendar.HOUR_OF_DAY, i);
                end.set(Calendar.MINUTE, 59);
                end.set(Calendar.SECOND, 59);
            } else {
                end.setTime(date);
                end.set(Calendar.HOUR_OF_DAY, i + 1);
                end.set(Calendar.MINUTE, 0);
                end.set(Calendar.SECOND, 0);
            }

            period.add(
                    new DatePeriod(
                            start.getTime(),
                            end.getTime())
            );
        }
        return period;
    }

    /**
     * Get day by hour period
     *
     * @param date  start date
     * @param hours hours to set
     * @return date period
     */
    public static ArrayList<DatePeriod> getDayByHourPeriod(Date date, int hours) {
        ArrayList<DatePeriod> period = new ArrayList<>();
        Date lastDate = addHoursToDate(date, 1, true);
        for (int i = hours; i > 0; i--) {
            Calendar start = Calendar.getInstance();
            start.setTime(lastDate);
            start.set(Calendar.HOUR, start.get(Calendar.HOUR));
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);

            Calendar end = Calendar.getInstance();
            end.setTime(lastDate);
            end.set(Calendar.HOUR, end.get(Calendar.HOUR) - 1);
            end.set(Calendar.MINUTE, 0);
            end.set(Calendar.SECOND, 0);

            lastDate = end.getTime();

            period.add(new DatePeriod(end.getTime(), start.getTime()));
        }
        return period;
    }

    /**
     * Add hours to date
     *
     * @param startDate start date
     * @param hoursAdd  hours add
     * @return date
     */
    public static Date addHoursToDate(Date startDate, int hoursAdd, boolean isSetToZero) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + hoursAdd);
        if (isSetToZero) {
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
        }
        return cal.getTime();
    }

    /**
     * Get first day of current month
     *
     * @return first day of month
     */
    public static Date getMonthFirstDay() {
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = 1;

        // Create a calendar object and set year and month
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay, 23, 59, 59);
        return calendar.getTime();
    }

    /**
     * Get current date start
     *
     * @return current day at 00:00:00
     */
    public static Date getCurrentDayStart() {
        return getDateStartFromDate(new Date());
    }

    /**
     * Get current date end
     *
     * @return current day at 23:59:59
     */
    public static Date getCurrentDayEnd() {
        return getDateEndFromDate(new Date());
    }

    /**
     * Get current day start in timestamp
     *
     * @return current day start timestamp
     */
    public static int getCurrentDayStartTimestamp() {
        return (int) (getCurrentDayStart().getTime() / 1000);
    }


    /**
     * Get current day ent in timestamp
     *
     * @return current day ent timestamp
     */
    public static int getCurrentDayEndTimestamp() {
        return (int) (getCurrentDayEnd().getTime() / 1000);
    }

    /**
     * Get Last day of month
     *
     * @return last day of month
     */
    public static Date getMonthLastDay() {
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        // Create a calendar object and set year and month
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay);

        Calendar calendarFromLastDay = new GregorianCalendar(iYear, iMonth, calendar.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59);

        return calendarFromLastDay.getTime();
    }


    /**
     * Get date end time
     *
     * @param date date to convert to start
     * @return date where time = 23:59
     */
    public static Date getDateEndFromDate(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }

    /**
     * Get date start time
     *
     * @param date unix timestamp
     * @return date from 00:00
     */
    public static Date getDateEndFromDate(int date) {
        return getDateEndFromDate(LazyDate.getDateFromUnixTimestamp(date));
    }

    /**
     * Get date start time
     *
     * @param date date to convert to start
     * @return date from 00:00
     */
    public static Date getDateStartFromDate(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(endOfDay);
    }

    /**
     * Get date start time
     *
     * @param date unix timestamp
     * @return date from 00:00
     */
    public static Date getDateStartFromDate(int date) {
        return getDateStartFromDate(LazyDate.getDateFromUnixTimestamp(date));
    }


    /**
     * Get date start time
     * Этот метод будет возвращать не правильную дату при работе на разных серверах
     *
     * @param date date to convert to start
     * @return date from 00:00
     */
    @Deprecated
    public static Date getCalendarDateStartFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Get date end time
     * Этот метод будет возвращать не правильную дату при работе на разных серверах
     *
     * @param date date to convert to start
     * @return date from 23:59
     */
    @Deprecated
    public static Date getCalendarDateEndFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    /**
     * Local date time to date
     *
     * @param startOfDay start of day
     * @return local date time to date
     */
    private static Date localDateTimeToDate(LocalDateTime startOfDay) {
        return Date.from(startOfDay.atZone(ZoneId.of("Etc/UTC")).toInstant());
    }

    /**
     * Date to local date
     *
     * @param date date to local datetime
     * @return local date time
     */
    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.of("Etc/UTC"));
    }


    /**
     * Check if date to check falls in period
     * Inclusive to start time and end time
     *
     * @param startDate   start date
     * @param endDate     end date
     * @param dateToCheck date to check
     * @return true if fall
     */
    public static boolean dateFallsInPeriod(Date startDate, Date endDate, Date dateToCheck) {

        return (startDate.getTime() == dateToCheck.getTime() || dateToCheck.after(startDate))
                &&
                (endDate.getTime() == dateToCheck.getTime() || dateToCheck.before(endDate));
    }

    /**
     * Check if date to check falls in period
     * Inclusive to start time and end time
     *
     * @param startTimestamp start date
     * @param endTimestamp   end date
     * @param dateToCheck    date to check
     * @return true if fall
     */
    public static boolean dateFallsInPeriod(int startTimestamp, int endTimestamp, int dateToCheck) {
        return startTimestamp >= dateToCheck && dateToCheck <= endTimestamp;
    }

    /**
     * Get current month string array
     *
     * @return current month date string array
     */
    public static String[] currentMonthArray() {
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay);

        String array[] = new String[calendar.getActualMaximum(Calendar.DAY_OF_MONTH)];
        for (int i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            calendar.set(Calendar.DAY_OF_MONTH, i);
            String date = DATE_FORMAT.format(calendar.getTime());
            array[i - 1] = date;
        }
        return array;
    }

    /**
     * Get current month date array
     *
     * @return current month date array
     */
    public static Date[] currentMonthDateArray() {
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay);

        Date array[] = new Date[calendar.getActualMaximum(Calendar.DAY_OF_MONTH)];
        for (int i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            calendar.set(Calendar.DAY_OF_MONTH, i);
            array[i - 1] = calendar.getTime();
        }
        return array;
    }

    /**
     * Get current week days array
     *
     * @return current week date array
     */
    public static Date[] getDaysOfWeek() {
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        Date[] daysOfWeek = new Date[7];
        for (int i = 0; i < 7; i++) {
            daysOfWeek[i] = calendar.getTime();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return daysOfWeek;
    }

    /**
     * Current month date array
     *
     * @return current month date array
     */
    public static LinkedHashMap<String, Integer> currentMonth() {
        LinkedHashMap<String, Integer> resultMap = new LinkedHashMap<>();
        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        // 1 (months begin with 0)
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Calendar calendar = new GregorianCalendar(iYear, iMonth, iDay);

        for (int i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            calendar.set(Calendar.DAY_OF_MONTH, i);
            String date = DATE_FORMAT.format(calendar.getTime());
            resultMap.put(date, 0);
        }
        return resultMap;
    }

    /**
     * Get days from date
     *
     * @param startDate  start date
     * @param isNext     direction
     *                   if true will be added next days from start start
     *                   else will be added previous days
     * @param amountDays amount days to add
     * @return list of dates
     */
    public static ArrayList<Date> getDaysFromDate(Date startDate, boolean isNext, int amountDays) {
        ArrayList<Date> result = new ArrayList<>();
        Date date = LazyCalendar.getDateStartFromDate(startDate);
        result.add(date);
        if (!isNext) {
            for (int i = 0; i < amountDays - 1; i++) {
                date = LazyCalendar.getDateStartFromDate(LazyCalendar.getPreviousDay(date));
                result.add(date);
            }
        } else {
            for (int i = 0; i < amountDays - 1; i++) {
                date = LazyCalendar.getDateStartFromDate(LazyCalendar.getNextDay(date));
                result.add(date);
            }
        }
        return result;
    }

    /**
     * Get previous date from date
     *
     * @param date date
     * @return date - 1 day
     */
    public static Date getPreviousDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }

    /**
     * Get previous day timestamp
     *
     * @param date date
     * @return date -1 day in timestamp
     */
    public static int getPreviousDayTimestamp(Date date) {
        return (int) (getPreviousDay(date).getTime() / 1000);
    }

    /**
     * Get previous date from date
     *
     * @param date date
     * @return date + 1 day
     */
    public static Date getNextDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }

    /**
     * Build day
     *
     * @param day   day
     * @param month month
     * @param year  year
     * @return date
     */
    public static Date buildDate(int day, int month, int year) {
        return new GregorianCalendar(year, month - 1, day).getTime();
    }


    /**
     * Add milliseconds to date
     *
     * @param milliseconds amount milliseconds
     * @param startTime    start time
     * @return updated date
     */
    public static Date addMillisecondsToDate(int milliseconds, Date startTime) {
        long curTimeInMs = startTime.getTime();
        return new Date(curTimeInMs + milliseconds);
    }

    /**
     * Current day hour
     *
     * @return current day hour
     */
    public static int getCurrentDayHour() {
        Calendar rightNow = Calendar.getInstance();
        return rightNow.get(Calendar.HOUR_OF_DAY);
    }
}
