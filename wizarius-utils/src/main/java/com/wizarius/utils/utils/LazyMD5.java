package com.wizarius.utils.utils;

import org.slf4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author max
 */
public class LazyMD5 {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(LazyMD5.class);

    /**
     * Returns string md5 hash
     *
     * @param checkPass value to convert
     * @return md5 hash
     */
    public static String md5(String checkPass) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            byte[] data = checkPass.getBytes();
            m.update(data, 0, data.length);
            BigInteger i = new BigInteger(1, m.digest());
            return String.format("%1$032X", i).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            return checkPass;
        }
    }

    /**
     * Returns string md5 hash bytess
     *
     * @param checkPass value to convert
     * @return md5 hash bytes
     */
    public static byte[] md5Bytes(String checkPass) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            byte[] data = checkPass.getBytes();
            m.update(data, 0, data.length);
            return m.digest();
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
