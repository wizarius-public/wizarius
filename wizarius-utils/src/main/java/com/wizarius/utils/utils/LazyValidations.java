package com.wizarius.utils.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vladyslav Shyshkin on 22.01.2018.
 */
public class LazyValidations {
    public static boolean isEmail(String email) {
        String patternString = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        if (email == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
