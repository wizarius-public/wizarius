package com.wizarius.utils.utils;

import java.util.ArrayList;
import java.util.Collection;

public class LazyArray {

    public static int[] stringToIntegers(String str, String devider) throws NumberFormatException {
        String[] parts = str.split(devider);
        int[] values = new int[parts.length];
        for (int i = 0; i < parts.length; i++) {
            values[i] = Integer.parseInt(parts[i]);
        }
        return values;
    }

    /**
     * String to string array by delimiter
     *
     * @param str       income string
     * @param delimiter symbol to split
     * @return string array
     * @throws NumberFormatException
     */
    public static String[] stringToStrings(String str, String delimiter) throws NumberFormatException {
        return str.split(delimiter);
    }

    /**
     * Replace empty values in array
     *
     * @param arr income array
     * @return result array without empty string
     */
    public static String[] replaceEmptyValues(String[] arr) {
        ArrayList<String> result = new ArrayList<>();
        for (String s : arr) {
            if (s != null && !s.isEmpty()) {
                result.add(s);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * Convert int array to string
     *
     * @param buffer int array
     * @return converted array to string
     */
    public static String arrayToString(int[] buffer) {
        final StringBuilder sb = new StringBuilder();
        for (int v : buffer) {
            sb.append(v).append(",");
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * Convert int array to string
     *
     * @param buffer int array
     * @return converted array to string
     */
    public static String arrayToString(byte[] buffer) {
        final StringBuilder sb = new StringBuilder();
        for (int v : buffer) {
            sb.append(v).append(",");
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * Get int array from string
     *
     * @param buffer String to parse
     * @return integer array
     */
    public static int[] intArrayFromString(String buffer) {
        String[] split = buffer.split(",");
        int[] resultArray = new int[split.length];
        int index = 0;
        for (String s : split) {
            resultArray[index++] = Integer.parseInt(s);
        }
        return resultArray;
    }

    public static String arrayToString(String[] array) {
        final StringBuilder sb = new StringBuilder();
        for (String v : array) {
            sb.append(v).append(",");
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    public static String arrayToString(String[] array, char delimiter) {
        final StringBuilder sb = new StringBuilder();
        for (String v : array) {
            sb.append(v).append(delimiter);
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    public static String arrayToString(Collection<String> array, char delimiter) {
        final StringBuilder sb = new StringBuilder();
        for (String v : array) {
            sb.append(v).append(delimiter);
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    public static String arrayToString(String[] array, char delimiter, char surround) {
        final StringBuilder sb = new StringBuilder();
        for (String v : array) {
            sb.append(surround).append(v).append(surround).append(delimiter);
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * Array to string
     *
     * @param array     long array
     * @param delimiter delimiter between elements
     * @return joined array
     */
    public static String arrayToString(long[] array, char delimiter) {
        final StringBuilder sb = new StringBuilder();
        for (long v : array) {
            sb.append(v).append(delimiter);
        }
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * Преобразовывает набор String-ов в строку вида "element1", "element2", ... "elementN"
     *
     * @param set Набор строк
     * @return Строка с эелементами в скобках через запятую
     */
    public static String arrayToString(Collection<String> set) {
        final StringBuilder sb = new StringBuilder();
        set.forEach(s -> {
            sb.append('"');
            sb.append(s);
            sb.append("\", ");
        });
        //отрезаем хврст ', ' размеров в два символа
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 2);
        }
        return sb.toString();
    }

    /**
     * Преобразовывает набор String-ов в строку вида "element1", "element2", ... "elementN"
     *
     * @param set Набор строк
     * @return Строка с эелементами в скобках через запятую
     */
    public static String arrayToString(ArrayList<Integer> set) {
        final StringBuilder sb = new StringBuilder();
        set.forEach(s -> {
            sb.append('"');
            sb.append(s);
            sb.append("\", ");
        });
        //отрезаем хврст ', ' размеров в два символа
        if (sb.length() != 0) {
            sb.setLength(sb.length() - 2);
        }
        return sb.toString();
    }

    /**
     * Consert array of int to array of string.
     *
     * @param docIDs array of int
     * @return array of strings
     */
    public static String[] intArrayToStringArray(int[] docIDs) {
        String[] result = new String[docIDs.length];
        for (int i = 0; i < docIDs.length; i++) {
            result[i] = Integer.toString(docIDs[i]);
        }
        return result;
    }

    /**
     * Convert integer arraylist to int array
     *
     * @param arr array list of integer elements
     * @return int array
     */
    public static int[] convertIntegerArrayListToIntArray(ArrayList<Integer> arr) {
        int[] array = new int[arr.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = arr.get(i);
        }
        return array;
    }


    /**
     * Check if int array contains number
     *
     * @param arr  int array
     * @param item number to find
     * @return true if found
     */
    public static boolean intArrayContainsKey(int[] arr, int item) {
        for (int elem : arr) {
            if (item == elem) {
                return true;
            }
        }
        return false;
    }

    /**
     * Generate string in format ['1','2'] from string array
     *
     * @param arr string array
     * @return string
     */
    public static String stringFromArray(String[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (String s : arr) {
            sb.append("'").append(s).append("'").append(",");
        }
        if (arr.length != 0) {
            sb.setLength(sb.length() - 1);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Concat two byte arrays
     *
     * @param a byte array A
     * @param b byte array B
     * @return joined array
     */
    public static byte[] concatArrays(byte[] a, byte[] b) {
        byte[] concat = new byte[a.length + b.length];
        for (int i = 0; i < concat.length; i++) {
            concat[i] = i < a.length ? a[i] : b[i - a.length];
        }

        return concat;
    }
}

