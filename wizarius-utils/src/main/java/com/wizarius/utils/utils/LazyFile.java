package com.wizarius.utils.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class LazyFile {
    /**
     * Get file extension
     *
     * @param filename filename
     * @return file extension
     */
    public static String getExtension(String filename) {
        int lastIndexOf = filename.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return filename.substring(lastIndexOf + 1);
    }

    /**
     * Change file extension
     *
     * @param filename     filename
     * @param newExtension new extension
     * @return new filename
     */
    public static String changeExtension(String filename, String newExtension) {
        int lastIndexOf = filename.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return filename.substring(0, lastIndexOf) + "." + newExtension;
    }

    /**
     * Copy file to directory
     *
     * @param directory directory
     * @param source    source
     * @throws IOException on write exception
     */
    public static void copyFileToDirectory(File directory, File source) throws IOException {
        FileUtils.copyFileToDirectory(source, directory);
    }

    /**
     * Copy file
     *
     * @param destination destination file
     * @param source      source file
     * @throws IOException on write exception
     */
    public static void copyFile(File destination, File source) throws IOException {
        FileUtils.copyFile(source, destination);
    }

    /**
     * Delete folder
     *
     * @param folder folder path
     */
    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }


    /**
     * Delete all files in folder
     *
     * @param folder folders with file
     */
    public static void deleteFilesInFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) {
            for (File f : files) {
                f.delete();
            }
        }
    }
}
