package com.wizarius.utils.utils;

import java.util.UUID;

/**
 * @author max
 */
public class LazyUUID {

    public static UUID parse(final String s) {
        if (s.length() == 36) {
            return UUID.fromString(s);
        }
        final char[] ch = s.toCharArray();
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ch.length; i++) {
            sb.append(ch[i]);
            if (i == 7 || i == 11 || i == 15 || i == 19) {
                sb.append("-");
            }
        }
        return UUID.fromString(sb.toString());
    }

    public static UUID generate() {
        return UUID.randomUUID();
    }

    public static String generateString() {
        return UUID.randomUUID().toString();
    }

    public static String generateStringShort() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String toStringShort(UUID uuid) {
        return uuid.toString().replace("-", "");
    }

    public static String toStringShort(String uuid) {
        return uuid.replace("-", "");
    }
}
