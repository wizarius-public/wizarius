package com.wizarius.utils.utils;

/**
 * @author Vladyslav Shyshkin on 23.05.17.
 */
public class LazyConvert {
    /**
     * Convert int to size string
     *
     * @param size bytes size
     * @return converted size string
     */
    public static String convertSize(int size) {
        double result;
        if (size <= 1000) {
            return size + " Bite";
        } else if (size >= 1000000) {
            result = size / 1000000.0;
            return String.format("%.2f", result) + " MB";
        } else {
            result = size / 1000.0;
            return String.format("%.2f", result) + " KB";
        }
    }
}
