package com.wizarius.utils.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Vladyslav Shyshkin on 03.01.2018.
 */
public class LazyBash {
    /**
     * Execute command
     *
     * @param command script to execute
     * @return bash response
     */
    public static String executeCommands(String... command) {
        StringBuilder output = new StringBuilder();
        Process process;
        try {
            if (command.length == 1) {
                process = Runtime.getRuntime().exec(command[0]);
            } else {
                process = Runtime.getRuntime().exec(command);
            }
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }
            //replace last new line symbol
            if (output.length() != 0) {
                output.setLength(output.length() - 1);
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Unable to execute command " + Arrays.toString(command) + " . " + e.getMessage());
        }
        return output.toString();
    }
}
