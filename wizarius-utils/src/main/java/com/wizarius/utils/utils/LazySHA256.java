package com.wizarius.utils.utils;

import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Vladyslav Shyshkin on 11.09.17.
 */
@Slf4j
public class LazySHA256 {
    /**
     * Convert string to digest (hash) using algorithm SHA-256
     *
     * @param message income message
     * @return Hash String
     */
    public static String sha256(String message) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
            return LazyHexBin.bytesToHex(hash);
        } catch (NoSuchAlgorithmException e) {
            log.trace("Unable to convert string to sha256 " + " . " + e.getMessage(), e);
            return message;
        }
    }

    /**
     * Get sha256 in byte array
     *
     * @param message message to hash
     * @return byte array of hashed message
     */
    public static byte[] sha256Bytes(String message) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(message.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            log.trace("Unable to convert string to sha256 " + " . " + e.getMessage(), e);
            return message.getBytes();
        }
    }
}
