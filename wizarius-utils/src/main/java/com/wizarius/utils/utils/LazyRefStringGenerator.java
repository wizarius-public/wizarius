package com.wizarius.utils.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;

public class LazyRefStringGenerator {
    private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String lower = upper.toLowerCase(Locale.ROOT);
    private static final String digits = "0123456789";
    private static final String alphanum = upper + lower + digits;
    private static final Random random = new SecureRandom();
    private static final char[] symbols = alphanum.toCharArray();
    private static final char[] buf = new char[16];
    private static final int DELIMITERS_COUNT = 4;

    /**
     * Generate a random string.
     */
    public static String nextShortString() {
        for (int idx = 0; idx < buf.length; ++idx) {
            buf[idx] = symbols[random.nextInt(symbols.length)];
        }
        return new String(buf);
    }

    /**
     * Generate next random long string
     */
    public static String nextLongString() {
        String originalString = nextShortString();
        int splitSize = originalString.length() / DELIMITERS_COUNT;
        StringBuilder sb = new StringBuilder();
        char[] chars = originalString.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            sb.append(chars[i]);
            if ((i + 1) % splitSize == 0) {
                sb.append("-");
            }
        }
        sb.setLength(sb.length() - 1);
        return sb.toString().toUpperCase();
    }

    /**
     * Generate code
     *
     * @param length code length
     * @return code string
     */
    public static String generateCode(int length) {
        StringBuilder sb = new StringBuilder();
        for (int idx = 0; idx < length; ++idx) {
            char symbol = symbols[random.nextInt(symbols.length)];
            sb.append(symbol);
        }
        return sb.toString();
    }
}
