package com.wizarius.utils.utils;

import java.net.URLEncoder;
import java.util.Map;

public class StringUtils {

    public static boolean empty(String str) {
        if (str == null || "".equals(str)) return true;
        return false;
    }

    public static String buildQueryString(Map<String, ? extends Object> args) {
        StringBuilder result = new StringBuilder();
        for (String hashKey : args.keySet()) {
            if (result.length() > 0){
                result.append('&');
            }
            try {
                result.append(URLEncoder.encode(hashKey, "UTF-8"))
                        .append("=")
                        .append(URLEncoder.encode(args.get(hashKey).toString(), "UTF-8"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }


}
