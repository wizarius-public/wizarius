package com.wizarius.utils.utils;

/**
 * @author Vladyslav Shyshkin
 */
public class LazyMimeType {
    public final static String[] IMAGE_MIMES = {
            "image/gif",
            "image/jpeg",
            "image/pjpeg",
            "image/png"
    };

    /**
     * Mime types https://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc
     */
    public final static String[] PDF_TYPES = {
            "application/pdf"
    };

    public final static String[] DOC_TYPES = {
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-powerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
    };

    /**
     * Check if income mimetype is image
     *
     * @param mimeType income type to check
     * @return true if image
     */
    public static boolean isImage(String mimeType) {
        if (mimeType == null || mimeType.length() == 0) {
            return false;
        }
        for (String type : IMAGE_MIMES) {
            if (mimeType.equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if income mimetype is pdf
     *
     * @param mimeType income type to check
     * @return true if pdf
     */
    public static boolean isPDF(String mimeType) {
        if (mimeType == null || mimeType.length() == 0) {
            return false;
        }
        for (String contentType : PDF_TYPES) {
            if (contentType.equalsIgnoreCase(mimeType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if income mimetype is doc
     *
     * @param mimeType income type to check
     * @return true if doc
     */
    public static boolean isDOC(String mimeType) {
        if (mimeType == null || mimeType.length() == 0) {
            return false;
        }
        for (String docType : DOC_TYPES) {
            if (docType.equalsIgnoreCase(mimeType)) {
                return true;
            }
        }
        return false;
    }
}
