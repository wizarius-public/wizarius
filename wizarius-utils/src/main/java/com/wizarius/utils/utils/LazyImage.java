package com.wizarius.utils.utils;

import com.wizarius.utils.image.ImageFormat;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.*;
import java.net.URL;

/**
 * @author Vladyslav Shyshkin on 21.02.17.
 */
public class LazyImage {
    public enum IMAGE_JSP_FORMAT {
        PNG,
        JPG,
        GIF
    }

    /**
     * Create image for jsp page
     *
     * @param image image for jsp
     * @return scr string for <img> tag
     */
    public static String createImageForJSP(byte[] image, IMAGE_JSP_FORMAT format) {
        if (image == null) {
            return null;
        }
        return "data:image/" + format.toString().toLowerCase() + ";base64," + LazyBase64.encode(image);
    }


    /**
     * Get image by url
     *
     * @param url url to image
     * @return image bytes
     */
    public static byte[] getImageByUrl(String url) {
        try {
            URL webUrl = new URL(url);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            InputStream is = webUrl.openStream();
            byte[] imageFromWebBytes = new byte[4096];
            int n;
            while ((n = is.read(imageFromWebBytes)) > 0) {
                baos.write(imageFromWebBytes, 0, n);
            }
            if (is != null) {
                is.close();
            }
            return baos.toByteArray();
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Convenience method that returns a scaled instance of the
     * provided {@code BufferedImage}.
     *
     * @param img           the original image to be scaled
     * @param targetWidth   the desired width of the scaled instance,
     *                      in pixels
     * @param targetHeight  the desired height of the scaled instance,
     *                      in pixels
     * @param higherQuality if true, this method will use a multi-step
     *                      scaling technique that provides higher quality than the usual
     *                      one-step technique (only useful in downscaling cases, where
     *                      {@code targetWidth} or {@code targetHeight} is
     *                      smaller than the original dimensions, and generally only when
     *                      the {@code BILINEAR} hint is specified)
     * @return a scaled version of the original {@code BufferedImage}
     */
    public static BufferedImage getScaledInstance(BufferedImage img,
                                                  int targetWidth,
                                                  int targetHeight,
                                                  boolean higherQuality) {
        int type = (img.getTransparency() == Transparency.OPAQUE) ?
                BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage) img;
        int w, h;
        if (higherQuality) {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
            h = img.getHeight();
        } else {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() call
            w = targetWidth;
            h = targetHeight;
        }

        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();
            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }

    /**
     * Rotate buffered image
     *
     * @param image buffered image
     * @param angle angle to rotate
     * @return rotated image
     */
    public static BufferedImage rotateImage(BufferedImage image, double angle, ImageFormat format) {
        angle = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(angle));
        double cos = Math.abs(Math.cos(angle));
        int originWidth = image.getWidth(), originHeight = image.getHeight();
        //calc new height and width
        int newWidth = (int) Math.floor(originWidth * cos + originHeight * sin);
        int newHeight = (int) Math.floor(originHeight * cos + originWidth * sin);
        //create empty image with compatible parameters
        BufferedImage result;
        if (image.getType() == 0) {
            //for jpeg TYPE_3BYTE_BGR and png TYPE_4BYTE_ABGR
            if (format == ImageFormat.JPG) {
                result = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_3BYTE_BGR);
            } else {
                result = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_4BYTE_ABGR);
            }
        } else {
            result = new BufferedImage(newWidth, newHeight, image.getType());
        }
        Graphics2D g = result.createGraphics();
        //rotate image
        g.translate((newWidth - originWidth) / 2, (newHeight - originHeight) / 2);
        g.rotate(angle, originWidth / 2, originHeight / 2);
        //draw rendered image
        g.drawRenderedImage(image, null);
        g.dispose();
        return result;
    }

    /**
     * Rotate the image to 90 degrees by rgb offset
     *
     * @param array image bytes
     * @return new image bytes
     * @throws IOException
     */
    public static byte[] rotateImageByRgb(byte[] array, ImageFormat format) throws IOException {
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(array));

        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage newImage = new BufferedImage(height, width, image.getType());

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                newImage.setRGB(height - 1 - j, i, image.getRGB(i, j));

        //convert image to bytes
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(newImage, format.formatToStringType(), baos);

        return baos.toByteArray();
    }


    /**
     * Rotate image
     *
     * @param image image bytes
     * @param angle angle to rotate
     * @return rotated image
     */
    public static BufferedImage rotateImage(byte[] image, double angle, ImageFormat format) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(image);
        try {
            BufferedImage img = ImageIO.read(byteArrayInputStream);
            return rotateImage(img, angle, format);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Copy buffered image
     *
     * @param bufferedImage buffered image
     * @return new buffered image
     */
    public static BufferedImage copyBufferedImage(BufferedImage bufferedImage) {
        ColorModel cm = bufferedImage.getColorModel();
        boolean isAlpha = cm.isAlphaPremultiplied();
        WritableRaster raster = bufferedImage.copyData(null);
        return new BufferedImage(cm, raster, isAlpha, null);
    }

    /**
     * Get bytes from buffered image
     *
     * @param img buffered image
     * @return bytes of buffered image
     */
    public static byte[] bufferedImageToBytes(BufferedImage img, ImageFormat format) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, format.formatToStringType(), byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Bytes to buffered image
     *
     * @param img image bytes
     * @return buffered image
     */
    public static BufferedImage bytesToBufferedImage(byte[] img) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(img);
        try {
            return ImageIO.read(byteArrayInputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Write image to file system
     *
     * @param image buffered image
     * @param type  type of image
     * @param path  path to image file
     * @throws IOException
     */
    public static void writeImage(BufferedImage image, String type, String path) throws IOException {
        ImageIO.write(image, type, new File(path));
    }

    /**
     * Write image to file system
     *
     * @param image buffered image
     * @param type  type of image
     * @param path  path to image file
     * @throws IOException
     */
    public static void writeImage(byte[] image, String type, String path) throws IOException {
        ImageIO.write(bytesToBufferedImage(image), type, new File(path));
    }

    /**
     * Read image
     *
     * @param path path to file
     * @return read image
     * @throws IOException on read exception
     */
    public static BufferedImage readImage(String path) throws IOException {
        return ImageIO.read(new File(path));
    }

    /**
     * Converts an image to another format
     *
     * @param inputImagePath  Path of the source image
     * @param outputImagePath Path of the destination image
     * @throws IOException if errors occur during writing
     */
    public static void convertToJPGFormat(String inputImagePath,
                                          String outputImagePath) throws IOException {
        // reads input image from file
        BufferedImage inputImage = ImageIO.read(new File(inputImagePath));
        // remove alpha channel from image
        BufferedImage newBufferedImage = new BufferedImage(
                inputImage.getWidth(),
                inputImage.getHeight(),
                BufferedImage.TYPE_INT_RGB
        );
        newBufferedImage.createGraphics().drawImage(inputImage, 0, 0, Color.WHITE, null);
        // writes to the output image in specified format
        writeJPGWithCompression(newBufferedImage, outputImagePath, 1);
    }

    /**
     * Write jpg image with compression
     *
     * @param image           buffered image
     * @param outputImagePath output image path
     * @param compression     compression value from 0 to 1
     * @throws IOException on write exception
     */
    public static void writeJPGWithCompression(BufferedImage image, String outputImagePath, float compression) throws IOException {
        // writes to the output image in specified format
        JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
        jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        jpegParams.setCompressionQuality(compression);
        final ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        writer.setOutput(new FileImageOutputStream(new File(outputImagePath)));
        writer.write(null, new IIOImage(image, null, null), jpegParams);
    }

    /**
     * Resize image
     *
     * @param originalImage original iamge
     * @param scaledWidth   scaled width
     * @param scaledHeight  scaled height
     * @param preserveAlpha alpha
     * @return buffered image
     */
    public static BufferedImage resizeImage(Image originalImage,
                                            int scaledWidth,
                                            int scaledHeight,
                                            boolean preserveAlpha) {
        int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
        Graphics2D g = scaledBI.createGraphics();
        if (preserveAlpha) {
            g.setComposite(AlphaComposite.Src);
        }
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        return scaledBI;
    }
}
