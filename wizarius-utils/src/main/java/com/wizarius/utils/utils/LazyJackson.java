package com.wizarius.utils.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * Created by Vladyslav Shyshkin on 28.09.17.
 */
@Slf4j
public class LazyJackson {
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    /**
     * Object to json
     *
     * @param object object
     * @return json object
     */
    public static String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Unable to convert object to json by jackson library " + e.getMessage());
            return null;
        }
    }

    /**
     * From json
     *
     * @param json  json string
     * @param clazz class name
     * @return converted object
     */
    public static <T> T fromJson(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error("Unable to read object from json by jackson library " + e.getMessage(), e);
            return null;
        }
    }

    /**
     * Get json by type reference
     *
     * @param json      json string
     * @param reference reference
     * @return object
     */
    public static Object fromJson(String json, TypeReference reference) {
        try {

            return mapper.readValue(json, reference);
        } catch (IOException e) {
            log.error("Unable to read object from json by jackson library " + e.getMessage());
            return null;
        }
    }

    /**
     * Get new node instance
     *
     * @return new node instance
     */
    public static ObjectNode getNewNodeInstance() {
        return mapper.createObjectNode();
    }

    /**
     * Read tree
     *
     * @param json json message
     * @return json node
     */
    public static JsonNode readTree(String json) {
        try {
            return mapper.readTree(json);
        } catch (IOException e) {
            log.error("Unable to read tree from json " + e.getMessage(), e);
        }
        return null;
    }

    /**
     * Get object mapper
     *
     * @return object mapper
     */
    public static ObjectMapper getMapper() {
        return mapper;
    }

    /**
     * Parser JSON string from generic
     * <p>
     * Code example:
     * List<String> list = new ArrayList<>(Arrays.asList("1", "2", "3"));
     * List<String> t = LazyJackson.fromJson(LazyJackson.toJson(list), List.class, String.class);
     *
     * @param json         json string
     * @param mainClass    main class name f.e. List
     * @param genericClass internal class
     * @return parsed generic
     */
    public static <T> T fromJson(String json, Class<?> mainClass, Class genericClass) {
        JavaType type = LazyJackson.getMapper().getTypeFactory().constructParametricType(mainClass, genericClass);
        try {
            return mapper.readValue(json, type);
        } catch (IOException e) {
            log.error("Unable to read object from json by jackson library " + e.getMessage());
            return null;
        }
    }
}
