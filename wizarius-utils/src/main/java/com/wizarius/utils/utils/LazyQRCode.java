package com.wizarius.utils.utils;

import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;

import java.io.ByteArrayOutputStream;

/**
 * @author shyslav
 * Date: 1/7/19
 * Time: 2:30 PM
 */
public class LazyQRCode {
    /**
     * Created by UNKNOWN-Shvi on 04.07.2018.
     */
    public static int margin = 0;
    public static ErrorCorrectionLevel correctionLevel = ErrorCorrectionLevel.L;

    /**
     * Generate qr code
     *
     * @param code   string code
     * @param width  qr width
     * @param height qr height
     * @param type   image type
     */
    public static byte[] generateQRCode(String code, int width, int height, ImageType type) {
        ByteArrayOutputStream qrCode = QRCode
                .from(code)
                .withSize(width, height)
                .withHint(EncodeHintType.MARGIN, margin)
                .withHint(EncodeHintType.ERROR_CORRECTION, correctionLevel)
                .to(type)
                .stream();
        return qrCode.toByteArray();
    }

    /**
     * Generate qr code
     *
     * @param code   string code
     * @param width  qr width
     * @param height qr height
     * @param type   image type
     */
    public static byte[] generateQRCode(String code, int width, int height, int margin, ImageType type) {
        ByteArrayOutputStream qrCode = QRCode
                .from(code)
                .withSize(width, height)
                .withHint(EncodeHintType.MARGIN, margin)
                .withHint(EncodeHintType.ERROR_CORRECTION, correctionLevel)
                .to(type)
                .stream();
        return qrCode.toByteArray();
    }

    /**
     * Generate qr code for html content
     *
     * @param code   qr code
     * @param width  qr code width
     * @param height qr code height
     * @param type   qr code type
     * @return html src string
     */
    public static String generateQRCodeSrcContent(String code, int width, int height, ImageType type) {
        return LazyImage.createImageForJSP(generateQRCode(code, width, height, type), LazyImage.IMAGE_JSP_FORMAT.JPG);
    }

    /**
     * Generate qr code for html content
     *
     * @param code   qr code
     * @param width  qr code width
     * @param height qr code height
     * @param type   qr code type
     * @param margin margin size
     * @return html src string
     */
    public static String generateQRCodeSrcContent(String code, int width, int height, int margin, ImageType type) {
        return LazyImage.createImageForJSP(generateQRCode(code, width, height, margin, type), LazyImage.IMAGE_JSP_FORMAT.JPG);
    }
}
