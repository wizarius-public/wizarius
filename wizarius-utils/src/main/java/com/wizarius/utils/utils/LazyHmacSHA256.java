package com.wizarius.utils.utils;


import com.wizarius.utils.models.ApiKeys;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * Created by Vladyslav Shyshkin on 18.06.2018.
 */
public class LazyHmacSHA256 {
    private static final String algorithm = "HmacSHA256";

    /**
     * Get api keys
     *
     * @return api keys
     */
    public static ApiKeys generateKeys() throws NoSuchAlgorithmException {
        String s = generateHmacKey();
        return new ApiKeys(s.substring(0, 32), s.substring(32, 64));
    }


    /**
     * Verify account
     *
     * @param key       user secret key
     * @param signature signature key
     * @return true if verified
     * @throws NoSuchAlgorithmException on hmac sha 256 not found in libs
     */
    public static boolean verify(String key, String message, String signature) throws NoSuchAlgorithmException, InvalidKeyException {
        return encode(message, key).equals(signature);
    }

    /**
     * Generate hmac key
     *
     * @return key string
     * @throws NoSuchAlgorithmException on hmac sha 256 not found in libs
     */
    static String generateHmacKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
        SecretKey key = keyGen.generateKey();
        return toHexString(key.getEncoded());
    }

    /**
     * Encode message
     *
     * @param message message string
     * @param key     api secure key
     * @return encoded string
     */
    public static String encode(String message, String key) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), algorithm);
        Mac mac = Mac.getInstance(algorithm);
        mac.init(secretKeySpec);
        return toHexString(mac.doFinal(message.getBytes()));
    }

    /**
     * Convert to hex string
     *
     * @param bytes key bytes
     * @return hex string
     */
    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
}
