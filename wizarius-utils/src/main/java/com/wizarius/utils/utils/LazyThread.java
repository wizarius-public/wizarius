package com.wizarius.utils.utils;

import java.util.concurrent.TimeUnit;

/**
 * @author Vladyslav Shyshkin
 */
public class LazyThread {
    /**
     * Sleep and suppress exception
     *
     * @param delay pause in ms
     */
    public static void sleep(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ignored) {
        }
    }

    /**
     * /**
     * Sleep and suppress exception
     *
     * @param delay pause
     * @param unit  time units to sleep
     */
    public static void sleep(int delay, TimeUnit unit) {
        try {
            unit.sleep(delay);
        } catch (InterruptedException ignored) {
        }
    }
}
