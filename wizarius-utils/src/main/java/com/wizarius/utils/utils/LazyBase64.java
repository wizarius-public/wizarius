package com.wizarius.utils.utils;

import java.util.Base64;

/**
 * Class to encode and decode base 64 string
 *
 * @author Vladyslav Shyshkin
 */
public class LazyBase64 {
    synchronized public static String encode(String value) {
        if (value == null) {
            return null;
        }
        return new String(Base64.getEncoder().encode(value.getBytes()));
    }

    synchronized public static String decodeToString(String value) {
        if (value == null) {
            return null;
        }
        return new String(Base64.getDecoder().decode(value));
    }

    synchronized public static String encode(byte[] content) {
        if (content == null) {
            return null;
        }
        return new String(Base64.getEncoder().encode(content));
    }

    synchronized public static byte[] decodeToBytes(String content) {
        if (content == null) {
            return null;
        }
        return Base64.getDecoder().decode(content);
    }
}
