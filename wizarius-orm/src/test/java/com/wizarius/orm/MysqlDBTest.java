package com.wizarius.orm;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.actions.WizDBSelect;
import com.wizarius.orm.database.connection.ConnectionParameter;
import com.wizarius.orm.database.connection.ConnectionParametersList;
import com.wizarius.orm.database.connection.DBConnection;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.data.*;
import com.wizarius.orm.database.entityreader.EntityQuery;
import com.wizarius.orm.database.mysql.driver.MysqlDriver;
import com.wizarius.orm.entities.JoinT1;
import com.wizarius.orm.entities.JoinT2;
import com.wizarius.orm.entities.User;
import com.wizarius.orm.migrations.DBMigrationException;
import com.wizarius.orm.migrations.DBMigrationSystem;
import com.wizarius.orm.migrations.mysql.MysqlSystemMigrationList;
import com.wizarius.orm.migrations.savers.DBMigrationSaver;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.*;

/**
 * @author Vladyslav Shyshkin on 21.01.17.
 */
@Ignore
@SuppressWarnings("Duplicates")
public class MysqlDBTest {
    private static DBConnectionPool pool;
    private static EntityQuery<User> userEntityQuery;

    @BeforeClass
    public static void beforeClass() throws DBException, DBMigrationException {
        ConnectionParametersList params = new ConnectionParametersList();
        params.add(new ConnectionParameter("useUnicode", "true"));
        params.add(new ConnectionParameter("characterEncoding", "utf-8"));
        params.add(new ConnectionParameter("useSSL", "false"));
        params.add(new ConnectionParameter("allowPublicKeyRetrieval", "true"));
        params.add(new ConnectionParameter("serverTimezone", "UTC"));
        MysqlDriver driver = new MysqlDriver("com.mysql.cj.jdbc.Driver",
                "root",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                3306,
                params);
        //setup migrations
        pool = new DBConnectionPool(driver);
        DBMigrationSaver saver = new DBMigrationSaver(pool, "migrations");
        DBMigrationSystem system = new DBMigrationSystem(
                pool,
                new MysqlSystemMigrationList(),
                saver
        );
        system.setup();
        assertEquals(1, saver.read().size());
        userEntityQuery = WizEntityQueryUtils.getEntityQuery(pool, User.class);
    }

    @After
    public void after() throws DBException {
        userEntityQuery.getDeleteQuery().execute();
    }

    @Test
    public void insertSymbolsTest() throws DBException {
        User user = insertTestUser("[_['''```~\"");
        assertNotNull(user);
    }

    @Test
    public void getFieldNameTest() throws DBException {
        String fieldName = userEntityQuery.getSelectQuery().getFieldName(
                "login", User.class
        );
        assertNotNull(fieldName);
        assertEquals("A_users.login", fieldName);
    }

    @Test
    public void customSelectTest() throws DBException {
        insertTestUser("hi");
        WizDBSelect<User> query = userEntityQuery.getSelectQuery();
        query.where(query.getFieldName("login") + "=" + "'hi'");
        User user = query.getOne();
        assertNotNull(user);
    }

    @Test
    public void preparedDeleteTest() throws DBException {
        User user = insertTestUser("hi");
        userEntityQuery.getDeleteQuery()
                .setWhereType(DBWhereType.OR)
                .where("login = 'hi'")
                .where("password", user.getPassword())
                .execute();
        assertNull(userEntityQuery.getSelectQuery().where("login", "hi").getOne());
    }

    //update database test
    @Test
    public void dbUpdateTest() throws DBException {
        User user = insertTestUser("test1");
        user.setPassword("NEW PASSWORD");
        userEntityQuery.getUpdateQuery().execute(user);
        List<User> dbEntities = userEntityQuery.getSelectQuery().execute();
        assertEquals((dbEntities.get(0)).getPassword(), user.getPassword());
    }


    //select from database test
    @Test
    public void dbSelectTest() throws DBException {
        User user1 = insertTestUser("test1");
        User user2 = insertTestUser("test2");

        List<User> dbEntities = userEntityQuery.getSelectQuery().execute();
        assertFalse(dbEntities.isEmpty());

        List<User> usersList = userEntityQuery.getSelectQuery().where("id", user1.getId()).execute();
        assertEquals(1, usersList.size());

        User user = userEntityQuery.getSelectQuery().where("id", user2.getId()).getOne();
        assertNotNull(user);
        assertEquals(user.getId(), user2.getId());
    }

    //insert to database test
    @Test
    public void dbInsertTest() throws DBException {
        User user = new User();
        user.setLogin("login");
        user.setPassword("password");
        userEntityQuery.getInsertQuery().execute(user);
        assertTrue(user.getId() != 0);
        User selectedUser = userEntityQuery.getSelectQuery().where("id", user.getId()).getOne();
        assertNotNull(selectedUser);
        assertEquals(user.getId(), selectedUser.getId());
    }

    //delete from database test
    @Test
    public void dbDeleteTest() throws DBException {
        User user1 = insertTestUser("test1");
        insertTestUser("test2");
        userEntityQuery.getDeleteQuery().where("id", user1.getId()).execute();
        List<User> list = userEntityQuery.getSelectQuery().execute();
        assertEquals(1, list.size());
    }

    /**
     * Insert to user and return last insert id
     */
    @Test
    public void dbInsertWithKey() throws DBException {
        User user = new User();
        user.setLogin("login");
        user.setPassword("password");
        userEntityQuery.getInsertQuery().execute(user);
        assertTrue(user.getId() != 0);
    }

    @Test
    public void joinTest() throws DBException {
        JoinT1 t1Data = new JoinT1();
        t1Data.setData("Hi");
        WizEntityQueryUtils.getInsertQuery(pool, JoinT1.class).execute(t1Data);

        JoinT2 t2Data = new JoinT2();
        t2Data.setId_t1(t1Data.getId());
        t2Data.setName("Hi from t2");
        WizEntityQueryUtils.getInsertQuery(pool, JoinT2.class).execute(t2Data);

        JoinT1 result = WizEntityQueryUtils.getSelectQuery(pool, JoinT1.class).joinTables(JoinTypes.INNER).getOne();
        assertNotNull(result);
        assertNotNull(result.getTable2());

        WizEntityQueryUtils.getDeleteQuery(pool, JoinT1.class).execute();
        WizEntityQueryUtils.getDeleteQuery(pool, JoinT2.class).execute();
    }

    @Test
    public void limitTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        List<User> execute = userEntityQuery.getSelectQuery().setLimit(5).execute();
        assertEquals(5, execute.size());

        execute = userEntityQuery.getSelectQuery().execute();
        assertEquals(30, execute.size());
        assertEquals((execute.get(0)).getLogin(), "test" + 0);

        execute = userEntityQuery.getSelectQuery().setLimit(5, 5).execute();
        assertEquals(5, execute.size());
        assertEquals((execute.get(0)).getLogin(), "test" + 5);
    }

    @Test
    public void orderByTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        List<User> execute = userEntityQuery.getSelectQuery().orderBy("id", DBOrderType.DESC).execute();
        assertTrue((execute.get(0)).getId() >= (execute.get(1)).getId());

        execute = userEntityQuery.getSelectQuery().orderBy("id", DBOrderType.ASC).execute();
        assertTrue((execute.get(0)).getId() <= (execute.get(1)).getId());
    }

    @Test
    public void groupByTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        List<User> execute = userEntityQuery
                .getSelectQuery()
                .addAggregate(new AggregateField("id", "users", AggregateFunctionTypes.COUNT))
                .addAggregate(new AggregateField("login", "users", AggregateFunctionTypes.COUNT))
                .groupBy("password").execute();
        assertEquals(1, execute.size());
        assertEquals(30, (execute.get(0)).getId());
    }


    @Test
    public void getCountTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        User user = userEntityQuery.getSelectQuery().getOne();
        long count = userEntityQuery.getSelectQuery().getCount();
        assertEquals(30, count);
        count = userEntityQuery.getSelectQuery().where("id", user.getId()).getCount();
        assertEquals(1, count);
    }

    @Test
    public void customQueryTest() throws DBException, SQLException {
        insertTestUser("test");
        try (DBConnection connection = pool.getConnection()) {
            ResultSet resultSet = connection.executeSqlQuery("Select count(1) from users");
            resultSet.next();
            long count = resultSet.getLong(1);
            assertEquals(1, count);
        }
    }

    private User insertTestUser(String login) throws DBException {
        User user = new User();
        user.setLogin(login);
        user.setPassword("password");
        userEntityQuery.getInsertQuery().execute(user);
        return user;
    }
}
