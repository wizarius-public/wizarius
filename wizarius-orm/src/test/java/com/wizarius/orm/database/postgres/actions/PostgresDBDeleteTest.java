package com.wizarius.orm.database.postgres.actions;

import com.wizarius.orm.JunitTestUtils;
import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.connection.DBConnection;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.entities.AllTypeEntity;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-13
 * Time: 14:14
 */
public class PostgresDBDeleteTest {
    private AllTypeEntity entity;
    private DBConnectionPool pool;

    @Before
    public void before() throws DBException {
        entity = JunitTestUtils.generateObject();
        PostgresDriver driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        pool = new DBConnectionPool(driver);
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
    }

    @Test
    public void deleteAllTest() throws DBException {
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertFalse(result.isEmpty());

        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(0, result.size());
    }

    @Test
    public void toSqlTest() throws DBException {
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertFalse(result.isEmpty());
        String sql = WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).toSQLQuery();
        assertNotNull(sql);
    }

    @Test
    public void deleteByIDEntity() throws DBException {
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(10, result.size());
        for (int i = 0; i < 5; i++) {
            WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute(result.get(i));
            String sql = WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).toSQLQuery(result.get(i));
            assertNotNull(sql);
        }
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(5, result.size());
    }

    @Test
    public void deleteByDate() throws DBException {
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(10, result.size());
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).where("date_element", entity.getDateElement()).execute();
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(0, result.size());
    }

    @Test
    public void deleteConnectionUseTest() throws DBException {
        for (int i = 0; i < 5; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(5, pool.getAvailableConnectionCount());

        DBConnection connection = pool.getConnection();
        assertEquals(4, pool.getAvailableConnectionCount());
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute(result.get(0), connection);
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(4, result.size());
        assertEquals(4, pool.getAvailableConnectionCount());
        connection.close();
        assertEquals(5, pool.getAvailableConnectionCount());
    }

    @Test
    public void deleteByLocalDate() throws DBException {
        AllTypeEntity entity = new AllTypeEntity()
                .setLocalDate(LocalDate.now());
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(10, result.size());
        assertNotNull(result.get(0).getLocalDate());
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).where("local_date", entity.getLocalDate()).execute();
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(0, result.size());
    }

    @Test
    public void deleteByLocalDateTime() throws DBException {
        AllTypeEntity entity = new AllTypeEntity()
                .setLocalDateTime(LocalDateTime.now());
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(10, result.size());
        assertNotNull(result.get(0).getLocalDateTime());
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).where("local_datetime", entity.getLocalDateTime()).execute();
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        assertEquals(0, result.size());
    }
}