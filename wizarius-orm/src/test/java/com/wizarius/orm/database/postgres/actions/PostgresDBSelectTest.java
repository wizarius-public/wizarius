package com.wizarius.orm.database.postgres.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wizarius.orm.JunitTestUtils;
import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityMapper;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.connection.DBConnection;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.data.DBOrderType;
import com.wizarius.orm.database.data.DBSignType;
import com.wizarius.orm.database.data.DBWhereType;
import com.wizarius.orm.database.data.PageResult;
import com.wizarius.orm.database.entityreader.EntityQuery;
import com.wizarius.orm.database.entityreader.WizTypeReference;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.entities.*;
import com.wizarius.orm.entities.sc.AbstractMainEntity;
import com.wizarius.orm.utils.WizJsonUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-13
 * Time: 14:28
 */
public class PostgresDBSelectTest {
    private static DBConnectionPool pool;
    private AllTypeEntity entity;

    @BeforeClass
    public static void beforeClass() throws DBException {
        PostgresDriver driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        pool = new DBConnectionPool(driver);
    }

    @Before
    public void setUp() throws DBException {
        entity = JunitTestUtils.generateObject();
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        for (int i = 0; i < 10; i++) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
    }

    @Test
    public void selectByShortArray() throws DBException {
        short[] array = {0, 1, 2, 3, 4, 5};
        entity.setShortArray(array);
        entity.setShortElement(Short.MAX_VALUE);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("short_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("short_element", Short.MAX_VALUE, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectByIntArray() throws DBException {
        int[] array = {0, 1, 2, 3, 4, 5};
        entity.setIntArray(array);
        entity.setIntElement(10232);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("int_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("int_element", 10232, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // select in query
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereIn("id", new int[]{entity.getId(), Integer.MAX_VALUE - 1}).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // select not in
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereNotIn("id", new int[]{entity.getId()}).execute();
        assertNotNull(result);
        assertEquals(10, result.size());
    }

    @Test
    public void selectByLongArray() throws DBException {
        long[] array = {0, 1, 2, 3, 4, 5};
        entity.setLongArray(array);
        entity.setLongElement(9992139L);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("long_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("long_element", 9992139, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectByFloatArray() throws DBException {
        float[] array = {0.3f, 1.3f};
        entity.setFloatArray(array);
        entity.setFloatElement(0.932f);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("float_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("float_element", 0.932f, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectByDoubleArray() throws DBException {
        double[] array = {0.3213, 1.124};
        entity.setDoubleArray(array);
        entity.setDoubleElement(0.9123);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("double_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("double_element", 0.9123, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectByBigDecimalArray() throws DBException {
        BigDecimal[] array = {new BigDecimal("0.3213"), new BigDecimal("1.124")};
        entity.setBigDecimalArray(array);
        entity.setBigDecimalElement(new BigDecimal("102.3"));
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        String fieldName = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).getFieldName("bigdecimal_array");
        // check bigdecimal array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .where(fieldName, array, DBSignType.EQUALS)
                .execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element value
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("bigdecimal_element", new BigDecimal("102.3"), DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectChartArray() throws DBException {
        char[] array = {'c', 'b'};
        entity.setCharArray(array);
        entity.setCharElement('d');
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check char array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("char_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check char element
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("char_element", 'd', DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectBooleanArray() throws DBException {
        boolean[] array = {true, true, true};
        entity.setBooleanArray(array);
        entity.setBooleanElement(false);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        //check array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("boolean_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        //check element
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("boolean_element", false, DBSignType.EQUALS).where("id", entity.getId()).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectEnumArray() throws DBException {
        StatusEnum[] array = {StatusEnum.ON, null, StatusEnum.OFF};
        entity.setEnumArray(array);
        entity.setEnumElement(StatusEnum.OFF);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("enum_array", array, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("enum_element", StatusEnum.OFF, DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        assertSame(StatusEnum.ON, result.get(0).getEnumArray()[0]);
        assertSame(null, result.get(0).getEnumArray()[1]);
        assertSame(StatusEnum.OFF, result.get(0).getEnumArray()[2]);
        // where in
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereIn("enum_element", new StatusEnum[]{StatusEnum.ON, StatusEnum.OFF}).execute();
        assertNotNull(result);
    }

    @Test
    public void selectStringTest() throws DBException {
        String[] array = {"wizarius", "orm"};
        entity.setStringArray(array);
        entity.setStringElement("orm-test");
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("string_array", entity.getStringArray(), DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("string_element", "orm-test", DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectByDate() throws DBException {
        Date[] array = {new Date(System.currentTimeMillis() - 86400 * 1000), new Date(System.currentTimeMillis())};
        entity.setDateArray(array);
        entity.setDateElement(new Date(System.currentTimeMillis()));
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("date_array", entity.getDateArray(), DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("date_element", entity.getDateElement(), DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(11, result.size());
    }

    @Test
    public void selectByTimestamp() throws DBException {
        Timestamp[] array = {new Timestamp(System.currentTimeMillis() - 86400 * 1000), new Timestamp(System.currentTimeMillis())};
        entity.setTimestampArray(array);
        entity.setTimestampElement(new Timestamp(System.currentTimeMillis()));
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check array
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("timestamp_array", entity.getTimestampArray(), DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check element
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("timestamp_element", entity.getTimestampElement(), DBSignType.EQUALS).execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void customQueryTest() throws DBException {
        EntityQuery<AllTypeEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AllTypeEntity.class);
        List<AllTypeEntity> result = query.getSelectQuery().execute("select * from all_types");
        assertNotNull(result);
    }

    @Test
    public void selectByQueryTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id = " + entity.getId()).execute();
        assertEquals(1, result.size());
    }

    @Test
    public void selectNullValues() throws DBException {
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        int[] array = {0, 1, 2, 3, 4, 5};
        for (int i = 0; i < 10; i++) {
            AllTypeEntity entity = JunitTestUtils.generateObject();
            if (i % 2 == 0) {
                entity.setIntArray(null);
            } else {
                entity.setIntArray(array);
            }
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereIsNotNull("int_array").execute();
        assertNotNull(result);
        assertEquals(5, result.size());
        assertNotNull(result.get(0).getIntArray());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereNull("int_array").execute();
        assertNotNull(result);
        assertEquals(5, result.size());
        assertNull(result.get(0).getIntArray());
    }

    @Test
    public void selectBetween() throws DBException {
        List<AllTypeEntity> items = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        long millis = System.currentTimeMillis() - 86400000L * items.size();
        for (int i = 0; i < items.size(); i++) {
            AllTypeEntity item = items.get(i);
            item.setIntElement(i + 1);
            item.setTimestampElement(new Timestamp(millis + ((i + 1) * 86400000L)));
            WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(item);
        }
        // check array select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereBetween("int_element", 1, 5).execute();
        assertNotNull(result);
        assertEquals(5, result.size());
        // check element select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereBetween("int_element", 1, 8).execute();
        assertNotNull(result);
        assertEquals(8, result.size());
        // check between timestamp
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).whereBetween("timestamp_element",
                new Timestamp(millis + ((8) * 86400000L)),
                new Timestamp(millis + ((10) * 86400000L))).execute();
        assertNotNull(result);
        assertEquals(3, result.size());
    }

    @Test
    public void isNewTest() throws DBException {
        EntityQuery<AllTypeEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AllTypeEntity.class);
        assertFalse(query.isNew(entity));
        entity.setId(0);
        assertTrue(query.isNew(entity));
    }

    @Test
    public void getIDTableName() throws DBException {
        EntityQuery<AllTypeEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AllTypeEntity.class);
        String field = query.getIDDbFieldName();
        assertNotNull(field);
        assertEquals("id", field);
    }

    @Test
    public void getFieldValue() throws DBException {
        EntityQuery<AllTypeEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AllTypeEntity.class);
        Object value = query.getIDValue(entity);
        assertNotNull(value);
        assertEquals(value, entity.getId());
    }

    @Test
    public void selectMultipleWhereType() throws DBException {
        EntityQuery<AllTypeEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AllTypeEntity.class);
        List<AllTypeEntity> items = query.getSelectQuery().execute();
        // where id in (?,?) or id > 0 and id is not null or id is null
        List<AllTypeEntity> whereItems = query.getSelectQuery()
                .whereIn("id", new int[]{items.get(0).getId(), items.get(1).getId()})

                .setWhereType(DBWhereType.OR)
                .where("id", 0, DBSignType.GREATER)

                .setWhereType(DBWhereType.AND)
                .whereIsNotNull("id")

                .setWhereType(DBWhereType.OR)
                .whereNull("id")
                .execute();
        assertEquals(items.size(), whereItems.size());
    }

    @Test
    public void selectSuperClassTest() throws DBException {
        EntityQuery<AbstractMainEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AbstractMainEntity.class);
        query.getDeleteQuery().execute();
        for (int i = 0; i < 100; i++) {
            AbstractMainEntity e = new AbstractMainEntity();
            e.setDescription("Test - " + i);
            e.setValue(new BigDecimal("10" + i));
            e.setStatus(i % 2 == 0 ? StatusEnum.ON : null);
            query.getInsertQuery().execute(e);
        }
        List<AbstractMainEntity> items = query.getSelectQuery()
                .where("value", 100, DBSignType.GREATER)
                .execute();
        assertNotNull(items);
        assertEquals(99, items.size());
    }

    @Test
    public void selectGroupQuery() throws DBException {
        EntityQuery<AbstractMainEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AbstractMainEntity.class);
        query.getDeleteQuery().execute();
        for (int i = 0; i < 100; i++) {
            AbstractMainEntity e = new AbstractMainEntity();
            e.setDescription("Test - " + i);
            e.setValue(new BigDecimal("10" + i));
            e.setStatus(i % 2 == 0 ? StatusEnum.ON : null);
            query.getInsertQuery().execute(e);
        }
        List<AbstractMainEntity> items = query.getSelectQuery()
                .where("value", 100, DBSignType.GREATER)
                .startWhereGroup()
                .where("value", 100, DBSignType.GREATER)
                .setWhereType(DBWhereType.OR)
                .where("value", 100, DBSignType.GREATER)
                .startWhereGroup()
                .where("value", 100, DBSignType.GREATER)
                .setWhereType(DBWhereType.AND)
                .where("value", 100, DBSignType.GREATER)
                .setWhereType(DBWhereType.AND)
                .where("value", 100, DBSignType.GREATER)
                .setWhereType(DBWhereType.OR)
                .where("value", 100, DBSignType.GREATER)
                .endWhereGroup()
                .endWhereGroup()
                .setWhereType(DBWhereType.AND)
                .where("value", 100, DBSignType.GREATER)
                .setWhereType(DBWhereType.AND)
                .where("value", 100, DBSignType.GREATER)
                .execute();
        assertNotNull(items);
    }

    @Test
    public void selectPage() throws DBException {
        EntityQuery<AbstractMainEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AbstractMainEntity.class);
        query.getDeleteQuery().execute();
        for (int i = 0; i < 100; i++) {
            AbstractMainEntity e = new AbstractMainEntity();
            e.setDescription("Test - " + i);
            e.setValue(new BigDecimal("10" + i));
            e.setStatus(i % 2 == 0 ? StatusEnum.ON : null);
            query.getInsertQuery().execute(e);
        }
        PageResult<AbstractMainEntity> items = query.getSelectQuery()
                .orderBy("id", DBOrderType.DESC)
                .setLimit(5)
                .executePage();
        assertNotNull(items);
        assertEquals(5, items.getRows().size());
        assertEquals(100, items.getTotalCount());

        items = query.getSelectQuery()
                .where("value", 105, DBSignType.GREATER)
                .orderBy("id", DBOrderType.DESC)
                .setLimit(5)
                .executePage();
        assertNotNull(items);
        assertEquals(5, items.getRows().size());
        assertEquals(94, items.getTotalCount());

        try (DBConnection connection = pool.getConnection()) {
            items = query.getSelectQuery()
                    .where("value", 105, DBSignType.GREATER)
                    .orderBy("id", DBOrderType.DESC)
                    .setLimit(5)
                    .executePage(connection);
            assertEquals(5, items.getRows().size());
            assertEquals(94, items.getTotalCount());
        }
    }

    @Test
    public void genericSelectTest() throws DBException {
        EntityQuery<GenericTypesEntity<ProductEntity, ProductPropertiesEntity>> query = WizEntityQueryUtils.getEntityQuery(pool, new WizTypeReference<GenericTypesEntity<ProductEntity, ProductPropertiesEntity>>() {
        });
        query.getDeleteQuery().execute();
        for (int i = 0; i < 100; i++) {
            GenericTypesEntity<ProductEntity, ProductPropertiesEntity> e = new GenericTypesEntity<>();
            ProductEntity product = new ProductEntity()
                    .setName("test-" + i);
            ProductPropertiesEntity property = new ProductPropertiesEntity()
                    .setColor("color" + i)
                    .setSize(new String[]{"red"});
            e.setGenericJson1(product);
            e.setGenericJson2(property);
            e.setGenericJsonArray1(new ProductEntity[]{product});
            e.setGenericJsonArray2(new ProductPropertiesEntity[]{property});
            query.getInsertQuery().execute(e);
        }
        List<GenericTypesEntity<ProductEntity, ProductPropertiesEntity>> items = query.getSelectQuery().execute();
        assertNotNull(items);
        assertFalse(items.isEmpty());
        assertTrue(items.get(0).getGenericJson1() instanceof ProductEntity);
        assertTrue(items.get(0).getGenericJson2() instanceof ProductPropertiesEntity);
        assertTrue(items.get(0).getGenericJsonArray1()[0] instanceof ProductEntity);
        assertTrue(items.get(0).getGenericJsonArray2()[0] instanceof ProductPropertiesEntity);
    }

    @Test
    public void resultSetToInstance() throws DBException, SQLException {
        EntityQuery<ProductEntity> query = WizEntityQueryUtils.getEntityQuery(pool, ProductEntity.class);
        query.getDeleteQuery().execute();
        for (int i = 0; i < 100; i++) {
            GenericTypesEntity<ProductEntity, ProductPropertiesEntity> e = new GenericTypesEntity<>();
            ProductEntity product = new ProductEntity()
                    .setName("test-" + i);
            query.getInsertQuery().execute(product);
        }
        try (DBConnection connection = pool.getConnection()) {
            PreparedStatement statement = connection.createPrepareStatement("select * from products where id >= (select max(id) from products)");
            ResultSet result = statement.executeQuery();
            List<ProductEntity> products = WizEntityMapper.initializeEntities(
                    result,
                    pool.getDialect(),
                    ProductEntity.class
            );
            assertNotNull(products);
            assertFalse(products.isEmpty());
        }
    }

    @Test
    public void selectLocalDate() throws DBException {
        LocalDate now = LocalDate.now();
        entity.setLocalDate(now);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check element select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .where("local_date", now, DBSignType.EQUALS)
                .execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check between select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .whereBetween("local_date", now, now.plusDays(1))
                .execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void selectLocalDateTime() throws DBException {
        LocalDateTime now = LocalDateTime.now();
        entity.setLocalDateTime(now);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        // check element select
        List<AllTypeEntity> result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .where("local_datetime", now, DBSignType.EQUALS)
                .execute();
        assertNotNull(result);
        assertEquals(1, result.size());
        // check between select
        result = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .whereBetween("local_datetime", now, now.plusSeconds(500))
                .execute();
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void serializeLocalDateTest() throws JsonProcessingException {
        LocalDateTime now = LocalDateTime.now();
        entity.setLocalDateTime(now);
        entity.setLocalDate(now.toLocalDate());
        String json = WizJsonUtils.toJsonWithError(entity);
        assertNotNull(json);
        AllTypeEntity deserialized = WizJsonUtils.fromJsonWithError(json, AllTypeEntity.class);
        assertNotNull(deserialized);
        assertNotNull(deserialized.getLocalDateTime());
        assertNotNull(deserialized.getLocalDate());
        assertEquals(entity.getLocalDate(), deserialized.getLocalDate());
        assertEquals(entity.getLocalDateTime(), deserialized.getLocalDateTime());
    }
}