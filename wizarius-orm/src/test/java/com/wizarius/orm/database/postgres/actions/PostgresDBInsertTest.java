package com.wizarius.orm.database.postgres.actions;

import com.wizarius.orm.JunitTestUtils;
import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.connection.DBConnection;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.entities.AllTypeEntity;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 18:32
 */
public class PostgresDBInsertTest {
    private static DBConnectionPool pool;
    private AllTypeEntity entity;

    @BeforeClass
    public static void beforeClass() throws DBException {
        PostgresDriver driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        pool = new DBConnectionPool(driver);
    }

    @Before
    public void setUp() {
        entity = JunitTestUtils.generateObject();
    }

    @Test
    public void toSqlTest() throws DBException {
        String sql = WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).toSQLQuery(entity);
        assertNotNull(sql);
    }

    @Test
    public void allNullToSqlTest() throws DBException {
        AllTypeEntity nullEntity = new AllTypeEntity();
        String sql = WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).toSQLQuery(nullEntity);
        assertNotNull(sql);
    }

    @Test
    public void executeQueryTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
    }

    @Test
    public void executeAllNullTest() throws DBException {
        AllTypeEntity entity = new AllTypeEntity();
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        AllTypeEntity selectedItem = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .where("id", entity.getId())
                .getOne();
        assertNotNull(selectedItem);
    }

    @Test
    public void executeCollectionTest() throws DBException {
        DBConnection connection = pool.getConnection();
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        List<AllTypeEntity> entities = generateEntities(100);
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entities, connection, 10);
        assertTrue(entities.get(0).getId() != 0);
        assertTrue(entities.get(entities.size() - 1).getId() != 0);
    }

    @Test
    @Ignore
    public void singleArraySaveVSList() throws DBException {
        int size = 5000;
        // save list by one query
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        List<AllTypeEntity> entities = generateEntities(size);
        long start = System.currentTimeMillis();
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entities);
        System.out.println("Array save spent " + (System.currentTimeMillis() - start) + " ms");
        // save list one by one
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        entities = generateEntities(size);
        start = System.currentTimeMillis();
        for (AllTypeEntity entity : entities) {
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        System.out.println("Single save spent " + (System.currentTimeMillis() - start) + " ms");
    }

    @Test
    public void insertWithoutConnectionClose() throws DBException {
        assertEquals(5, pool.getAvailableConnectionCount());
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(JunitTestUtils.generateObject());
        assertEquals(5, pool.getAvailableConnectionCount());
        DBConnection connection = pool.getConnection();
        assertEquals(4, pool.getAvailableConnectionCount());
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(JunitTestUtils.generateObject(), connection);
        assertEquals(4, pool.getAvailableConnectionCount());
        connection.close();
        assertEquals(5, pool.getAvailableConnectionCount());
    }

    private List<AllTypeEntity> generateEntities(int size) {
        List<AllTypeEntity> entities = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            entities.add(JunitTestUtils.generateObject());
        }
        return entities;
    }
}