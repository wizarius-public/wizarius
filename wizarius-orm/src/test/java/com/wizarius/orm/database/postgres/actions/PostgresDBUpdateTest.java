package com.wizarius.orm.database.postgres.actions;

import com.wizarius.orm.JunitTestUtils;
import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.data.DBOrderType;
import com.wizarius.orm.database.entityreader.EntityQuery;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.entities.AllTypeEntity;
import com.wizarius.orm.entities.StatusEnum;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertArrayEquals;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-07
 * Time: 10:12
 * <p>
 * TODO
 * Нужно сделать проверку на все поля update
 */
public class PostgresDBUpdateTest {
    private static DBConnectionPool pool;
    private AllTypeEntity entity;

    @BeforeClass
    public static void beforeClass() throws DBException {
        PostgresDriver driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        pool = new DBConnectionPool(driver);
    }

    @Before
    public void setUp() {
        entity = JunitTestUtils.generateObject();
    }

    @Test
    public void toSqlTest() throws DBException {
        String sql = WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).toSQLQuery(entity);
        assertNotNull(sql);
    }

    @Test
    public void allNullToSqlTest() throws DBException {
        AllTypeEntity nullEntity = new AllTypeEntity();
        String sql = WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).toSQLQuery(nullEntity);
        assertNotNull(sql);
    }

    @Test
    public void updateByteTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setByteElement((byte) 99);
        entity.setByteArray("wizarius".getBytes());
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(99, entity.getByteElement());
        assertArrayEquals("wizarius".getBytes(), entity.getByteArray());
    }

    @Test
    public void updateShortArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setShortElement((short) 99);
        entity.setShortArray(new short[]{777, 777});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(99, entity.getShortElement());
        assertArrayEquals(new short[]{777, 777}, entity.getShortArray());
    }

    @Test
    public void updateIntArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setIntElement(99);
        entity.setIntArray(new int[]{777, 777});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(99, entity.getIntElement());
        assertArrayEquals(new int[]{777, 777}, entity.getIntArray());
    }

    @Test
    public void updateLongArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setLongElement(99L);
        entity.setLongArray(new long[]{777L, 777L});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(99, entity.getLongElement());
        assertArrayEquals(new long[]{777L, 777L}, entity.getLongArray());
    }

    @Test
    public void updateDoubleArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setDoubleElement(312.777);
        entity.setDoubleArray(new double[]{777.312, 777.312});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(312.777, entity.getDoubleElement());
        assertArrayEquals(new double[]{777.312, 777.312}, entity.getDoubleArray(), 0);
    }

    @Test
    public void updateFloatArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setFloatElement(312.777f);
        entity.setFloatArray(new float[]{777.312f, 777.312f});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(312.777f, entity.getFloatElement());
        assertArrayEquals(new float[]{777.312f, 777.312f}, entity.getFloatArray(), 0);
    }

    @Test
    public void updateBooleanArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setBooleanElement(false);
        entity.setBooleanArray(new boolean[]{true, false, true, false});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertFalse(entity.isBooleanElement());
        assertArrayEquals(new boolean[]{true, false, true, false}, entity.getBooleanArray());
    }

    @Test
    public void updateStringArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setStringElement("wizarius");
        entity.setStringArray(new String[]{"WIZARIUS", "ORM"});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals("wizarius", entity.getStringElement());
        assertArrayEquals(new String[]{"WIZARIUS", "ORM"}, entity.getStringArray());
    }

    @Test
    public void updateCharStringArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setCharElement('w');
        entity.setCharArray(new char[]{'W', 'O'});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals('w', entity.getCharElement());
        assertArrayEquals(new char[]{'W', 'O'}, entity.getCharArray());
    }

    @Test
    public void enumArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setEnumElement(StatusEnum.OFF);
        entity.setEnumArray(new StatusEnum[]{StatusEnum.ON, StatusEnum.OFF, StatusEnum.OFF});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(StatusEnum.OFF, entity.getEnumElement());
        assertArrayEquals(new StatusEnum[]{StatusEnum.ON, StatusEnum.OFF, StatusEnum.OFF}, entity.getEnumArray());
    }

    @Test
    public void bigDecimalArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        entity.setBigDecimalElement(new BigDecimal("999.999"));
        entity.setBigDecimalArray(new BigDecimal[]{new BigDecimal("888.888"), new BigDecimal("999.999")});
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(new BigDecimal("999.999"), entity.getBigDecimalElement());
        assertArrayEquals(new BigDecimal[]{new BigDecimal("888.888"), new BigDecimal("999.999")}, entity.getBigDecimalArray());
    }

    @Test
    public void dateArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        Date dateElement = new Date(System.currentTimeMillis());
        Date[] dateArray = {new Date(System.currentTimeMillis() - 86400 * 1000), new Date(System.currentTimeMillis())};
        entity.setDateElement(dateElement);
        entity.setDateArray(dateArray);
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(dateElement, entity.getDateElement());
        assertArrayEquals(dateArray, entity.getDateArray());
    }

    @Test
    public void timestampArrayTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        Timestamp timestampElement = new Timestamp(System.currentTimeMillis());
        Timestamp[] timestampArray = {new Timestamp(System.currentTimeMillis() - 86400 * 1000), new Timestamp(System.currentTimeMillis())};
        entity.setTimestampElement(timestampElement);
        entity.setTimestampArray(timestampArray);
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(entity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNotNull(selectedEntity);
        assertEquals(timestampElement, entity.getTimestampElement());
        assertArrayEquals(timestampArray, entity.getTimestampArray());
    }

    @Test
    public void allNullSaveTest() throws DBException {
        WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        assertTrue(entity.getId() != 0);
        AllTypeEntity newEntity = new AllTypeEntity();
        newEntity.setId(entity.getId());
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).execute(newEntity);
        AllTypeEntity selectedEntity = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).where("id", entity.getId()).getOne();
        assertNull(selectedEntity.getEnumArray());
        assertNull(selectedEntity.getIntArray());
        assertNull(selectedEntity.getShortArray());
        assertNull(selectedEntity.getLongArray());
        assertNull(selectedEntity.getFloatArray());
        assertNull(selectedEntity.getDoubleArray());
        assertNull(selectedEntity.getBooleanArray());
        assertNull(selectedEntity.getByteArray());
        assertNull(selectedEntity.getCharArray());
        assertNull(selectedEntity.getStringArray());
        assertNull(selectedEntity.getBigDecimalArray());
        assertNull(selectedEntity.getDateArray());
        assertNull(selectedEntity.getTimestampArray());
    }

    @Test
    public void executeCustomUpdateTest() throws DBException {
        WizEntityQueryUtils.getDeleteQuery(pool, AllTypeEntity.class).execute();
        for (int i = 0; i < 10; i++) {
            entity.setId(0);
            WizEntityQueryUtils.getInsertQuery(pool, AllTypeEntity.class).execute(entity);
        }
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class).executeCustom(new HashMap<String, Object>() {{
            put("int_element", 777);
            put("timestamp_element", null);
            put("timestamp_array", null);
        }});
        List<AllTypeEntity> allItems = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class).execute();
        for (AllTypeEntity item : allItems) {
            assertEquals(777, item.getIntElement());
            assertNull(item.getTimestampArray());
            assertNull(item.getTimestampElement());
        }
        WizEntityQueryUtils.getUpdateQuery(pool, AllTypeEntity.class)
                .where("id", allItems.get(0).getId())
                .executeCustom(new HashMap<String, Object>() {{
                    put("int_element", 999);
                    put("timestamp_element", new Timestamp(System.currentTimeMillis()));
                    put("timestamp_array", new Timestamp[]{new Timestamp(System.currentTimeMillis())});
                }});
        // check updated item
        AllTypeEntity finalItem = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .where("id", allItems.get(0).getId())
                .getOne();
        assertEquals(999, finalItem.getIntElement());
        assertNotNull(finalItem.getTimestampArray());
        assertNotNull(finalItem.getTimestampElement());
        // check other item
        finalItem = WizEntityQueryUtils.getSelectQuery(pool, AllTypeEntity.class)
                .where("id", allItems.get(1).getId())
                .getOne();
        assertEquals(777, finalItem.getIntElement());
        assertNull(finalItem.getTimestampArray());
        assertNull(finalItem.getTimestampElement());
    }

    @Test
    public void updateIsolationTest() throws DBException {
        EntityQuery<AllTypeEntity> query = WizEntityQueryUtils.getEntityQuery(pool, AllTypeEntity.class);
        List<AllTypeEntity> items = query.getSelectQuery().orderBy("id", DBOrderType.DESC).execute();
        int i = 0;
        for (AllTypeEntity item : items) {
            item.setIntElement(i++);
            query.getUpdateQuery().execute(item);
        }
        items = query.getSelectQuery().orderBy("id", DBOrderType.DESC).execute();
        i = 0;
        for (AllTypeEntity item : items) {
            assertEquals(item.getIntElement(), i++);
        }
    }
}