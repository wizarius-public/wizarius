package com.wizarius.orm.database.entityreader;

import com.wizarius.orm.JunitTestUtils;
import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityReader;
import com.wizarius.orm.entities.AllTypeEntity;
import com.wizarius.orm.entities.sc.AbstractMainEntity;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Vladyslav Shyshkin
 * Date: 22.03.2020
 * Time: 18:17
 */
public class WizEntityReaderTest {
    private final AllTypeEntity object = JunitTestUtils.generateObject();

    @Before
    public void before() throws DBException {
        assertEquals("all_types", WizEntityReader.parseEntity(object.getClass()).getTableName());
    }

    /**
     * Test field
     *
     * @param dbFieldName   name of field in database
     * @param javaFieldName name of field in java cclass
     * @param exceptedType  name of excepted type
     * @param dataToCheck   data to check in field
     * @throws Exception on unable to access
     */
    private void testField(String dbFieldName,
                           String javaFieldName,
                           DBSupportedTypes exceptedType,
                           Object dataToCheck) throws Exception {
        DBParsedFieldsList fieldsMap = WizEntityReader.parseEntity(object.getClass());
        assertNotNull(fieldsMap.getFieldByDBFieldName(dbFieldName));
        DBParsedField parsedField = fieldsMap.getFieldByFieldName(javaFieldName);
        assertNotNull(parsedField);
        assertSame(parsedField.getFieldType(), exceptedType);
        parsedField.getField().setAccessible(true);
        assertEquals(parsedField.getField().get(object), dataToCheck);
    }

    @Test
    public void testByte() throws Exception {
        testField("byte_element", "byteElement", DBSupportedTypes.BYTE, object.getByteElement());
        testField("byte_array", "byteArray", DBSupportedTypes.BYTE_ARRAY, object.getByteArray());
    }

    @Test
    public void testShort() throws Exception {
        testField("short_element", "shortElement", DBSupportedTypes.SHORT, object.getShortElement());
        testField("short_array", "shortArray", DBSupportedTypes.SHORT_ARRAY, object.getShortArray());
    }

    @Test
    public void testInt() throws Exception {
        testField("int_element", "intElement", DBSupportedTypes.INTEGER, object.getIntElement());
        testField("int_array", "intArray", DBSupportedTypes.INTEGER_ARRAY, object.getIntArray());
    }

    @Test
    public void testLong() throws Exception {
        testField("long_element", "longElement", DBSupportedTypes.LONG, object.getLongElement());
        testField("long_array", "longArray", DBSupportedTypes.LONG_ARRAY, object.getLongArray());
    }

    @Test
    public void testFloat() throws Exception {
        testField("float_element", "floatElement", DBSupportedTypes.FLOAT, object.getFloatElement());
        testField("float_array", "floatArray", DBSupportedTypes.FLOAT_ARRAY, object.getFloatArray());
    }

    @Test
    public void testDouble() throws Exception {
        testField("double_element", "doubleElement", DBSupportedTypes.DOUBLE, object.getDoubleElement());
        testField("double_array", "doubleArray", DBSupportedTypes.DOUBLE_ARRAY, object.getDoubleArray());
    }

    @Test
    public void testBoolean() throws Exception {
        testField("boolean_element", "booleanElement", DBSupportedTypes.BOOLEAN, object.isBooleanElement());
        testField("boolean_array", "booleanArray", DBSupportedTypes.BOOLEAN_ARRAY, object.getBooleanArray());
    }

    @Test
    public void testChart() throws Exception {
        testField("char_element", "charElement", DBSupportedTypes.CHAR, object.getCharElement());
        testField("char_array", "charArray", DBSupportedTypes.CHAR_ARRAY, object.getCharArray());
    }

    @Test
    public void testString() throws Exception {
        testField("string_element", "stringElement", DBSupportedTypes.STRING, object.getStringElement());
        testField("string_array", "stringArray", DBSupportedTypes.STRING_ARRAY, object.getStringArray());
    }

    @Test
    public void testEnum() throws Exception {
        testField("enum_element", "enumElement", DBSupportedTypes.ENUM, object.getEnumElement());
        testField("enum_array", "enumArray", DBSupportedTypes.ENUM_ARRAY, object.getEnumArray());
    }

    @Test
    public void testBigDecimal() throws Exception {
        testField("bigdecimal_element", "bigDecimalElement", DBSupportedTypes.BIGDECIMAL, object.getBigDecimalElement());
        testField("bigdecimal_array", "bigDecimalArray", DBSupportedTypes.BIGDECIMAL_ARRAY, object.getBigDecimalArray());
    }

    @Test
    public void testDate() throws Exception {
        testField("date_element", "dateElement", DBSupportedTypes.DATE, object.getDateElement());
        testField("date_array", "dateArray", DBSupportedTypes.DATE_ARRAY, object.getDateArray());
    }

    @Test
    public void testTimestamp() throws Exception {
        testField("timestamp_element", "timestampElement", DBSupportedTypes.TIMESTAMP, object.getTimestampElement());
        testField("timestamp_array", "timestampArray", DBSupportedTypes.TIMESTAMP_ARRAY, object.getTimestampArray());
    }

    @Test
    public void testSuperClass() throws DBException {
        AbstractMainEntity object = new AbstractMainEntity();
        DBParsedFieldsList result = WizEntityReader.parseEntity(object.getClass());
        assertEquals("abstract_entities", result.getTableName());
        assertEquals(result.size(), 4);
    }
}