package com.wizarius.orm.database;

import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;
import com.wizarius.orm.database.entityreader.WizTypeReference;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.entities.AllTypeEntity;
import com.wizarius.orm.entities.GenericTypesEntity;
import com.wizarius.orm.entities.User;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;

public class DatabaseStorageTest {
    private DBConnectionPool pool;

    @Before
    public void before() throws DBException {
        PostgresDriver driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        pool = new DBConnectionPool(driver);
    }

    @Test
    public void classInitializeTest() throws DBException {
        DatabaseStorage<User> storage = new DatabaseStorage<>(pool, User.class);
        assertNotNull(storage.getClass());
        assertFalse(storage.getSession().getFields().isEmpty());
        assertEquals(User.class, storage.getSession().getFields().getClazz());
    }

    @Test
    public void typeReferenceInitializeTest() throws DBException {
        DatabaseStorage<User> storage = new DatabaseStorage<>(pool, new WizTypeReference<User>() {
        });
        assertNotNull(storage.getClass());
        assertFalse(storage.getSession().getFields().isEmpty());
        assertEquals(User.class, storage.getSession().getFields().getClazz());
    }

    @Test
    public void genericTypeReferenceInitializeTest() throws DBException {
        DatabaseStorage<GenericTypesEntity<User, AllTypeEntity>> storage = new DatabaseStorage<>(pool, new WizTypeReference<GenericTypesEntity<User, AllTypeEntity>>() {
        });
        assertNotNull(storage.getClass());
        DBParsedFieldsList fields = storage.getSession().getFields();
        assertFalse(fields.isEmpty());
        assertEquals(GenericTypesEntity.class, fields.getClazz());
        assertEquals(User.class, fields.getFieldByFieldName("genericJson1").getClazz());
        assertEquals(AllTypeEntity.class, fields.getFieldByFieldName("genericJson2").getClazz());
        assertEquals(User[].class, fields.getFieldByFieldName("genericJsonArray1").getClazz());
        assertEquals(AllTypeEntity[].class, fields.getFieldByFieldName("genericJsonArray2").getClazz());
    }
}