package com.wizarius.orm;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.connection.*;
import com.wizarius.orm.database.mysql.driver.MysqlDriver;
import com.wizarius.orm.entities.User;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Vladyslav Shyshkin on 20.04.2018.
 */
@Ignore
public class MySQLDBConnectionTest {
    private static ConnectionDriver driver;

    @BeforeClass
    public static void beforeClass() {
        ConnectionParametersList params = new ConnectionParametersList();
        params.add(new ConnectionParameter("useUnicode", "true"));
        params.add(new ConnectionParameter("characterEncoding", "utf-8"));
        params.add(new ConnectionParameter("useSSL", "false"));
        params.add(new ConnectionParameter("allowPublicKeyRetrieval", "true"));
        params.add(new ConnectionParameter("serverTimezone", "UTC"));
        driver = new MysqlDriver("com.mysql.cj.jdbc.Driver",
                "root",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                3306,
                params);
    }

    @Test
    public void getConnectionCountTest() throws DBException {
        DBConnectionPool pool = new DBConnectionPool(driver);
        DBConnection connection = pool.getConnection();
        assertEquals(4, pool.getAvailableConnectionCount());
        connection.close();
        assertEquals(5, pool.getAvailableConnectionCount());
    }

    @Test
    public void connectionReleaseTest() throws DBException {
        DBConnectionPool pool = new DBConnectionPool(driver);
        assertEquals(5, pool.getAvailableConnectionCount());
        WizEntityQueryUtils.getDeleteQuery(pool, User.class).execute();
        assertEquals(5, pool.getAvailableConnectionCount());
        WizEntityQueryUtils.getSelectQuery(pool, User.class).execute();
        assertEquals(5, pool.getAvailableConnectionCount());
        User user = new User();
        user.setLogin("test");
        user.setPassword("test");
        WizEntityQueryUtils.getInsertQuery(pool, User.class).execute(user);
        assertEquals(5, pool.getAvailableConnectionCount());
        WizEntityQueryUtils.getUpdateQuery(pool, User.class).execute(user);
        assertEquals(5, pool.getAvailableConnectionCount());
    }
}
