package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;


/**
 * @author Vladyslav Shyshkin on 21.01.17.
 */
@DBModel(tableName = "users")
public class User {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBField(fieldName = "login")
    private String login;

    @DBField(fieldName = "password")
    private String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
