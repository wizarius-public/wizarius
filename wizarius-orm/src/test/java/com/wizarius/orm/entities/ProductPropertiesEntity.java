package com.wizarius.orm.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author shvi
 * Date: 05.05.2024
 * Time: 21:25
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ProductPropertiesEntity {
    @JsonProperty("size")
    private String[] size;
    @JsonProperty("color")
    private String color;
}
