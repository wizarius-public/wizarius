package com.wizarius.orm.entities.sc;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 17:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@DBModel(tableName = "abstract_entities", readSuperClass = true)
public class AbstractMainEntity extends Abstract2Entity {
    @DBField(fieldName = "value")
    protected BigDecimal value;
}
