package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;
import com.wizarius.orm.database.annotations.DBPredefinedField;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 17:40
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@DBModel(tableName = "generic_types")
public class GenericTypesEntity<T, G> {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBPredefinedField(fieldName = "json1", type = DBSupportedTypes.JSONB)
    private T genericJson1;
    @DBPredefinedField(fieldName = "json2", type = DBSupportedTypes.JSONB)
    private G genericJson2;

    @DBPredefinedField(fieldName = "json_array1", type = DBSupportedTypes.JSONB)
    private T[] genericJsonArray1;
    @DBPredefinedField(fieldName = "json_array2", type = DBSupportedTypes.JSONB)
    private G[] genericJsonArray2;
}