package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;
import com.wizarius.orm.database.annotations.DBPredefinedField;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author shvi
 * Date: 05.05.2024
 * Time: 21:25
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@DBModel(tableName = "products")
public class ProductEntity {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;
    @DBField(fieldName = "name")
    private String name;
    @DBPredefinedField(fieldName = "properties_jsonb", type = DBSupportedTypes.JSONB)
    private ProductPropertiesEntity propertiesJSONB;
    @DBPredefinedField(fieldName = "properties_json", type = DBSupportedTypes.JSON)
    private ProductPropertiesEntity propertiesJSON;
}
