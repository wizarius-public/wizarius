package com.wizarius.orm.entities.sc;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.entities.StatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 17:40
 */
@Data
@NoArgsConstructor
class Abstract1Entity {
    @DBField(fieldName = "id", isAutoIncrement = true)
    protected int id;

    @DBField(fieldName = "status")
    protected StatusEnum status;
}
