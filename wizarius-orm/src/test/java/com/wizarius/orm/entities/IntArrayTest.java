package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;

import java.util.Arrays;

/**
 * Created by Vladyslav Shyshkin on 12.11.17.
 */
@DBModel(tableName = "int_array_test")
public class IntArrayTest {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;
    @DBField(fieldName = "test_arr")
    private int[] array;

    public IntArrayTest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    @Override
    public String toString() {
        return "ArrayTest{" +
                "id=" + id +
                ", array=" + Arrays.toString(array) +
                '}';
    }
}
