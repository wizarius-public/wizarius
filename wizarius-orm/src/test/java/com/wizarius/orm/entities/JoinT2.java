package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;


/**
 * Created by Vladyslav Shyshkin on 13.11.17.
 */
@DBModel(tableName = "join_t2")
public class JoinT2 {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBField(fieldName = "id_t1")
    private int id_t1;

    @DBField(fieldName = "name")
    private String name;

    public JoinT2() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_t1() {
        return id_t1;
    }

    public void setId_t1(int id_t1) {
        this.id_t1 = id_t1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "JoinT2{" +
                "id=" + id +
                ", id_t1=" + id_t1 +
                ", name='" + name + '\'' +
                '}';
    }
}
