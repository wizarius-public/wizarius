package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 17:40
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@DBModel(tableName = "all_types")
public class AllTypeEntity {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBField(fieldName = "byte_element")
    private byte byteElement;
    @DBField(fieldName = "byte_array")
    private byte[] byteArray;


    @DBField(fieldName = "short_element")
    private short shortElement;
    @DBField(fieldName = "short_array")
    private short[] shortArray;


    @DBField(fieldName = "int_element")
    private int intElement;
    @DBField(fieldName = "int_array")
    private int[] intArray;


    @DBField(fieldName = "long_element")
    private long longElement;
    @DBField(fieldName = "long_array")
    private long[] longArray;


    @DBField(fieldName = "float_element")
    private float floatElement;
    @DBField(fieldName = "float_array")
    private float[] floatArray;


    @DBField(fieldName = "double_element")
    private double doubleElement;
    @DBField(fieldName = "double_array")
    private double[] doubleArray;


    @DBField(fieldName = "boolean_element")
    private boolean booleanElement;
    @DBField(fieldName = "boolean_array")
    private boolean[] booleanArray;

    @DBField(fieldName = "char_element")
    private char charElement;
    @DBField(fieldName = "char_array")
    private char[] charArray;

    @DBField(fieldName = "string_element")
    private String stringElement;
    @DBField(fieldName = "string_array")
    private String[] stringArray;

    @DBField(fieldName = "enum_element")
    private StatusEnum enumElement;
    @DBField(fieldName = "enum_array")
    private StatusEnum[] enumArray;

    @DBField(fieldName = "bigdecimal_element")
    private BigDecimal bigDecimalElement;
    @DBField(fieldName = "bigdecimal_array")
    private BigDecimal[] bigDecimalArray;

    @DBField(fieldName = "date_element")
    private Date dateElement;
    @DBField(fieldName = "date_array")
    private Date[] dateArray;

    @DBField(fieldName = "timestamp_element")
    private Timestamp timestampElement;
    @DBField(fieldName = "timestamp_array")
    private Timestamp[] timestampArray;

    @DBField(fieldName = "local_date")
    private LocalDate localDate;

    @DBField(fieldName = "local_datetime")
    private LocalDateTime localDateTime;
}
