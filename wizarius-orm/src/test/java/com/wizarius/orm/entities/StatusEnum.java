package com.wizarius.orm.entities;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 18:29
 */
public enum StatusEnum {
    ON,
    OFF,
    ERROR
}
