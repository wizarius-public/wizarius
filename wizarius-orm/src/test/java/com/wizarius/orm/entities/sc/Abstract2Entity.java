package com.wizarius.orm.entities.sc;

import com.wizarius.orm.database.annotations.DBField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 17:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
class Abstract2Entity extends Abstract1Entity {
    @DBField(fieldName = "description")
    protected String description;
}
