package com.wizarius.orm.entities;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBJoinModel;
import com.wizarius.orm.database.annotations.DBModel;


/**
 * Created by Vladyslav Shyshkin on 13.11.17.
 */
@DBModel(tableName = "join_t1")
public class JoinT1 {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBField(fieldName = "data")
    private String data;

    @DBJoinModel(currentDBFieldName = "id", insideDBFieldName = "id_t1")
    private JoinT2 table2;

    public JoinT1() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public JoinT2 getTable2() {
        return table2;
    }

    public void setTable2(JoinT2 table2) {
        this.table2 = table2;
    }
}
