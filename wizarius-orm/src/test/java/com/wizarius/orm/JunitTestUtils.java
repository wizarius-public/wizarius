package com.wizarius.orm;

import com.wizarius.orm.entities.AllTypeEntity;
import com.wizarius.orm.entities.StatusEnum;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-06
 * Time: 18:40
 */
public class JunitTestUtils {
    /**
     * Generate test object
     *
     * @return test all supported type object
     */
    public static AllTypeEntity generateObject() {
        return new AllTypeEntity()
                .setByteElement((byte) 1)
                .setByteArray("ABCDE".getBytes())

                .setShortElement((short) 1)
                .setShortArray(new short[]{(short) 1, (short) 2, (short) 3})

                .setIntElement(1)
                .setIntArray(new int[]{1, 2, 3})

                .setLongElement(1L)
                .setLongArray(new long[]{1, 2, 3, 4, 5})

                .setFloatElement(1.3f)
                .setFloatArray(new float[]{1, 2, 3.3f, 4.2f, 5.3f})

                .setDoubleElement(1.3)
                .setDoubleArray(new double[]{1, 2, 3.123, 4.1234})

                .setBooleanElement(true)
                .setBooleanArray(new boolean[]{true, false})

                .setCharElement('c')
                .setCharArray(new char[]{'a', 'b', 'c'})

                .setStringElement("test")
                .setStringArray(new String[]{"test", "test-1", "test-2"})

                .setEnumElement(StatusEnum.ON)
                .setEnumArray(new StatusEnum[]{StatusEnum.ON, StatusEnum.OFF})

                .setBigDecimalElement(new BigDecimal("10.02"))
                .setBigDecimalArray(new BigDecimal[]{new BigDecimal("103.412234"), new BigDecimal("0.9123")})

                .setDateElement(new Date(System.currentTimeMillis()))
                .setDateArray(new Date[]{new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis())})

                .setTimestampElement(new Timestamp(System.currentTimeMillis()))
                .setTimestampArray(new Timestamp[]{new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis())});

    }
}
