package com.wizarius.orm;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.WizEntityQueryUtils;
import com.wizarius.orm.database.actions.WizDBDelete;
import com.wizarius.orm.database.actions.WizDBInsert;
import com.wizarius.orm.database.actions.WizDBSelect;
import com.wizarius.orm.database.actions.WizDBUpdate;
import com.wizarius.orm.database.connection.DBConnection;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.data.*;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.entities.*;
import com.wizarius.orm.migrations.DBMigrationException;
import com.wizarius.orm.migrations.DBMigrationSystem;
import com.wizarius.orm.migrations.postgres.PostgresSystemMigrationList;
import com.wizarius.orm.migrations.savers.DBMigrationSaver;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.*;

/**
 * Created by Vladyslav Shyshkin on 12.11.17.
 */
public class PostgresDBTest {
    private static DBConnectionPool pool;

    @BeforeClass
    public static void beforeClass() throws DBException, DBMigrationException {
        PostgresDriver driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        //setup migrations
        pool = new DBConnectionPool(driver);
        DBMigrationSaver saver = new DBMigrationSaver(pool);
        DBMigrationSystem system = new DBMigrationSystem(
                pool,
                new PostgresSystemMigrationList(),
                saver
        );
        system.setup();
        assertEquals(1, saver.read().size());
        WizEntityQueryUtils.getDeleteQuery(pool, User.class).execute();
    }

    @After
    public void after() throws DBException {
        WizEntityQueryUtils.getDeleteQuery(pool, User.class).execute();
    }

    @Test
    public void insertSymbolsTest() throws DBException {
        User user = insertTestUser("[_['''```~\"");
        assertNotNull(user);
    }

    @Test
    public void getFieldNameTest() throws DBException {
        String fieldName = WizEntityQueryUtils.getSelectQuery(pool, User.class).getFieldName(
                "login",
                User.class);
        assertNotNull(fieldName);
        assertEquals("A_users.login", fieldName);
    }

    @Test
    public void customSelectTest() throws DBException {
        insertTestUser("hi");
        WizDBSelect<User> query = WizEntityQueryUtils.getSelectQuery(pool, User.class);
        query.where(query.getFieldName("login") + "=" + "'hi'");
        User user = query.getOne();
        assertNotNull(user);
    }

    @Test
    public void preparedDeleteTest() throws DBException {
        User user = insertTestUser("hi");
        WizEntityQueryUtils
                .getDeleteQuery(pool, User.class)
                .setWhereType(DBWhereType.OR)
                .where("login = 'hi'")
                .where("password", user.getPassword())
                .execute();
        assertNull(WizEntityQueryUtils.getSelectQuery(pool, User.class).where("login", "hi").getOne());
    }

    @Test
    public void dbUpdateTest() throws DBException {
        User user = insertTestUser("test1");
        user.setPassword("NEW PASSWORD");
        WizEntityQueryUtils.getUpdateQuery(pool, User.class).execute(user);
        List<User> dbEntities = WizEntityQueryUtils.getSelectQuery(pool, User.class).execute();
        assertEquals(dbEntities.get(0).getPassword(), user.getPassword());
    }


    //select from database test
    @Test
    public void dbSelectTest() throws DBException {
        User user1 = insertTestUser("test1");
        User user2 = insertTestUser("test2");

        List<User> dbEntities = WizEntityQueryUtils.getSelectQuery(pool, User.class).execute();
        assertFalse(dbEntities.isEmpty());

        List<User> usersList = WizEntityQueryUtils.getSelectQuery(pool, User.class).where("id", user1.getId()).execute();
        assertEquals(1, usersList.size());

        User user = (User) WizEntityQueryUtils.getSelectQuery(pool, User.class).where("id", user2.getId()).getOne();
        assertNotNull(user);
        assertEquals(user.getId(), user2.getId());
    }

    //insert to database test
    @Test
    public void dbInsertTest() throws DBException {
        User user = new User();
        user.setLogin("login");
        user.setPassword("password");
        WizEntityQueryUtils.getInsertQuery(pool, User.class).execute(user);
        assertTrue(user.getId() != 0);
        User selectedUser = WizEntityQueryUtils.getSelectQuery(pool, User.class).where("id", user.getId()).getOne();
        assertNotNull(selectedUser);
        assertEquals(user.getId(), selectedUser.getId());
    }

    //delete from database test
    @Test
    public void dbDeleteTest() throws DBException {
        User user1 = insertTestUser("test1");
        insertTestUser("test2");

        WizEntityQueryUtils.getDeleteQuery(pool, User.class).where("id", user1.getId()).execute();

        List<User> list = WizEntityQueryUtils.getSelectQuery(pool, User.class).execute();
        assertEquals(1, list.size());
    }

    /**
     * Insert to user and return last insert id
     */
    @Test
    public void dbInsertWithKey() throws DBException {
        User user = new User();
        user.setLogin("login");
        user.setPassword("password");
        WizEntityQueryUtils.getInsertQuery(pool, User.class).execute(user);
        assertTrue(user.getId() != 0);
    }

    @Test
    public void intArrayTest() throws DBException {
        WizEntityQueryUtils.getDeleteQuery(pool, IntArrayTest.class).execute();

        //insert int array
        IntArrayTest arrayTest = new IntArrayTest();
        arrayTest.setArray(new int[]{1, 2, 3, 4, 5, 6, 7, 8});
        WizEntityQueryUtils.getInsertQuery(pool, IntArrayTest.class).execute(arrayTest);

        //select array
        IntArrayTest one = WizEntityQueryUtils.getSelectQuery(pool, IntArrayTest.class).getOne();
        assertNotNull(one);
        assertEquals(8, one.getArray().length);

        //update integer array
        one.setArray(new int[]{1, 2, 3});
        WizEntityQueryUtils.getUpdateQuery(pool, IntArrayTest.class).execute(one);

        //select updated int array
        one = WizEntityQueryUtils.getSelectQuery(pool, IntArrayTest.class).getOne();
        assertNotNull(one);
        assertEquals(3, one.getArray().length);

        WizEntityQueryUtils.getDeleteQuery(pool, IntArrayTest.class).execute();
    }

    @Test
    public void stringArrayTest() throws DBException {
        WizEntityQueryUtils.getDeleteQuery(pool, StringArrayTest.class).execute();

        //insert string array
        StringArrayTest test = new StringArrayTest();
        test.setArray(new String[]{"test1", "test2"});
        WizEntityQueryUtils.getInsertQuery(pool, StringArrayTest.class).execute(test);

        //select array
        StringArrayTest one = WizEntityQueryUtils.getSelectQuery(pool, StringArrayTest.class).getOne();
        assertNotNull(one);
        assertEquals(2, one.getArray().length);

        //update string array
        one.setArray(new String[]{"hi"});
        WizEntityQueryUtils.getUpdateQuery(pool, StringArrayTest.class).execute(one);

        //select updated int array
        one = WizEntityQueryUtils.getSelectQuery(pool, StringArrayTest.class).getOne();
        assertNotNull(one);
        assertEquals(1, one.getArray().length);
        assertEquals("hi", one.getArray()[0]);

        WizEntityQueryUtils.getDeleteQuery(pool, StringArrayTest.class).execute();
    }

    @Test
    public void joinTest() throws DBException {
        JoinT1 t1Data = new JoinT1();
        t1Data.setData("Hi");
        WizEntityQueryUtils.getInsertQuery(pool, JoinT1.class).execute(t1Data);

        JoinT2 t2Data = new JoinT2();
        t2Data.setId_t1(t1Data.getId());
        t2Data.setName("Hi from t2");
        WizEntityQueryUtils.getInsertQuery(pool, JoinT2.class).execute(t2Data);

        JoinT1 result = WizEntityQueryUtils.getSelectQuery(pool, JoinT1.class).joinTables(JoinTypes.INNER).getOne();
        assertNotNull(result);
        assertNotNull(result.getTable2());

        WizEntityQueryUtils.getDeleteQuery(pool, JoinT2.class).execute();
        WizEntityQueryUtils.getDeleteQuery(pool, JoinT1.class).execute();
    }

    @Test
    public void limitTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        List<User> execute = WizEntityQueryUtils.getSelectQuery(pool, User.class).setLimit(5).execute();
        assertEquals(5, execute.size());

        execute = WizEntityQueryUtils.getSelectQuery(pool, User.class).orderBy("id", DBOrderType.ASC).execute();
        assertEquals(30, execute.size());
        assertEquals("test" + 0, execute.get(0).getLogin());

        execute = WizEntityQueryUtils.getSelectQuery(pool, User.class).orderBy("id", DBOrderType.ASC).setLimit(5, 5).execute();
        assertEquals(5, execute.size());
        assertEquals("test" + 5, execute.get(0).getLogin());
    }

    @Test
    public void orderByTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        List<User> execute = WizEntityQueryUtils.getSelectQuery(pool, User.class).orderBy("id", DBOrderType.DESC).execute();
        assertTrue(execute.get(0).getId() >= execute.get(1).getId());

        execute = WizEntityQueryUtils.getSelectQuery(pool, User.class).orderBy("id", DBOrderType.ASC).execute();
        assertTrue(execute.get(0).getId() <= execute.get(1).getId());
    }

    @Test
    public void groupByTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        List<User> execute = WizEntityQueryUtils
                .getSelectQuery(pool, User.class)
                .addAggregate(new AggregateField("id", "users", AggregateFunctionTypes.COUNT))
                .addAggregate(new AggregateField("login", "users", AggregateFunctionTypes.COUNT))
                .groupBy("password")
                .execute();
        assertEquals(1, execute.size());
        assertEquals(30, ((User) execute.get(0)).getId());
    }

    @Test
    public void getCountTest() throws DBException {
        for (int i = 0; i < 30; i++) {
            insertTestUser("test" + i);
        }
        User user = WizEntityQueryUtils.getSelectQuery(pool, User.class).getOne();
        long count = WizEntityQueryUtils.getSelectQuery(pool, User.class).getCount();
        assertEquals(30, count);

        count = WizEntityQueryUtils.getSelectQuery(pool, User.class).where("id", user.getId()).getCount();
        assertEquals(1, count);
    }

    @Test
    public void customQueryTest() throws DBException {
        insertTestUser("test");
        try (DBConnection connection = pool.getConnection()) {
            ResultSet resultSet = connection.executeSqlQuery("Select count(1) from users");
            resultSet.next();
            long count = resultSet.getLong(1);
            assertEquals(1, count);
        } catch (SQLException e) {
            Assert.fail();
        }
    }

    @Test
    public void customTableSelect() throws DBException {
        WizDBSelect<User> query = WizEntityQueryUtils
                .getSelectQuery(pool, User.class)
                .where("login", "test")
                .customTableName("test_users");
        String sqlQuery = query.toSQLQuery();
        assertNotNull(sqlQuery);
        assertTrue(sqlQuery.contains("FROM test_users as A_test_users"));
        assertFalse(sqlQuery.contains("FROM A_users"));
        assertTrue(sqlQuery.contains("SELECT A_test_users.id as A_id"));
        assertFalse(sqlQuery.contains("SELECT A_users.id as A_id"));
        // check aggregation
        query.addAggregate(new AggregateField("id", "test_users", AggregateFunctionTypes.MAX));
        sqlQuery = query.toSQLQuery();
        assertNotNull(sqlQuery);
        assertTrue(sqlQuery.contains("MAX(A_test_users.id)"));
        // check where
        query.where("id", 1);
        sqlQuery = query.toSQLQuery();
        assertNotNull(sqlQuery);
        assertTrue(sqlQuery.contains("id = ('1'::int4)"));
        // check join table with custom tablename
        query.joinTables(JoinTypes.INNER);
        try {
            query.toSQLQuery();
            Assert.fail();
        } catch (DBException e) {
            assertEquals("Unable to use custom table name for join query", e.getMessage());
        }
        System.out.println();
    }

    @Test
    public void customTableDelete() throws DBException {
        WizDBDelete<User> query = WizEntityQueryUtils
                .getDeleteQuery(pool, User.class)
                .where("login", "test")
                .customTableName("test_users");
        String sqlQuery = query.toSQLQuery();
        assertNotNull(sqlQuery);
        assertTrue(sqlQuery.contains("FROM test_users"));
    }

    @Test
    public void customTableInsert() throws DBException {
        WizDBInsert<User> query = WizEntityQueryUtils
                .getInsertQuery(pool, User.class)
                .customTableName("test_users");
        String sqlQuery = query.toSQLQuery(new User());
        assertNotNull(sqlQuery);
        assertTrue(sqlQuery.contains("INTO test_users"));
    }

    @Test
    public void customTableUpdate() throws DBException {
        WizDBUpdate<User> query = WizEntityQueryUtils
                .getUpdateQuery(pool, User.class)
                .customTableName("test_users");
        String sqlQuery = query.toSQLQuery(new User());
        assertNotNull(sqlQuery);
        assertTrue(sqlQuery.contains("UPDATE test_users"));
    }

    @Test
    public void productSelect() throws DBException {
        WizEntityQueryUtils
                .getDeleteQuery(pool, ProductEntity.class)
                .execute();
        ProductEntity entity = new ProductEntity()
                .setName("junit")
                .setPropertiesJSONB(new ProductPropertiesEntity().setColor("white").setSize(new String[]{"L", "S"}))
                .setPropertiesJSON(new ProductPropertiesEntity().setColor("red").setSize(new String[]{"M", "XL"}));
        WizEntityQueryUtils
                .getInsertQuery(pool, ProductEntity.class)
                .execute(entity);
        List<ProductEntity> products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .execute();
        assertFalse(products.isEmpty());
        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .where("properties_jsonb ->> 'color'", "white")
                .execute();
        assertFalse(products.isEmpty());

        entity = new ProductEntity()
                .setName("junit-null")
                .setPropertiesJSONB(null)
                .setPropertiesJSON(null);
        WizEntityQueryUtils
                .getInsertQuery(pool, ProductEntity.class)
                .execute(entity);
        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .execute();
        assertFalse(products.isEmpty());

        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .whereNull("properties_jsonb")
                .execute();
        assertNotNull(products);

        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .whereNull("properties_json")
                .execute();
        assertNotNull(products);
    }

    @Test
    public void productInsert() throws DBException {
        ProductEntity entity = new ProductEntity()
                .setName("junit")
                .setPropertiesJSONB(new ProductPropertiesEntity().setColor("white").setSize(new String[]{"L", "S"}))
                .setPropertiesJSON(new ProductPropertiesEntity().setColor("red").setSize(new String[]{"M", "XL"}));
        WizEntityQueryUtils
                .getInsertQuery(pool, ProductEntity.class)
                .execute(entity);
        assertTrue(entity.getId() != 0);
        assertNotNull(entity.getPropertiesJSONB());
        assertNotNull(entity.getPropertiesJSON());
        entity.setPropertiesJSONB(null).setPropertiesJSON(null).setId(0);
        WizEntityQueryUtils
                .getInsertQuery(pool, ProductEntity.class)
                .execute(entity);
        assertTrue(entity.getId() != 0);
        assertNull(entity.getPropertiesJSONB());
        assertNull(entity.getPropertiesJSON());
    }

    @Test
    public void productUpdate() throws DBException {
        ProductEntity entity = new ProductEntity()
                .setName("junit")
                .setPropertiesJSONB(new ProductPropertiesEntity().setColor("white").setSize(new String[]{"L", "S"}))
                .setPropertiesJSON(new ProductPropertiesEntity().setColor("red").setSize(new String[]{"M", "XL"}));
        WizEntityQueryUtils
                .getInsertQuery(pool, ProductEntity.class)
                .execute(entity);
        assertTrue(entity.getId() != 0);
        assertNotNull(entity.getPropertiesJSONB());
        assertNotNull(entity.getPropertiesJSON());
        entity.setPropertiesJSONB(null).setPropertiesJSON(null);
        WizEntityQueryUtils
                .getUpdateQuery(pool, ProductEntity.class)
                .execute(entity);
        assertTrue(entity.getId() != 0);
        assertNull(entity.getPropertiesJSONB());
        assertNull(entity.getPropertiesJSON());
    }

    @Test
    public void productsJsonOrderBy() throws DBException {
        WizEntityQueryUtils
                .getDeleteQuery(pool, ProductEntity.class)
                .execute();
        ProductEntity entity = new ProductEntity()
                .setName("junit")
                .setPropertiesJSONB(new ProductPropertiesEntity().setColor("white").setSize(new String[]{"L", "S"}))
                .setPropertiesJSON(new ProductPropertiesEntity().setColor("red").setSize(new String[]{"M", "XL"}));
        WizEntityQueryUtils
                .getInsertQuery(pool, ProductEntity.class)
                .execute(entity);
        List<ProductEntity> products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .execute();
        assertFalse(products.isEmpty());

        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .orderBy("id", DBOrderType.DESC)
                .execute();
        assertNotNull(products);
        assertFalse(products.isEmpty());


        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .orderByCustomField("(properties_jsonb ->> 'color')", DBOrderType.DESC)
                .execute();
        assertNotNull(products);
        assertFalse(products.isEmpty());

        products = WizEntityQueryUtils
                .getSelectQuery(pool, ProductEntity.class)
                .orderByQuery("(properties_jsonb ->> 'color') DESC")
                .execute();
        assertNotNull(products);
        assertFalse(products.isEmpty());
    }

    private User insertTestUser(String login) throws DBException {
        User user = new User();
        user.setLogin(login);
        user.setPassword("password");
        WizEntityQueryUtils.getInsertQuery(pool, User.class).execute(user);
        return user;
    }
}
