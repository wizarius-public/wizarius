package com.wizarius.orm.migrations.postgres;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.connection.ConnectionDriver;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.postgres.driver.PostgresDriver;
import com.wizarius.orm.migrations.DBMigration;
import com.wizarius.orm.migrations.DBMigrationException;
import com.wizarius.orm.migrations.DBMigrationList;
import com.wizarius.orm.migrations.DBMigrationSystem;
import com.wizarius.orm.migrations.savers.DBFileMigrationSaver;
import com.wizarius.orm.migrations.savers.DBMigrationSaver;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-06-11
 * Time: 10:34
 */
@Ignore
public class PostgresMigrationTest {
    private static ConnectionDriver driver;

    /**
     * Before
     * drop database wizarius_orm_migrations;
     * create database  wizarius_orm_migrations;
     */
    @Test
    @Ignore
    public void fileMigrationTest() throws DBException, DBMigrationException {
        driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm_migrations",
                "127.0.0.1",
                5432,
                null);
        //setup migrations
        DBConnectionPool pool = new DBConnectionPool(driver);
        String path = "migrationTestPostgres.wizariusDBMigration";
        DBMigrationSystem system = new DBMigrationSystem(
                pool,
                new PostgresSystemMigrationList(),
                new DBFileMigrationSaver(path)
        );
        system.setup();
        assertEquals(1, system.getSavedMigrations().size());
        // delete migration file
        File file = new File(path);
        file.deleteOnExit();
    }

    /**
     * Before
     * drop database wizarius_orm;
     * create database  wizarius_orm;
     */
    @Test
    @Ignore
    public void dbMigrationTest() throws DBException, DBMigrationException {
        driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        //setup migrations
        DBConnectionPool pool = new DBConnectionPool(driver);
        DBMigrationSystem system = new DBMigrationSystem(
                pool,
                new PostgresSystemMigrationList(),
                new DBMigrationSaver(pool)
        );
        system.setup();
        assertEquals(1, system.getSavedMigrations().size());
    }

    @Test
    @Ignore
    public void dbMigrationTransactionTest() throws DBException, DBMigrationException {
        driver = new PostgresDriver("org.postgresql.Driver",
                "postgres",
                "12345",
                "wizarius_orm",
                "127.0.0.1",
                5432,
                null);
        //setup migrations
        DBConnectionPool pool = new DBConnectionPool(driver);
        DBMigrationSystem system = new DBMigrationSystem(
                pool,
                new IncorrectMigrationList(),
                new DBMigrationSaver(pool)
        );
        try {
            system.setup();
            fail();
        } catch (Exception ignored) {
        }
        assertEquals(0, system.getSavedMigrations().size());
    }


    /**
     * Incorrect migration test
     */
    @SuppressWarnings("InnerClassMayBeStatic")
    class IncorrectMigrationList extends DBMigrationList {
        IncorrectMigrationList() {
            add(new TestIncorrectMigration(1, "Initial migrations"));
        }

        class TestIncorrectMigration extends DBMigration {
            TestIncorrectMigration(int level, String name) {
                super(level, name);
            }

            @Override
            public void up() {
                executeSQL("create table incorrect_test\n" +
                        "(\n" +
                        "    id   serial primary key not null,\n" +
                        "    name varchar            not null\n" +
                        ");");
                executeSQL("create table incorrect_test\n" +
                        "(\n" +
                        "    id   serial primary key not null,\n" +
                        "    name varchar            not null\n" +
                        ");");
                executeSQL("create table incorrect_test\n" +
                        "(\n" +
                        "    id   serial primary key not null,\n" +
                        "    name varchar            not null\n" +
                        ");");
            }
        }
    }
}
