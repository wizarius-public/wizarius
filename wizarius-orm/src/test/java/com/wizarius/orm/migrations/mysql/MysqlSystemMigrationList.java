package com.wizarius.orm.migrations.mysql;

import com.wizarius.orm.migrations.DBMigrationList;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
public class MysqlSystemMigrationList extends DBMigrationList {
    public MysqlSystemMigrationList() {
        add(new MysqlInitialMigration(1, "Initial migrations"));
    }
}
