package com.wizarius.orm.migrations.postgres;

import com.wizarius.orm.migrations.DBMigration;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
public class PostgresInitialMigration extends DBMigration {
    public PostgresInitialMigration(int level, String name) {
        super(level, name);
    }

    @Override
    public void up() {
        executeSQL("create table string_array_test(\n" +
                "\tid serial primary key,\n" +
                "\ttest_arr text[]\n" +
                ");");

        executeSQL("create table int_array_test(\n" +
                "\tid serial primary key,\n" +
                "\ttest_arr int[]\n" +
                ");");

        executeSQL("CREATE TYPE user_keys AS (\n" +
                "\tprivate_key text,\n" +
                "\tpublic_key text\n" +
                "); ");

        executeSQL("create table test_custom_type(\n" +
                "\tid serial primary key,\n" +
                "\tkeys user_keys NOT NULL\n" +
                ");");

        executeSQL("create table join_t1(\n" +
                "\tid serial primary key,\n" +
                "\tdata text not null\n" +
                ");");

        executeSQL("create table join_t2(\n" +
                "\tid serial primary key,\n" +
                "\tid_t1 integer REFERENCES join_t1(id),\n" +
                "\tname text not null  \n" +
                ");");

        executeSQL("create type StatusEnum as enum ('ON','OFF','ERROR');");

        executeSQL("CREATE TABLE users (\n" +
                "\tid serial NOT NULL PRIMARY KEY,\n" +
                "\tlogin text UNIQUE NOT NULL,\n" +
                "\tpassword varchar(50) NOT NULL\n" +
                ");");

        executeSQL("create table all_types\n" +
                "(\n" +
                "    id                 serial not null primary key,\n" +
                "-- byte\n" +
                "    byte_element       smallint,\n" +
                "    byte_array         bytea,\n" +
                "-- short\n" +
                "    short_element      smallint,\n" +
                "    short_array        smallint[],\n" +
                "-- int\n" +
                "    int_element        integer,\n" +
                "    int_array          integer[],\n" +
                "-- long\n" +
                "    long_element       bigint,\n" +
                "    long_array         bigint[],\n" +
                "-- float\n" +
                "    float_element      real,\n" +
                "    float_array        real[],\n" +
                "-- double\n" +
                "    double_element     DOUBLE PRECISION,\n" +
                "    double_array       DOUBLE PRECISION[],\n" +
                "-- boolean\n" +
                "    boolean_element    BOOLEAN,\n" +
                "    boolean_array      BOOLEAN[],\n" +
                "-- char\n" +
                "    char_element       varchar,\n" +
                "    char_array         varchar[],\n" +
                "-- string\n" +
                "    string_element     varchar,\n" +
                "    string_array       varchar[],\n" +
                "-- enum\n" +
                "    enum_element       StatusEnum,\n" +
                "    enum_array         StatusEnum[],\n" +
                "-- bigdecimal\n" +
                "    bigdecimal_element numeric,\n" +
                "    bigdecimal_array   numeric[],\n" +
                "-- date \n" +
                "    date_element       date,\n" +
                "    date_array         date[],\n" +
                "-- timestamp \n" +
                "    timestamp_element  timestamp,\n" +
                "    timestamp_array    timestamp[]\n" +
                "-- local date \n" +
                "    local_date         date,\n" +
                "    local_date_time    timestamp\n" +
                ");");

        executeSQL("create table abstract_entities\n" +
                "(\n" +
                "    id           serial primary key,\n" +
                "    value        numeric not null,\n" +
                "    description  text    not null,\n" +
                "    status StatusEnum default null\n" +
                ");");
        executeSQL("CREATE TABLE products\n" +
                "(\n" +
                "    id         SERIAL PRIMARY KEY,\n" +
                "    name       VARCHAR(255) NOT NULL,\n" +
                "    properties_jsonb JSONB,\n" +
                "    properties_json  JSON\n" +
                ");");
        executeSQL("create table generic_types\n" +
                "(\n" +
                "    id                serial not null primary key,\n" +
                "    json1             JSONB,\n" +
                "    json2             JSONB,\n" +
                "    json_array1       JSONB,\n" +
                "    json_array2       JSONB\n" +
                ");");
    }

    /**
     * Returns true if need execute migration in transaction block
     *
     * @return execute in transaction block
     */
    @Override
    protected boolean isInTransaction() {
        return true;
    }
}
