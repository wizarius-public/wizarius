package com.wizarius.orm.migrations.postgres;

import com.wizarius.orm.migrations.DBMigrationList;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
public class PostgresSystemMigrationList extends DBMigrationList {
    public PostgresSystemMigrationList() {
        add(new PostgresInitialMigration(1, "Initial migrations"));
    }
}
