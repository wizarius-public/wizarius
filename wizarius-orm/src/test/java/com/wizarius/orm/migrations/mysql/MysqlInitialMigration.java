package com.wizarius.orm.migrations.mysql;

import com.wizarius.orm.migrations.DBMigration;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
public class MysqlInitialMigration extends DBMigration {
    public MysqlInitialMigration(int level, String name) {
        super(level, name);
    }

    @Override
    public void up() {
        executeSQL("CREATE TABLE users (\n" +
                "  id int(11) NOT NULL AUTO_INCREMENT,\n" +
                "  login varchar(25) NOT NULL,\n" +
                "  password varchar(50) NOT NULL,\n" +
                "  PRIMARY KEY (id),\n" +
                "  UNIQUE KEY login (login)\n" +
                ") ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;");

        executeSQL("CREATE TABLE friends_link (\n" +
                "  user_id int(11) NOT NULL,\n" +
                "  friend_id int(11) NOT NULL,\n" +
                "  userConfirm tinyint(1) NOT NULL,\n" +
                "  friendConfirm tinyint(1) DEFAULT NULL,\n" +
                "  KEY user_id (user_id),\n" +
                "  KEY friend_id (friend_id),\n" +
                "  CONSTRAINT friends_link_ibfk_1 FOREIGN KEY (user_id) REFERENCES users (id),\n" +
                "  CONSTRAINT friends_link_ibfk_2 FOREIGN KEY (friend_id) REFERENCES users (id)\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        executeSQL("CREATE TABLE `join_t1` (\n" +
                "  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,\n" +
                "  `data` text NOT NULL,\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  UNIQUE KEY `id` (`id`)\n" +
                ") ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;");

        executeSQL("CREATE TABLE `join_t2` (\n" +
                "  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,\n" +
                "  `id_t1` int(11) DEFAULT NULL,\n" +
                "  `name` text NOT NULL,\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  UNIQUE KEY `id` (`id`)\n" +
                ") ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;");

    }
}
