package com.wizarius.orm.database.data;

/**
 * Created by Vladyslav Shyshkin on 16.01.2018.
 */
public class AggregateField {
    private final String dbFieldName;
    private final String tableName;
    private final AggregateFunctionTypes type;

    public AggregateField(String dbFieldName, String tableName, AggregateFunctionTypes type) {
        this.dbFieldName = dbFieldName;
        this.tableName = tableName;
        this.type = type;
    }

    public String getDbFieldName() {
        return dbFieldName;
    }

    public String getTableName() {
        return tableName;
    }

    public AggregateFunctionTypes getType() {
        return type;
    }

    @Override
    public String toString() {
        return "AggregateField{" +
                "dbFieldName='" + dbFieldName + '\'' +
                ", tableName='" + tableName + '\'' +
                ", type=" + type +
                '}';
    }
}
