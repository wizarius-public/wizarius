package com.wizarius.orm.database.data;

import lombok.Getter;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:54
 */
@Getter
public class WhereBaseCondition<T> extends WhereCondition {
    private final String dbFieldName;
    private final T value;
    private final DBSignType signType;

    public WhereBaseCondition(DBWhereType type, String dbFieldName, T value, DBSignType signType) {
        super(type);
        this.dbFieldName = dbFieldName;
        this.value = value;
        this.signType = signType;
    }
}
