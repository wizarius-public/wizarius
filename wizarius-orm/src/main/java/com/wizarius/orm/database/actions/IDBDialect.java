package com.wizarius.orm.database.actions;

import com.wizarius.orm.database.handlers.ReadableHandlers;
import com.wizarius.orm.database.handlers.WritableHandlers;

/**
 * @author Vladyslav Shyshkin
 * Date: 23.03.2020
 * Time: 21:48
 */
public interface IDBDialect {
    /**
     * Build limit query
     *
     * @param limit  limit
     * @param offset limit offset
     * @return limit string
     */
    String buildLimitQuery(long limit, long offset);

    /**
     * Return writable handlers
     *
     * @return writable handlers
     */
    WritableHandlers getWritableHandlers();

    /**
     * Returns readable handlers
     *
     * @return readable handlers
     */
    ReadableHandlers getReadableHandlers();
}
