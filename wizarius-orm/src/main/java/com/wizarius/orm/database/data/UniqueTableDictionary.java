package com.wizarius.orm.database.data;

import com.wizarius.orm.database.DBException;

/**
 * Created by Vladyslav Shyshkin on 19.01.2018.
 */
public class UniqueTableDictionary {
    private final char[] uniqueDictionary = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private int currentIndex = 0;

    public UniqueTableDictionary() {
    }

    /**
     * Get next identification
     *
     * @return unique char
     * @throws DBException on unable to get next table prefix
     */
    public char getNext() throws DBException {
        if (currentIndex + 1 > uniqueDictionary.length) {
            throw new DBException("Unique identifiers end");
        }
        return uniqueDictionary[currentIndex++];
    }
}
