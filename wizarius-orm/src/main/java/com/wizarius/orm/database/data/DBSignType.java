package com.wizarius.orm.database.data;

/**
 * Created by Vladyslav Shyshkin on 27.01.2018.
 */
public enum DBSignType {
    EQUALS("="),
    NOTEQUALS("!="),
    GREATER(">"),
    LESS("<"),
    GREATEROREQUAL(">="),
    LESSOREQUAL("<="),
    IN("IN"),
    NOT_IN("NOT IN"),
    LIKE("LIKE"),
    ILIKE("ILIKE"),
    NONE("");

    private final String sign;

    DBSignType(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }
}
