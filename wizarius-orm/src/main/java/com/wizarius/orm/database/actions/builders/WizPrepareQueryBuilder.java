package com.wizarius.orm.database.actions.builders;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;
import com.wizarius.orm.database.handlers.WritableHandler;
import com.wizarius.orm.database.handlers.WritableHandlers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Vladyslav Shyshkin on 20.04.2018.
 * <p>
 * Class to insert into the prepared statement query values from entity
 * Update query: update table_name set name = ?, surname = ?
 * Insert query: insert table_name values(?,?,?)
 */
public class WizPrepareQueryBuilder {
    /**
     * Entity fields map
     */
    private final DBParsedFieldsList fieldsMap;
    private final WritableHandlers writableHandlers;

    public WizPrepareQueryBuilder(DBParsedFieldsList fieldsMap, WritableHandlers writableHandlers) {
        this.fieldsMap = fieldsMap;
        this.writableHandlers = writableHandlers;
    }

    /**
     * Setup fields value
     *
     * @param index             index
     * @param entity            database entity
     * @param preparedStatement prepared statement
     */
    public void setupPreparedStatementValues(AtomicInteger index, Object entity, PreparedStatement preparedStatement) throws DBException {
        for (DBParsedField entry : fieldsMap) {
            if (entry.isAutoincrement() || entry.isJoinField()) {
                continue;
            }
            Field field = entry.getField();
            DBSupportedTypes fieldType = entry.getFieldType();
            field.setAccessible(true);
            WritableHandler<?> handler = writableHandlers.getHandlerByType(fieldType);
            try {
                handler.set(field, entity, entry, index.intValue(), preparedStatement);
                index.getAndIncrement();
            } catch (SQLException e) {
                throw new DBException("Unable to set entity value to prepared statement query", e);
            } catch (NoSuchFieldException e) {
                throw new DBException("Could not find field in object", e);
            } catch (IllegalAccessException e) {
                throw new DBException("Unable to access object field", e);
            } catch (IOException e) {
                throw new DBException("Unable to write data to object", e);
            }
        }
    }
}
