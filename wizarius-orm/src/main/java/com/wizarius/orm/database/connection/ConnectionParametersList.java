package com.wizarius.orm.database.connection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Vladyslav Shyshkin on 09.04.17.
 */
public class ConnectionParametersList extends ArrayList<ConnectionParameter> {
    public ConnectionParametersList(Collection<? extends ConnectionParameter> c) {
        super(c);
    }

    public ConnectionParametersList() {
    }
}
