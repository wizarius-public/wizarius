package com.wizarius.orm.database.postgres.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;
import com.wizarius.orm.database.handlers.ReadableHandler;
import com.wizarius.orm.database.handlers.ReadableHandlers;
import com.wizarius.orm.utils.WizJsonUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-13
 * Time: 10:06
 */
public class PostgresReadableHelper {
    /**
     * Get postgres readable handlers
     *
     * @return readable handlers
     */
    public static ReadableHandlers getReadableHandlers() {
        ReadableHandlers handlers = new ReadableHandlers();
        handlers.put(DBSupportedTypes.CHAR_ARRAY, getCharArrayHandler());
        handlers.put(DBSupportedTypes.STRING_ARRAY, getStringArrayHandler());
        handlers.put(DBSupportedTypes.SHORT_ARRAY, getShortArrayHandler());
        handlers.put(DBSupportedTypes.INTEGER_ARRAY, getIntegerArrayHandler());
        handlers.put(DBSupportedTypes.LONG_ARRAY, getLongArrayHandler());
        handlers.put(DBSupportedTypes.FLOAT_ARRAY, getFloatArrayHandler());
        handlers.put(DBSupportedTypes.DOUBLE_ARRAY, getDoubleArrayHandler());
        handlers.put(DBSupportedTypes.BOOLEAN_ARRAY, getBooleanArrayHandler());
        handlers.put(DBSupportedTypes.BIGDECIMAL_ARRAY, getBigDecimalArrayHandler());
        handlers.put(DBSupportedTypes.ENUM_ARRAY, getEnumArrayHandler());
        handlers.put(DBSupportedTypes.DATE_ARRAY, getDateArrayHandler());
        handlers.put(DBSupportedTypes.TIMESTAMP_ARRAY, getTimestampArrayHandler());
        handlers.put(DBSupportedTypes.JSONB, getJSONReadableHandler());
        handlers.put(DBSupportedTypes.JSON, getJSONReadableHandler());
        return handlers;
    }

    /**
     * Get short array handler
     *
     * @return short readable handler
     */
    private static ReadableHandler getShortArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                Short[] data = (Short[]) array.getArray();
                short[] result = new short[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i];
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get big decimal array handler
     *
     * @return big decimal readable handler
     */
    private static ReadableHandler getBigDecimalArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                BigDecimal[] data = (BigDecimal[]) array.getArray();
                field.set(instance, data);
            }
        };
    }

    /**
     * Get boolean array handler
     *
     * @return boolean readable handler
     */
    private static ReadableHandler getBooleanArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                Boolean[] data = (Boolean[]) array.getArray();
                boolean[] result = new boolean[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i];
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get char array handler
     *
     * @return char readable handler
     */
    private static ReadableHandler getCharArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                String[] data = (String[]) array.getArray();
                char[] result = new char[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i].charAt(0);
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get double array handler
     *
     * @return double readable handler
     */
    private static ReadableHandler getDoubleArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                Double[] data = (Double[]) array.getArray();
                double[] result = new double[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i];
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get float array handler
     *
     * @return float readable handler
     */
    private static ReadableHandler getFloatArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                Float[] data = (Float[]) array.getArray();
                float[] result = new float[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i];
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get long array handler
     *
     * @return long readable handler
     */
    private static ReadableHandler getLongArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                Long[] data = (Long[]) array.getArray();
                long[] result = new long[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i];
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get integer readable handler
     *
     * @return integer readable handler
     */
    private static ReadableHandler getIntegerArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                Integer[] data = (Integer[]) array.getArray();
                int[] result = new int[data.length];
                for (int i = 0; i < data.length; i++) {
                    result[i] = data[i];
                }
                field.set(instance, result);
            }
        };
    }

    /**
     * Get string readable handler
     *
     * @return readable string handler
     */
    private static ReadableHandler getStringArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws IllegalAccessException, SQLException {
                if (array == null) {
                    return;
                }
                field.set(instance, array.getArray());
            }
        };
    }

    /**
     * Get enum array handler
     *
     * @return readable enum handler
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static ReadableHandler getEnumArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                String[] data = (String[]) array.getArray();
                Object enumsArray = java.lang.reflect.Array.newInstance(field.getType().getComponentType(), data.length);
                for (int i = 0; i < data.length; i++) {
                    String value = data[i];
                    if (value != null) {
                        Enum enumValue = Enum.valueOf((Class<Enum>) field.getType().getComponentType(), value);
                        java.lang.reflect.Array.set(enumsArray, i, enumValue);
                    } else {
                        java.lang.reflect.Array.set(enumsArray, i, null);
                    }
                }
                field.set(instance, enumsArray);
            }
        };
    }

    /**
     * Get date readable handler
     *
     * @return readable date handler
     */
    private static ReadableHandler getDateArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                field.set(instance, array.getArray());
            }
        };
    }

    /**
     * Get timestamp readable
     *
     * @return readable timestamp handler
     */
    private static ReadableHandler getTimestampArrayHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(resultSetFieldName);
                process(field, instance, array);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                Array array = rs.getArray(columnIndex);
                process(field, instance, array);
            }

            private void process(Field field, Object instance, Array array) throws SQLException, IllegalAccessException {
                if (array == null) {
                    return;
                }
                field.set(instance, array.getArray());
            }
        };
    }

    /**
     * Get JSONB readable
     *
     * @return readable JSONB handler
     */
    private static ReadableHandler getJSONReadableHandler() {
        return new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException, JsonProcessingException {
                String json = rs.getString(resultSetFieldName);
                if (json != null) {
                    Object obj = WizJsonUtils.fromJsonWithError(json, parsedField.getClazz());
                    field.set(instance, obj);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException, JsonProcessingException {
                String json = rs.getString(columnIndex);
                if (json != null) {
                    Object obj = WizJsonUtils.fromJsonWithError(json, parsedField.getClazz());
                    field.set(instance, obj);
                }
            }
        };
    }
}
