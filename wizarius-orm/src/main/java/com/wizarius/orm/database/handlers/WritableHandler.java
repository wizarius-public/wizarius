package com.wizarius.orm.database.handlers;

import com.wizarius.orm.database.entityreader.DBParsedField;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Vladyslav Shyshkin on 04.04.2018.
 */
public interface WritableHandler<T> {
    /**
     * Set value in prepared statement
     *
     * @param field             field
     * @param entity            entity
     * @param parsedField       parsed field
     * @param parameterIndex    parameterIndex in prepare statement
     * @param preparedStatement prepare statement
     * @throws SQLException           on unable to read from result set
     * @throws NoSuchFieldException   on field not found
     * @throws IllegalAccessException on unable to set data to field
     * @throws IOException            on unable to write data to object
     */
    void set(Field field,
             Object entity,
             DBParsedField parsedField,
             int parameterIndex,
             PreparedStatement preparedStatement) throws NoSuchFieldException, IllegalAccessException, SQLException, IOException;

    /**
     * Set value in prepared statement
     *
     * @param value             value
     * @param parameterIndex    parameter index
     * @param preparedStatement prepared statement
     * @throws SQLException on unable to build sql query
     */
    void set(T value,
             int parameterIndex,
             PreparedStatement preparedStatement) throws SQLException;
}
