package com.wizarius.orm.database.actions;

import com.wizarius.orm.database.actions.builders.WizPrepareQueryBuilder;
import com.wizarius.orm.database.actions.builders.WizWhereQueryBuilder;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.data.fieldfinder.FieldFinder;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;
import com.wizarius.orm.database.handlers.ReadableHandlers;
import com.wizarius.orm.database.handlers.WritableHandlers;

/**
 * @author Vladyslav Shyshkin
 * Date: 22.03.2020
 * Time: 17:13
 */
public abstract class WizAbstractDBAction {
    /**
     * DB dialect
     */
    protected final IDBDialect dialect;
    /**
     * Connection pool
     */
    protected final DBConnectionPool pool;
    /**
     * Object fields map
     */
    protected final DBParsedFieldsList fields;
    /**
     * Where query builder
     */
    protected final WizWhereQueryBuilder whereQueryBuilder;
    /**
     * Fields finder
     */
    protected final FieldFinder fieldFinder;
    /**
     * Writable handlers
     */
    protected final WritableHandlers writableHandlers;
    /**
     * Readable handlers
     */
    protected final ReadableHandlers readableHandlers;
    /**
     * Prepared statement query builder
     */
    protected final WizPrepareQueryBuilder fieldsPrepareStatementQueryBuilder;
    /**
     * Custom table name for any query
     */
    protected String customTableName;

    public WizAbstractDBAction(DBConnectionPool pool, DBParsedFieldsList fields) {
        this.dialect = pool.getDialect();
        this.pool = pool;
        this.fields = fields;
        this.fieldFinder = new FieldFinder(fields);
        this.whereQueryBuilder = new WizWhereQueryBuilder(dialect.getWritableHandlers(), fieldFinder);
        this.fieldsPrepareStatementQueryBuilder = new WizPrepareQueryBuilder(fields, dialect.getWritableHandlers());
        this.writableHandlers = dialect.getWritableHandlers();
        this.readableHandlers = dialect.getReadableHandlers();
    }

    /**
     * Returns table name for query
     *
     * @return table name
     */
    protected final String getTableName() {
        return customTableName == null ? fields.getTableName() : customTableName;
    }
}
