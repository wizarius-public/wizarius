package com.wizarius.orm.database.data;

/**
 * Created by Vladyslav Shyshkin on 27.01.2018.
 */
public enum DBWhereType {
    OR,
    AND
}
