package com.wizarius.orm.database.connection;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.actions.IDBDialect;

import java.sql.Connection;

/**
 * @author Vladyslav Shyshkin on 09.04.17.
 */
public interface ConnectionDriver {
    /**
     * Returns sql connection
     *
     * @return jdbc connection
     * @throws DBException on unable to initialize
     */
    Connection getConnection() throws DBException;

    /**
     * Returns connection url
     *
     * @return connection url
     */
    String getURL();

    /**
     * Returns db dialect
     *
     * @return db dialect
     */
    IDBDialect getDialect();
}
