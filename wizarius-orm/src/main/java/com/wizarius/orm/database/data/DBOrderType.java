package com.wizarius.orm.database.data;

/**
 * Created by Vladyslav Shyshkin on 13.11.17.
 */
public enum DBOrderType {
    DESC,
    ASC
}
