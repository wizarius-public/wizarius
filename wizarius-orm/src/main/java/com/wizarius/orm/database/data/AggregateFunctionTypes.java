package com.wizarius.orm.database.data;

/**
 * Created by Vladyslav Shyshkin on 16.01.2018.
 */
public enum AggregateFunctionTypes {
    SUM,
    MAX,
    MIN,
    AVG,
    COUNT
}
