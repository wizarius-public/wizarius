package com.wizarius.orm.database.entityreader;

/**
 * Created by Vladyslav Shyshkin on 11.01.2018.
 */
public enum DBSupportedTypes {
    SHORT,
    SHORT_ARRAY,

    INTEGER,
    INTEGER_ARRAY,

    STRING,
    STRING_ARRAY,

    BYTE,
    BYTE_ARRAY,

    FLOAT,
    FLOAT_ARRAY,

    LONG,
    LONG_ARRAY,

    CHAR,
    CHAR_ARRAY,

    DOUBLE,
    DOUBLE_ARRAY,

    BOOLEAN,
    BOOLEAN_ARRAY,

    ENUM,
    ENUM_ARRAY,

    JOIN_OBJECT,
    UNKNOWN,

    BIGDECIMAL,
    BIGDECIMAL_ARRAY,

    DATE,
    DATE_ARRAY,

    TIMESTAMP,
    TIMESTAMP_ARRAY,

    LOCAL_DATE,
    LOCAL_DATE_TIME,

    JSONB,
    JSON
}
