package com.wizarius.orm.database.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Vladyslav Shyshkin on 21.01.17.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DBModel {
    String tableName();

    /**
     * true if you want to read the superclass in entity reader
     */
    boolean readSuperClass() default false;
}
