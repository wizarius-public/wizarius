package com.wizarius.orm.database;

import com.wizarius.orm.database.annotations.DBField;
import com.wizarius.orm.database.annotations.DBJoinModel;
import com.wizarius.orm.database.annotations.DBModel;
import com.wizarius.orm.database.annotations.DBPredefinedField;
import com.wizarius.orm.database.data.UniqueTableDictionary;
import com.wizarius.orm.database.entityreader.*;
import lombok.Getter;

import java.lang.reflect.*;
import java.util.*;

/**
 * Created by Vladyslav Shyshkin on 12.11.17.
 */
@Getter
public class WizEntityReader {
    /**
     * Get connection type by java class
     *
     * @param clazz class instance
     * @return connection type
     */
    public static DBSupportedTypes javaTypeToDBType(Class<?> clazz) {
        if (clazz.isEnum()) {
            return DBSupportedTypes.ENUM;
        }
        switch (clazz.getSimpleName()) {
            case "Integer":
            case "int": {
                return DBSupportedTypes.INTEGER;
            }
            case "int[]": {
                return DBSupportedTypes.INTEGER_ARRAY;
            }
            case "Short":
            case "short": {
                return DBSupportedTypes.SHORT;
            }
            case "short[]": {
                return DBSupportedTypes.SHORT_ARRAY;
            }
            case "String": {
                return DBSupportedTypes.STRING;
            }
            case "String[]": {
                return DBSupportedTypes.STRING_ARRAY;
            }
            case "Byte":
            case "byte": {
                return DBSupportedTypes.BYTE;
            }
            case "byte[]": {
                return DBSupportedTypes.BYTE_ARRAY;
            }
            case "Float":
            case "float": {
                return DBSupportedTypes.FLOAT;
            }
            case "float[]": {
                return DBSupportedTypes.FLOAT_ARRAY;
            }
            case "Long":
            case "long": {
                return DBSupportedTypes.LONG;
            }
            case "long[]": {
                return DBSupportedTypes.LONG_ARRAY;
            }
            case "Character":
            case "char": {
                return DBSupportedTypes.CHAR;
            }
            case "char[]": {
                return DBSupportedTypes.CHAR_ARRAY;
            }
            case "Double":
            case "double": {
                return DBSupportedTypes.DOUBLE;
            }
            case "double[]": {
                return DBSupportedTypes.DOUBLE_ARRAY;
            }
            case "boolean[]": {
                return DBSupportedTypes.BOOLEAN_ARRAY;
            }
            case "Boolean":
            case "boolean": {
                return DBSupportedTypes.BOOLEAN;
            }
            case "BigDecimal": {
                return DBSupportedTypes.BIGDECIMAL;
            }
            case "BigDecimal[]": {
                return DBSupportedTypes.BIGDECIMAL_ARRAY;
            }
            case "Date": {
                return DBSupportedTypes.DATE;
            }
            case "Date[]": {
                return DBSupportedTypes.DATE_ARRAY;
            }
            case "Timestamp": {
                return DBSupportedTypes.TIMESTAMP;
            }
            case "Timestamp[]": {
                return DBSupportedTypes.TIMESTAMP_ARRAY;
            }
            case "LocalDate": {
                return DBSupportedTypes.LOCAL_DATE;
            }
            case "LocalDateTime": {
                return DBSupportedTypes.LOCAL_DATE_TIME;
            }
        }
        if (clazz.isArray() && clazz.getComponentType().isEnum()) {
            return DBSupportedTypes.ENUM_ARRAY;
        }
        return DBSupportedTypes.UNKNOWN;
    }

    /**
     * Parse entity
     *
     * @param clazz entity clazz
     * @return parsed fields list
     * @throws DBException on parse entity exception
     */
    public static DBParsedFieldsList parseEntity(Class<?> clazz) throws DBException {
        WizTypeReference<Object> reference = new WizTypeReference<Object>() {
            @Override
            public Type getType() {
                return clazz;
            }
        };
        return parseEntity(reference);
    }

    /**
     * Parses a database entity using the provided type reference.
     *
     * @param reference the type reference of the entity to be parsed
     * @return the list of parsed fields from the database entity
     * @throws DBException if an error occurs during the parsing of the entity
     */
    public static DBParsedFieldsList parseEntity(WizTypeReference<?> reference) throws DBException {
        return parseEntity(reference, new UniqueTableDictionary());
    }

    /**
     * Parses a database entity using the provided type reference and unique table dictionary.
     *
     * @param reference             the type reference of the entity to parse
     * @param uniqueTableDictionary a unique table tracker to ensure unique table processing
     * @return the list of parsed fields from the database entity
     * @throws DBException if an error occurs during the parsing of the entity
     */
    private static DBParsedFieldsList parseEntity(WizTypeReference<?> reference, UniqueTableDictionary uniqueTableDictionary) throws DBException {
        Map<TypeVariable<?>, Type> typeMapping = new HashMap<>();
        Type type = reference.getType();
        Class<?> clazz;
        if (type instanceof Class<?>) {
            clazz = (Class<?>) type;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            clazz = (Class<?>) parameterizedType.getRawType();
            TypeVariable<?>[] typeParams = clazz.getTypeParameters();
            Type[] actualTypes = parameterizedType.getActualTypeArguments();
            for (int i = 0; i < typeParams.length; i++) {
                typeMapping.put(typeParams[i], actualTypes[i]);
            }
        } else {
            throw new DBException("Unsupported type: " + type);
        }
        Object dbEntity;
        try {
            dbEntity = clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new DBException("Unable to create new instance", e);
        }
        if (!dbEntity.getClass().isAnnotationPresent(DBModel.class)) {
            throw new DBException("DBModel annotation not found");
        }
        DBModel modelAnnotation = dbEntity.getClass().getAnnotation(DBModel.class);
        String tableName = modelAnnotation.tableName();
        boolean readSuperClass = modelAnnotation.readSuperClass();
        DBParsedFieldsList fieldsList = new DBParsedFieldsList(
                tableName,
                clazz,
                uniqueTableDictionary.getNext()
        );
        List<Field> fields = findFields(dbEntity, readSuperClass);
        for (Field field : fields) {
            Class<?> resolvedClass = resolveFieldType(field.getGenericType(), typeMapping);
            if (resolvedClass == null) {
                resolvedClass = field.getType();
            }
            // DB Field
            if (field.isAnnotationPresent(DBField.class)) {
                DBSupportedTypes dbFieldType = WizEntityReader.javaTypeToDBType(resolvedClass);
                String fieldName = field.getName();
                DBField dbFieldAnnotation = field.getAnnotation(DBField.class);
                String dbFieldName = dbFieldAnnotation.fieldName();
                boolean isAutoIncrement = dbFieldAnnotation.isAutoIncrement();
                fieldsList.add(new DBParsedField(
                        tableName,
                        fieldName,
                        dbFieldName,
                        resolvedClass,
                        field,
                        dbFieldType,
                        isAutoIncrement)
                );
            }
            //if predefined field
            if (field.isAnnotationPresent(DBPredefinedField.class)) {
                String fieldName = field.getName();
                DBPredefinedField dbFieldAnnotation = field.getAnnotation(DBPredefinedField.class);
                String dbFieldName = dbFieldAnnotation.fieldName();
                fieldsList.add(new DBParsedField(
                        tableName,
                        fieldName,
                        dbFieldName,
                        resolvedClass,
                        field,
                        dbFieldAnnotation.type(),
                        false)
                );
            }
            //if join model
            if (field.isAnnotationPresent(DBJoinModel.class)) {
                String fieldName = field.getName();
                String parentField = field.getAnnotation(DBJoinModel.class).currentDBFieldName();
                String childField = field.getAnnotation(DBJoinModel.class).insideDBFieldName();
                DBParsedFieldsList parsedFields = parseEntity(new WizTypeReference<Object>() {
                    @Override
                    public Type getType() {
                        return field.getType();
                    }
                }, uniqueTableDictionary);
                DBParsedField parsedField = new DBParsedField(
                        tableName,
                        fieldName,
                        "",
                        resolvedClass,
                        field,
                        DBSupportedTypes.JOIN_OBJECT,
                        false);
                fieldsList.add(parsedField);
                parsedField.setJoinField(new DBJoinField(parentField, childField, parsedFields));
            }
        }
        return fieldsList;
    }

    /**
     * Returns fields
     *
     * @param dbEntity       db entity
     * @param readSuperClass true if read superclass fields
     * @return fields
     */
    private static List<Field> findFields(Object dbEntity, boolean readSuperClass) {
        List<Field> result = new ArrayList<>();
        Field[] entityFields = dbEntity.getClass().getDeclaredFields();
        Collections.addAll(result, entityFields);
        if (readSuperClass) {
            Class<?> superclass = dbEntity.getClass().getSuperclass();
            while (!superclass.getName().equals(Object.class.getName())) {
                Field[] fields = superclass.getDeclaredFields();
                Collections.addAll(result, fields);
                superclass = superclass.getSuperclass();
            }
        }
        return result;
    }

    /**
     * Resolves the runtime class type of a given field's Java type.
     *
     * @param fieldType   the Type of the field to resolve
     * @param typeMapping a map of TypeVariable to actual Type definitions, used for resolving generic types
     * @return the resolved runtime class of the field, or null if the type cannot be resolved
     */
    private static Class<?> resolveFieldType(Type fieldType, Map<TypeVariable<?>, Type> typeMapping) {
        if (fieldType instanceof TypeVariable<?>) {
            Type resolved = typeMapping.get(fieldType);
            if (resolved instanceof Class<?>) {
                return (Class<?>) resolved;
            } else if (resolved instanceof ParameterizedType) {
                return (Class<?>) ((ParameterizedType) resolved).getRawType();
            } else if (resolved instanceof GenericArrayType) {
                GenericArrayType gat = (GenericArrayType) resolved;
                Class<?> componentClass = resolveFieldType(gat.getGenericComponentType(), typeMapping);
                if (componentClass == null) {
                    return null;
                }
                return Array.newInstance(componentClass, 0).getClass();
            }
        } else if (fieldType instanceof ParameterizedType) {
            return (Class<?>) ((ParameterizedType) fieldType).getRawType();
        } else if (fieldType instanceof GenericArrayType) {
            GenericArrayType gat = (GenericArrayType) fieldType;
            Class<?> componentClass = resolveFieldType(gat.getGenericComponentType(), typeMapping);
            if (componentClass == null) {
                return null;
            }
            return Array.newInstance(componentClass, 0).getClass();
        } else if (fieldType instanceof Class<?>) {
            return (Class<?>) fieldType;
        }
        return null;
    }
}
