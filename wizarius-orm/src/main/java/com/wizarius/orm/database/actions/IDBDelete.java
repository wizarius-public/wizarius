package com.wizarius.orm.database.actions;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.connection.DBConnection;

/**
 * @author Vladyslav Shyshkin
 * Date: 25.03.2020
 * Time: 18:50
 */
public interface IDBDelete<Entity> {
    /**
     * Execute delete query
     *
     * @param entity entity to delete
     * @throws DBException on unable to delete
     */
    void execute(Entity entity) throws DBException;

    /**
     * Execute delete query
     *
     * @throws DBException on unable to execute query
     */
    void execute() throws DBException;

    /**
     * Execute delete query
     * If connection is presented, it is assumed that the user himself wants to manage the connection
     * The connection will not be automatically closed after the request
     *
     * @param entity     entity to delete
     * @param connection connection to database
     * @throws DBException on unable to delete
     */
    void execute(Entity entity, DBConnection connection) throws DBException;

    /**
     * Execute delete query
     * If connection is presented, it is assumed that the user himself wants to manage the connection
     * The connection will not be automatically closed after the request
     *
     * @param connection connection to database
     * @throws DBException on unable to delete
     */
    void execute(DBConnection connection) throws DBException;

    /**
     * Returns sql query with all parameters and where conditions
     *
     * @param entity entity to delete
     * @return query for execution
     * @throws DBException unable to build query
     */
    String toSQLQuery(Entity entity) throws DBException;

    /**
     * Returns sql query with all parameters and where conditions
     *
     * @return query for execution
     * @throws DBException unble to build query
     */
    String toSQLQuery() throws DBException;
}
