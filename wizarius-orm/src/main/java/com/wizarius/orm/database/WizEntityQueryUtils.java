package com.wizarius.orm.database;

import com.wizarius.orm.database.actions.WizDBDelete;
import com.wizarius.orm.database.actions.WizDBInsert;
import com.wizarius.orm.database.actions.WizDBSelect;
import com.wizarius.orm.database.actions.WizDBUpdate;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;
import com.wizarius.orm.database.entityreader.EntityQuery;
import com.wizarius.orm.database.entityreader.WizTypeReference;

public class WizEntityQueryUtils {
    /**
     * Returns select query
     *
     * @param clazz    entity class
     * @param <Entity> generic class
     * @return db select query
     * @throws DBException on unable to parse entity
     */
    public static <Entity> WizDBSelect<Entity> getSelectQuery(DBConnectionPool pool,
                                                              Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return new WizDBSelect<>(pool, fields);
    }

    /**
     * Returns insert query
     *
     * @param clazz    entity class
     * @param <Entity> generic class
     * @return db insert query
     * @throws DBException on unable to parse entity
     */
    public static <Entity> WizDBInsert<Entity> getInsertQuery(DBConnectionPool pool,
                                                              Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return new WizDBInsert<>(pool, fields);
    }

    /**
     * Returns update query
     *
     * @param clazz    entity class
     * @param <Entity> generic class
     * @return db update query
     * @throws DBException on unable to parse entity
     */
    public static <Entity> WizDBUpdate<Entity> getUpdateQuery(DBConnectionPool pool,
                                                              Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return new WizDBUpdate<>(pool, fields);
    }

    /**
     * Returns update query
     *
     * @param clazz    entity class
     * @param <Entity> generic class
     * @return db update query
     * @throws DBException on unable to parse entity
     */
    public static <Entity> WizDBDelete<Entity> getDeleteQuery(DBConnectionPool pool,
                                                              Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return new WizDBDelete<>(pool, fields);
    }

    /**
     * Returns entity query builder
     *
     * @param clazz    entity class
     * @param <Entity> entity
     * @return entity queries
     * @throws DBException on unable to parse entity
     */
    public static <Entity> EntityQuery<Entity> getEntityQuery(DBConnectionPool pool,
                                                              Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return new EntityQuery<>(pool, fields);
    }

    /**
     * Constructs an entity query object for the specified type reference.
     *
     * @param reference the type reference of the entity to query
     * @param <Entity>  the generic type of the entity
     * @return an instance of EntityQuery for the specified entity
     * @throws DBException if an error occurs while parsing the entity or building the query
     */
    public static <Entity> EntityQuery<Entity> getEntityQuery(DBConnectionPool pool,
                                                              WizTypeReference<?> reference) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(reference);
        return new EntityQuery<>(pool, fields);
    }
}
