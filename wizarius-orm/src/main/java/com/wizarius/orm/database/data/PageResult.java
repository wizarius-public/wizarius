package com.wizarius.orm.database.data;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<Entity> {
    private List<Entity> rows;
    private long totalCount;

    public PageResult(List<Entity> rows, long totalCount) {
        this.rows = rows;
        this.totalCount = totalCount;
    }
}
