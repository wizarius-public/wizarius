package com.wizarius.orm.database.data.fieldfinder;

import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;

/**
 * Created by Vladyslav Shyshkin on 19.01.2018.
 */
public class FieldFinderResult {
    private final DBParsedFieldsList list;
    private final DBParsedField findField;

    public FieldFinderResult(DBParsedFieldsList list, DBParsedField findField) {
        this.list = list;
        this.findField = findField;
    }


    public DBParsedFieldsList getList() {
        return list;
    }

    public DBParsedField getFindField() {
        return findField;
    }

    @Override
    public String toString() {
        return "FieldFinderResult{" +
                "list=" + list +
                ", findField=" + findField +
                '}';
    }
}
