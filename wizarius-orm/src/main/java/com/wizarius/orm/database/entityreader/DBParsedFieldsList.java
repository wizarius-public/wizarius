package com.wizarius.orm.database.entityreader;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Vladyslav Shyshkin on 17.01.2018.
 */
public class DBParsedFieldsList extends ArrayList<DBParsedField> {
    @Getter
    private final String tableName;
    @Getter
    private final char uniqueIdentification;
    @Getter
    private final Class<?> clazz;
    /**
     * Database field
     * key - database field name
     * value - parsed field
     */
    private final HashMap<String, DBParsedField> dbFieldsMap = new HashMap<>();
    /**
     * Java fields
     * key - java field name
     * value - parsed field
     */
    private final HashMap<String, DBParsedField> fieldsMap = new HashMap<>();

    /**
     * Initialize parsed field
     *
     * @param tableName            table name
     * @param clazz                Class extend DBEntity
     * @param uniqueIdentification unique identification
     */
    public DBParsedFieldsList(String tableName,
                              Class<?> clazz,
                              char uniqueIdentification) {
        this.tableName = tableName;
        this.clazz = clazz;
        this.uniqueIdentification = uniqueIdentification;
    }

    /**
     * Add parsed field
     *
     * @param field parsed field
     * @return true if added
     */
    @Override
    public boolean add(DBParsedField field) {
        dbFieldsMap.put(field.getDbFieldName(), field);
        fieldsMap.put(field.getFieldName(), field);
        return super.add(field);
    }

    /**
     * Get field by connection field name
     *
     * @param dbFieldName database field name
     * @return parsed field
     */
    public DBParsedField getFieldByDBFieldName(String dbFieldName) {
        return dbFieldsMap.get(dbFieldName);
    }

    /**
     * Get primary key
     */
    public DBParsedField getPrimaryKey() {
        for (DBParsedField parsedField : this) {
            if (parsedField.isAutoincrement()) {
                return parsedField;
            }
        }
        return null;
    }

    /**
     * Get field by connection field name
     *
     * @param fieldName object field name
     * @return parsed field
     */
    public DBParsedField getFieldByFieldName(String fieldName) {
        return fieldsMap.get(fieldName);
    }

    @Override
    public String toString() {
        return "DBParsedFieldsList{" +
                "dbFieldsMap=" + dbFieldsMap +
                ", fieldsMap=" + fieldsMap +
                ", tableName='" + tableName + '\'' +
                ", uniqueIdentification=" + uniqueIdentification +
                ", clazz=" + clazz +
                '}';
    }
}
