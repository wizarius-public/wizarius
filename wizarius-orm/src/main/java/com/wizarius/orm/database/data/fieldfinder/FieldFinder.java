package com.wizarius.orm.database.data.fieldfinder;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;

/**
 * Created by Vladyslav Shyshkin on 19.01.2018.
 */
public class FieldFinder {
    private final DBParsedFieldsList fieldsMap;

    public FieldFinder(DBParsedFieldsList fieldsMap) {
        this.fieldsMap = fieldsMap;
    }

    /**
     * Find connection field
     *
     * @param dbFieldName database field name
     * @return field finder result
     */
    public FieldFinderResult findDBField(String dbFieldName) throws DBException {
        FieldFinderResultList fields = findDBField(new FieldFinderResultList(), fieldsMap, dbFieldName, false);
        return afterSearch(fields, dbFieldName);
    }

    /**
     * Find connection field
     *
     * @param dbFieldName database field name
     * @return database field name
     */
    public FieldFinderResult findDBField(String dbFieldName, Class<?> clazz) throws DBException {
        FieldFinderResultList fields = findDBField(new FieldFinderResultList(), fieldsMap, dbFieldName, clazz, false);
        return afterSearch(fields, dbFieldName);
    }

    /**
     * Field finder
     *
     * @return parsed field
     */
    private FieldFinderResultList findDBField(FieldFinderResultList fields,
                                              DBParsedFieldsList fieldsMap,
                                              String dbFieldName,
                                              Class<?> clazz,
                                              boolean isRecursion) {
        DBParsedField parsedField = fieldsMap.getFieldByDBFieldName(dbFieldName);
        if (parsedField != null) {
            if (fieldsMap.getClazz() == clazz) {
                fields.add(new FieldFinderResult(fieldsMap, parsedField));
            }
        }
        for (DBParsedField field : fieldsMap) {
            if (field.isJoinField()) {
                findDBField(fields, field.getJoinField().getJoinFields(), dbFieldName, clazz, true);
            }
        }
        if (isRecursion) {
            return null;
        }
        return fields;
    }

    /**
     * Field finder
     *
     * @return parsed field
     */
    private FieldFinderResultList findDBField(FieldFinderResultList fields,
                                              DBParsedFieldsList fieldsMap,
                                              String dbFieldName,
                                              boolean isRecursion) {
        DBParsedField parsedField = fieldsMap.getFieldByDBFieldName(dbFieldName);
        if (parsedField != null) {
            fields.add(new FieldFinderResult(fieldsMap, parsedField));
        }
        for (DBParsedField field : fieldsMap) {
            if (field.isJoinField()) {
                findDBField(fields, field.getJoinField().getJoinFields(), dbFieldName, true);
            }
        }
        if (isRecursion) {
            return null;
        }
        return fields;
    }

    /**
     * Check if find correct field
     *
     * @param fields      found fields
     * @param dbFieldName database field name
     * @return find field
     * @throws DBException database exception
     */
    private FieldFinderResult afterSearch(FieldFinderResultList fields, String dbFieldName) throws DBException {
        if (fields == null || fields.isEmpty()) {
            throw new DBException("Field " + dbFieldName + " NOT FOUND ");
        }
        if (fields.size() > 1) {
            StringBuilder sb = new StringBuilder();
            for (FieldFinderResult field : fields) {
                sb.append(field.getList().getClazz().getSimpleName())
                        .append(" - ")
                        .append(" ")
                        .append(field.getFindField().getFieldName())
                        .append("; ");
            }
            sb.setLength(sb.length() - 2);
            throw new DBException("More than one field found: " + sb);
        }
        return fields.get(0);
    }
}
