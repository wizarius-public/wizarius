package com.wizarius.orm.database;

/**
 * @author Vladyslav Shyshkin on 21.01.17.
 */
public class DBException extends Exception {
    public DBException(Throwable cause) {
        super(cause);
    }

    public DBException(String message, Throwable cause) {
        super(message + cause.getMessage(), cause);
    }

    public DBException(String message){
        super(message);
    }
}
