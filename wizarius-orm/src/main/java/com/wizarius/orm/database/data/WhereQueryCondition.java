package com.wizarius.orm.database.data;

import lombok.Getter;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:54
 */
@Getter
public class WhereQueryCondition extends WhereCondition {
    private final String query;

    public WhereQueryCondition(DBWhereType type, String query) {
        super(type);
        this.query = query;
    }
}
