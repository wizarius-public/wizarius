package com.wizarius.orm.database.actions;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.database.data.DBSignType;
import com.wizarius.orm.database.data.DBWhereType;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;

/**
 * @author Vladyslav Shyshkin
 * Date: 22.03.2020
 * Time: 17:57
 */
public abstract class WizAbstractWhereAction<T extends WizAbstractWhereAction<T>> extends WizAbstractDBAction {
    public WizAbstractWhereAction(DBConnectionPool pool, DBParsedFieldsList fields) {
        super(pool, fields);
    }

    /**
     * Start where group
     *
     * @return current instance
     */
    public T startWhereGroup() {
        whereQueryBuilder.startWhereGroup();
        return getInstance();
    }

    /**
     * End where group
     *
     * @return current instance
     */
    public T endWhereGroup() {
        whereQueryBuilder.endWhereGroup();
        return getInstance();
    }

    /**
     * Setup where filed
     *
     * @param field filed name
     * @param value field value
     */
    public T where(String field, Object value) {
        whereQueryBuilder.where(field, value);
        return getInstance();
    }

    /**
     * Setup where filed
     *
     * @param field    filed name
     * @param value    field value
     * @param signType sign type
     */
    public T where(String field, Object value, DBSignType signType) {
        whereQueryBuilder.where(field, value, signType);
        return getInstance();
    }

    /**
     * Setup where filed
     *
     * @param key   database field name
     * @param value database field value
     * @param clazz instance of object
     * @throws DBException on unable to build where condition
     */
    public T where(String key, Object value, Class<?> clazz) throws DBException {
        whereQueryBuilder.where(key, value, clazz);
        return getInstance();
    }

    /**
     * Setup where query
     *
     * @param query custom query
     */
    public T where(String query) {
        whereQueryBuilder.where(query);
        return getInstance();
    }

    /**
     * Setup where field in array
     *
     * @param field  field name
     * @param values values array
     * @return current instance
     */
    public T whereIn(String field, Object values) {
        whereQueryBuilder.whereIN(field, values);
        return getInstance();
    }

    /**
     * Setup where field not in array
     *
     * @param field  database field name
     * @param values value array
     * @return current instance
     */
    public T whereNotIn(String field, Object values) {
        whereQueryBuilder.whereNotIN(field, values);
        return getInstance();
    }

    /**
     * Setup where null
     *
     * @param field database field name
     * @return current instance
     */
    public T whereNull(String field) {
        whereQueryBuilder.whereNull(field);
        return getInstance();
    }

    /**
     * Setup where is not null
     *
     * @param field database field name
     * @return current instance
     */
    public T whereIsNotNull(String field) {
        whereQueryBuilder.whereIsNotNull(field);
        return getInstance();
    }

    /**
     * Setup where between cause
     * WHERE column_name BETWEEN value1 AND value2;
     *
     * @param field  database field name
     * @param value1 value1
     * @param value2 value2
     * @return current instance
     */
    public T whereBetween(String field, Object value1, Object value2) {
        whereQueryBuilder.whereBetween(field, value1, value2);
        return getInstance();
    }

    /**
     * Set where type
     *
     * @param type new where type
     */
    public T setWhereType(DBWhereType type) {
        whereQueryBuilder.setWhereType(type);
        return getInstance();
    }

    /**
     * Setup custom table name for queries
     *
     * @param tableName table name
     * @return current instance
     */
    public T customTableName(String tableName) {
        customTableName = tableName;
        return getInstance();
    }

    /**
     * Returns instance of child class
     *
     * @return child class
     */
    @SuppressWarnings("unchecked")
    private T getInstance() {
        return (T) this;
    }
}



