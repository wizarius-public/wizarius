package com.wizarius.orm.database.actions;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.connection.DBConnection;

import java.util.Map;

/**
 * @author Vladyslav Shyshkin
 * Date: 25.03.2020
 * Time: 18:46
 */
public interface IDBUpdate<Entity> {
    /**
     * Execute update query
     *
     * @param entity entity to delete
     * @throws DBException on unable to delete
     */
    void execute(Entity entity) throws DBException;

    /**
     * Execute update query
     * If connection is presented, it is assumed that the user himself wants to manage the connection
     * The connection will not be automatically closed after the request
     *
     * @param entity     entity to delete
     * @param connection connection to database
     * @throws DBException on unable to delete
     */
    void execute(Entity entity, DBConnection connection) throws DBException;

    /**
     * Execute custom update query
     *
     * @param values values map where key = db field name, value = field value
     * @throws DBException on unable to execute query
     */
    void executeCustom(Map<String, Object> values) throws DBException;

    /**
     * Execute custom update query
     * If connection is presented, it is assumed that the user himself wants to manage the connection
     * The connection will not be automatically closed after the request
     *
     * @param values     values map where key = db field name, value = field value
     * @param connection database connection
     * @throws DBException on unable to execute query
     */
    void executeCustom(Map<String, Object> values, DBConnection connection) throws DBException;

    /**
     * Returns sql query with all parameters and where conditions
     *
     * @param entity entity to delete
     * @return query for execution
     * @throws DBException unable to build query
     */
    String toSQLQuery(Entity entity) throws DBException;
}
