package com.wizarius.orm.database.handlers;

import com.wizarius.orm.database.entityreader.DBParsedField;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Vladyslav Shyshkin on 29.12.2017.
 */
public interface ReadableHandler {
    /**
     * Set data into object
     *
     * @param field              field in instance
     * @param instance           instance
     * @param rs                 result set
     * @param resultSetFieldName result set field name
     * @param parsedField        parsed field
     * @throws SQLException           on unable to read from result set
     * @throws IllegalAccessException on unable to set data to instance
     */
    void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException, IOException;

    /**
     * Set data into object
     *
     * @param field       field in instance
     * @param instance    instance
     * @param rs          result set
     * @param columnIndex column index
     * @param parsedField parsed field
     * @throws SQLException           on unable to read from result set
     * @throws IllegalAccessException on unable to set data to instance
     */
    void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException, IOException;
}
