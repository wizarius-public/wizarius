package com.wizarius.orm.database.handlers;

import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * Created by Vladyslav Shyshkin on 29.12.2017.
 * Hash map where key its type of object and value its handler
 */
public class WritableHandlers extends HashMap<DBSupportedTypes, WritableHandler<?>> {
    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public WritableHandlers() {
        initializeDefaults();
    }

    public void initializeDefaults() {
        this.put(DBSupportedTypes.SHORT, new WritableHandler<Short>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Short value = (Short) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Short value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.SMALLINT);
                } else {
                    preparedStatement.setShort(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.INTEGER, new WritableHandler<Integer>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Integer value = (Integer) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Integer value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.INTEGER);
                } else {
                    preparedStatement.setInt(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.DOUBLE, new WritableHandler<Double>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Double value = (Double) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Double value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.DOUBLE);
                } else {
                    preparedStatement.setDouble(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.LONG, new WritableHandler<Long>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Long value = (Long) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Long value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.BIGINT);
                } else {
                    preparedStatement.setLong(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.FLOAT, new WritableHandler<Float>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Float value = (Float) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Float value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.FLOAT);
                } else {
                    preparedStatement.setFloat(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.BOOLEAN, new WritableHandler<Boolean>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Boolean value = (Boolean) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Boolean value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.BOOLEAN);
                } else {
                    preparedStatement.setBoolean(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.BIGDECIMAL, new WritableHandler<BigDecimal>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                BigDecimal value = (BigDecimal) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(BigDecimal value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.NUMERIC);
                } else {
                    preparedStatement.setBigDecimal(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.STRING, new WritableHandler<String>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                String value = (String) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(String value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.VARCHAR);
                } else {
                    preparedStatement.setString(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.BYTE, new WritableHandler<Byte>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Byte value = (Byte) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Byte value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.NULL);
                } else {
                    preparedStatement.setByte(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.BYTE_ARRAY, new WritableHandler<byte[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                byte[] value = (byte[]) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(byte[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.BINARY);
                } else {
                    preparedStatement.setBytes(parameterIndex, value);
                }
            }
        });
        this.put(DBSupportedTypes.CHAR, new WritableHandler<Character>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Character value = (Character) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Character value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null || value == Character.MIN_VALUE) {
                    preparedStatement.setNull(parameterIndex, Types.CHAR);
                } else {
                    preparedStatement.setString(parameterIndex, String.valueOf(value));
                }
            }
        });
        this.put(DBSupportedTypes.ENUM, new WritableHandler<Object>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws NoSuchFieldException, IllegalAccessException, SQLException {
                Object result = field.get(entity);
                set(result, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Object value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.OTHER);
                } else {
                    preparedStatement.setObject(parameterIndex, value.toString(), Types.OTHER);
                }
            }
        });
        this.put(DBSupportedTypes.DATE, new WritableHandler<Date>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Date value = (Date) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Date value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.DATE);
                } else {
                    preparedStatement.setDate(parameterIndex, value);
                }
            }
        });

        this.put(DBSupportedTypes.TIMESTAMP, new WritableHandler<Timestamp>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Timestamp value = (Timestamp) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(Timestamp value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.TIMESTAMP);
                } else {
                    preparedStatement.setTimestamp(parameterIndex, value);
                }
            }
        });

        this.put(DBSupportedTypes.LOCAL_DATE, new WritableHandler<LocalDate>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                LocalDate value = (LocalDate) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(LocalDate value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.DATE);
                } else {
                    preparedStatement.setDate(parameterIndex, Date.valueOf(value));
                }
            }
        });

        this.put(DBSupportedTypes.LOCAL_DATE_TIME, new WritableHandler<LocalDateTime>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                LocalDateTime value = (LocalDateTime) field.get(entity);
                set(value, parameterIndex, preparedStatement);
            }

            @Override
            public void set(LocalDateTime value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.TIMESTAMP);
                } else {
                    preparedStatement.setTimestamp(parameterIndex, Timestamp.valueOf(value));
                }
            }
        });

    }

    /**
     * Returns handler by type
     *
     * @param type database supported type
     * @return prepared handler
     * @throws UnknownHandlerException on unknown handler
     */
    public WritableHandler<?> getHandlerByType(DBSupportedTypes type) {
        WritableHandler<?> handler = get(type);
        if (handler == null) {
            throw new UnknownHandlerException("Unable to found handler for field where type = " + type);
        }
        return handler;
    }
}
