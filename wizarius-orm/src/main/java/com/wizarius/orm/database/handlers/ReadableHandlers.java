package com.wizarius.orm.database.handlers;

import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

/**
 * Created by Vladyslav Shyshkin on 29.12.2017.
 * Hash map where key its type of object and value its handler
 */
public class ReadableHandlers extends HashMap<DBSupportedTypes, ReadableHandler> {
    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public ReadableHandlers() {
        initializeDefaults();
    }

    /**
     * Initialize default handlers
     */
    @SuppressWarnings("unchecked")
    public void initializeDefaults() {
        this.put(DBSupportedTypes.BYTE, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                byte value = rs.getByte(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                byte value = rs.getByte(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.SHORT, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                short value = rs.getShort(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                short value = rs.getShort(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.INTEGER, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                int value = rs.getInt(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                int value = rs.getInt(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.LONG, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                long value = rs.getLong(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                long value = rs.getLong(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.FLOAT, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                float value = rs.getFloat(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                float value = rs.getFloat(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.DOUBLE, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                double value = rs.getDouble(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                double value = rs.getDouble(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.BOOLEAN, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                boolean value = rs.getBoolean(resultSetFieldName);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                boolean value = rs.getBoolean(columnIndex);
                if (!rs.wasNull()) {
                    field.set(instance, value);
                }
            }
        });
        this.put(DBSupportedTypes.BIGDECIMAL, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                BigDecimal value = rs.getBigDecimal(resultSetFieldName);
                field.set(instance, value);
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                BigDecimal value = rs.getBigDecimal(columnIndex);
                field.set(instance, value);
            }
        });
        this.put(DBSupportedTypes.CHAR, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                String string = rs.getString(resultSetFieldName);
                if (string != null && string.length() != 0) {
                    field.set(instance, string.charAt(0));
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                String string = rs.getString(columnIndex);
                if (string != null && string.length() != 0) {
                    field.set(instance, string.charAt(0));
                }
            }
        });
        this.put(DBSupportedTypes.STRING, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getString(resultSetFieldName));
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getString(columnIndex));
            }
        });
        this.put(DBSupportedTypes.BYTE_ARRAY, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getBytes(resultSetFieldName));
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getBytes(columnIndex));
            }
        });
        this.put(DBSupportedTypes.DATE, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getDate(resultSetFieldName));
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getDate(columnIndex));
            }
        });
        this.put(DBSupportedTypes.TIMESTAMP, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getTimestamp(resultSetFieldName));
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                field.set(instance, rs.getTimestamp(columnIndex));
            }
        });
        this.put(DBSupportedTypes.ENUM, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                String value = rs.getString(resultSetFieldName);
                if (value != null) {
                    Enum<?> enumValue = Enum.valueOf((Class<Enum>) field.getType(), value);
                    field.set(instance, enumValue);
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException {
                String value = rs.getString(columnIndex);
                if (value != null) {
                    Enum<?> enumValue = Enum.valueOf((Class<Enum>) field.getType(), value);
                    field.set(instance, enumValue);
                }
            }
        });
        this.put(DBSupportedTypes.LOCAL_DATE, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException, IOException {
                Date value = rs.getDate(resultSetFieldName);
                if (value != null) {
                    field.set(instance, value.toLocalDate());
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException, IOException {
                Date value = rs.getDate(columnIndex);
                if (value != null) {
                    field.set(instance, value.toLocalDate());
                }
            }
        });
        this.put(DBSupportedTypes.LOCAL_DATE_TIME, new ReadableHandler() {
            @Override
            public void set(Field field, Object instance, ResultSet rs, String resultSetFieldName, DBParsedField parsedField) throws SQLException, IllegalAccessException, IOException {
                Timestamp value = rs.getTimestamp(resultSetFieldName);
                if (value != null) {
                    field.set(instance, value.toLocalDateTime());
                }
            }

            @Override
            public void set(Field field, Object instance, ResultSet rs, int columnIndex, DBParsedField parsedField) throws SQLException, IllegalAccessException, IOException {
                Timestamp value = rs.getTimestamp(columnIndex);
                if (value != null) {
                    field.set(instance, value.toLocalDateTime());
                }
            }
        });
    }
}
