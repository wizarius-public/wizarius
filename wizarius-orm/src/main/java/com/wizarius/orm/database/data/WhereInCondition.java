package com.wizarius.orm.database.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:54
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class WhereInCondition<T> extends WhereCondition {
    private final String dbFieldName;
    private final T[] values;
    private final DBSignType signType;

    public WhereInCondition(DBWhereType whereType, String dbFieldName, T[] values, DBSignType signType) {
        super(whereType);
        this.dbFieldName = dbFieldName;
        this.values = values;
        this.signType = signType;
    }
}
