package com.wizarius.orm.database.data;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:54
 */
@Getter
public class WhereGroupCondition extends WhereCondition {
    private final List<WhereCondition> conditions = new ArrayList<>();

    public WhereGroupCondition(DBWhereType type) {
        super(type);
    }
}
