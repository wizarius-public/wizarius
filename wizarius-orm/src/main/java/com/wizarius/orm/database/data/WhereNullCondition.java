package com.wizarius.orm.database.data;

import lombok.Getter;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:54
 */
@Getter
public class WhereNullCondition extends WhereCondition {
    private final String dbFieldName;
    private final boolean isNull;

    public WhereNullCondition(DBWhereType type, String dbFieldName, boolean isNull) {
        super(type);
        this.dbFieldName = dbFieldName;
        this.isNull = isNull;
    }
}
