package com.wizarius.orm.database.postgres;

import com.wizarius.orm.database.actions.IDBDialect;
import com.wizarius.orm.database.handlers.ReadableHandlers;
import com.wizarius.orm.database.handlers.WritableHandlers;
import com.wizarius.orm.database.postgres.helpers.PostgresReadableHelper;
import com.wizarius.orm.database.postgres.helpers.PostgresWritableHelper;

/**
 * @author Vladyslav Shyshkin
 * Date: 23.03.2020
 * Time: 21:49
 */
public class PostgresDialect implements IDBDialect {
    private final ReadableHandlers readableHandlers;
    private final WritableHandlers writableHandlers;

    public PostgresDialect() {
        this.writableHandlers = new WritableHandlers();
        this.writableHandlers.putAll(PostgresWritableHelper.getWritableHandlers());
        this.readableHandlers = new ReadableHandlers();
        this.readableHandlers.putAll(PostgresReadableHelper.getReadableHandlers());
    }

    /**
     * Build limit query
     *
     * @param limit  limit
     * @param offset limit offset
     * @return limit string
     */
    @Override
    public String buildLimitQuery(long limit, long offset) {
        if (offset != 0) {
            return "LIMIT " + limit + " OFFSET " + offset;
        } else {
            return "LIMIT " + limit;
        }
    }

    /**
     * Return writable handlers
     *
     * @return writable handlers
     */
    @Override
    public WritableHandlers getWritableHandlers() {
        return writableHandlers;
    }

    /**
     * Returns readable handlers
     *
     * @return readable handlers
     */
    @Override
    public ReadableHandlers getReadableHandlers() {
        return readableHandlers;
    }
}
