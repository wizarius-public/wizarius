package com.wizarius.orm.database.entityreader;

import java.lang.reflect.Field;

/**
 * @author Vladyslav Shyshkin on 29.04.17.
 */
public class DBParsedField {
    private final String tableName;
    private final String fieldName;
    private final String dbFieldName;
    private final DBSupportedTypes fieldType;
    private final Class<?> clazz;
    private final Field field;
    private DBJoinField joinField;
    private final boolean autoincrement;

    public DBParsedField(String tableName,
                         String fieldName,
                         String dbFieldName,
                         Class<?> clazz,
                         Field field,
                         DBSupportedTypes fieldType,
                         boolean autoincrement) {
        this.tableName = tableName;
        this.fieldName = fieldName;
        this.dbFieldName = dbFieldName;
        this.fieldType = fieldType;
        this.field = field;
        this.clazz = clazz;
        this.autoincrement = autoincrement;
    }

    public boolean isAutoincrement() {
        return autoincrement;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getDbFieldName() {
        return dbFieldName;
    }

    public DBSupportedTypes getFieldType() {
        return fieldType;
    }

    public DBJoinField getJoinField() {
        return joinField;
    }

    public boolean isJoinField() {
        return joinField != null;
    }

    public void setJoinField(DBJoinField joinField) {
        this.joinField = joinField;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public Field getField() {
        return field;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public String toString() {
        return "DBParsedField{" +
                "tableName='" + tableName + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", dbFieldName='" + dbFieldName + '\'' +
                ", fieldType=" + fieldType +
                ", clazz=" + clazz +
                ", field=" + field +
                ", joinField=" + joinField +
                ", autoincrement=" + autoincrement +
                '}';
    }
}
