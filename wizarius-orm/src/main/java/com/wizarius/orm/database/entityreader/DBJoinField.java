package com.wizarius.orm.database.entityreader;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Created by Vladyslav Shyshkin on 17.01.2018.
 */
@Getter
@ToString
@AllArgsConstructor
public class DBJoinField {
    private final String currentClassDBField;
    private final String insideClassDBField;
    private final DBParsedFieldsList joinFields;
}
