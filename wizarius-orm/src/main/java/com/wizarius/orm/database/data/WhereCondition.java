package com.wizarius.orm.database.data;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:55
 */
public abstract class WhereCondition {
    private final DBWhereType type;

    public WhereCondition(DBWhereType type) {
        this.type = type;
    }

    public DBWhereType getType() {
        return type;
    }
}
