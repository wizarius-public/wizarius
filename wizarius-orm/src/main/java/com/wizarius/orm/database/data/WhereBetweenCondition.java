package com.wizarius.orm.database.data;

import lombok.Getter;

/**
 * @author shvi
 * Date: 07.10.2021
 * Time: 18:54
 */
@Getter
public class WhereBetweenCondition extends WhereCondition {
    private final String dbFieldName;
    private final Object value1;
    private final Object value2;

    public WhereBetweenCondition(DBWhereType type, String dbFieldName, Object value1, Object value2) {
        super(type);
        this.dbFieldName = dbFieldName;
        this.value1 = value1;
        this.value2 = value2;
    }
}
