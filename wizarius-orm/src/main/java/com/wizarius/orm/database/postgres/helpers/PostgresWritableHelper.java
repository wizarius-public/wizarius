package com.wizarius.orm.database.postgres.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;
import com.wizarius.orm.database.handlers.WritableHandler;
import com.wizarius.orm.database.handlers.WritableHandlers;
import com.wizarius.orm.utils.WizJsonUtils;
import org.postgresql.util.PGobject;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Arrays;

/**
 * Created by Vladyslav Shyshkin on 12.11.17.
 * <p>
 * TypeInfoCache - supported types in postgres
 */
public class PostgresWritableHelper {
    /**
     * Get list of writable handlers
     *
     * @return prepared type handlers
     */
    public static WritableHandlers getWritableHandlers() {
        WritableHandlers handlers = new WritableHandlers();
        handlers.put(DBSupportedTypes.SHORT_ARRAY, getShortArrayHandler());
        handlers.put(DBSupportedTypes.INTEGER_ARRAY, getIntegerArrayHandler());
        handlers.put(DBSupportedTypes.LONG_ARRAY, getLongArrayHandler());
        handlers.put(DBSupportedTypes.FLOAT_ARRAY, getFloatArrayHandler());
        handlers.put(DBSupportedTypes.DOUBLE_ARRAY, getDoubleArrayHandler());
        handlers.put(DBSupportedTypes.STRING_ARRAY, getStringArrayHandler());
        handlers.put(DBSupportedTypes.CHAR_ARRAY, getChartArrayHandler());
        handlers.put(DBSupportedTypes.ENUM_ARRAY, getEnumArrayHandler());
        handlers.put(DBSupportedTypes.BOOLEAN_ARRAY, getBooleanArrayHandler());
        handlers.put(DBSupportedTypes.BIGDECIMAL_ARRAY, getBigDecimalArrayHandler());
        handlers.put(DBSupportedTypes.DATE_ARRAY, getDateArrayHandler());
        handlers.put(DBSupportedTypes.TIMESTAMP_ARRAY, getTimestampArrayHandler());
        handlers.put(DBSupportedTypes.JSONB, getJsonBHandler());
        handlers.put(DBSupportedTypes.JSON, getJsonHandler());
        return handlers;
    }

    /**
     * Get integer array handler
     *
     * @return writable int handler
     */
    private static WritableHandler<int[]> getIntegerArrayHandler() {
        return new WritableHandler<int[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    int[] value = (int[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(int[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Integer[] integers = Arrays.stream(value).boxed().toArray(Integer[]::new);
                    Array array = preparedStatement.getConnection().createArrayOf("integer", integers);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get short array handler
     *
     * @return writable short handler
     */
    private static WritableHandler<short[]> getShortArrayHandler() {
        return new WritableHandler<short[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    short[] value = (short[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(short[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Short[] shorts = new Short[value.length];
                    for (int i = 0; i < value.length; i++) {
                        shorts[i] = value[i];
                    }
                    Array array = preparedStatement.getConnection().createArrayOf("smallint", shorts);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get long array handler
     *
     * @return writable long handler
     */
    private static WritableHandler<long[]> getLongArrayHandler() {
        return new WritableHandler<long[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    long[] value = (long[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(long[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Long[] longs = Arrays.stream(value).boxed().toArray(Long[]::new);
                    Array array = preparedStatement.getConnection().createArrayOf("BIGINT", longs);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get long array handler
     *
     * @return writable float handler
     */
    private static WritableHandler<float[]> getFloatArrayHandler() {
        return new WritableHandler<float[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    float[] value = (float[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(float[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Float[] floats = new Float[value.length];
                    for (int i = 0; i < value.length; i++) {
                        floats[i] = value[i];
                    }
                    Array array = preparedStatement.getConnection().createArrayOf("float4", floats);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get double array handler
     *
     * @return writable double handler
     */
    private static WritableHandler<double[]> getDoubleArrayHandler() {
        return new WritableHandler<double[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    double[] value = (double[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(double[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Double[] doubles = new Double[value.length];
                    for (int i = 0; i < value.length; i++) {
                        doubles[i] = value[i];
                    }
                    Array array = preparedStatement.getConnection().createArrayOf("float8", doubles);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get string array handler
     *
     * @return writable string handler
     */
    private static WritableHandler<String[]> getStringArrayHandler() {
        return new WritableHandler<String[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    String[] value = (String[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(String[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Array array = preparedStatement.getConnection().createArrayOf("varchar", value);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get char array handler
     *
     * @return writable char handler
     */
    private static WritableHandler<char[]> getChartArrayHandler() {
        return new WritableHandler<char[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    char[] value = (char[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(char[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Character[] chars = new Character[value.length];
                    for (int i = 0; i < value.length; i++) {
                        chars[i] = value[i];
                    }
                    Array array = preparedStatement.getConnection().createArrayOf("varchar", chars);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get char array handler
     *
     * @return writable char handler
     */
    private static WritableHandler<boolean[]> getBooleanArrayHandler() {
        return new WritableHandler<boolean[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    boolean[] value = (boolean[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(boolean[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Boolean[] booleans = new Boolean[value.length];
                    for (int i = 0; i < value.length; i++) {
                        booleans[i] = value[i];
                    }
                    Array array = preparedStatement.getConnection().createArrayOf("boolean", booleans);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get bigdecimal array handler
     *
     * @return writable bigdecimal handler
     */
    private static WritableHandler<BigDecimal[]> getBigDecimalArrayHandler() {
        return new WritableHandler<BigDecimal[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    BigDecimal[] value = (BigDecimal[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(BigDecimal[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Array array = preparedStatement.getConnection().createArrayOf("numeric", value);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get enum array handler
     *
     * @return writable enum handler
     */
    private static WritableHandler<Enum[]> getEnumArrayHandler() {
        return new WritableHandler<Enum[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    Enum[] value = (Enum[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(Enum[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Array array = preparedStatement.getConnection().createArrayOf("varchar", value);
                    preparedStatement.setObject(parameterIndex, array, Types.OTHER);
                }
            }
        };
    }

    /**
     * Get date array handler
     *
     * @return writable date handler
     */
    private static WritableHandler<Date[]> getDateArrayHandler() {
        return new WritableHandler<Date[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    Date[] value = (Date[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(Date[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Array array = preparedStatement.getConnection().createArrayOf("date", value);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }

    /**
     * Get timestamp array handler
     *
     * @return writable timestamp handler
     */
    private static WritableHandler<Timestamp[]> getTimestampArrayHandler() {
        return new WritableHandler<Timestamp[]>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException {
                Object obj = field.get(entity);
                if (obj == null) {
                    set(null, parameterIndex, preparedStatement);
                } else {
                    Timestamp[] value = (Timestamp[]) obj;
                    set(value, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(Timestamp[] value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.ARRAY);
                } else {
                    Array array = preparedStatement.getConnection().createArrayOf("timestamp", value);
                    preparedStatement.setArray(parameterIndex, array);
                }
            }
        };
    }


    /**
     * Get JSONB handler
     *
     * @return writable handler
     */
    private static WritableHandler<?> getJsonBHandler() {
        return new WritableHandler<PGobject>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException, JsonProcessingException {
                Object value = field.get(entity);
                if (value == null) {
                    PGobject jsonObject = new PGobject();
                    jsonObject.setType("jsonb");
                    jsonObject.setValue(null);
                    set(jsonObject, parameterIndex, preparedStatement);
                } else {
                    String json = WizJsonUtils.toJsonWithError(value);
                    PGobject jsonObject = new PGobject();
                    jsonObject.setType("jsonb");
                    jsonObject.setValue(json);
                    set(jsonObject, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(PGobject value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.NULL);
                } else {
                    preparedStatement.setObject(parameterIndex, value);
                }
            }
        };
    }

    /**
     * Get JSON handler
     *
     * @return writable handler
     */
    private static WritableHandler<?> getJsonHandler() {
        return new WritableHandler<PGobject>() {
            @Override
            public void set(Field field, Object entity, DBParsedField parsedField, int parameterIndex, PreparedStatement preparedStatement) throws IllegalAccessException, SQLException, JsonProcessingException {
                Object value = field.get(entity);
                if (value == null) {
                    PGobject jsonObject = new PGobject();
                    jsonObject.setType("json");
                    jsonObject.setValue(null);
                    set(jsonObject, parameterIndex, preparedStatement);
                } else {
                    String json = WizJsonUtils.toJsonWithError(value);
                    PGobject jsonObject = new PGobject();
                    jsonObject.setType("json");
                    jsonObject.setValue(json);
                    set(jsonObject, parameterIndex, preparedStatement);
                }
            }

            @Override
            public void set(PGobject value, int parameterIndex, PreparedStatement preparedStatement) throws SQLException {
                if (value == null) {
                    preparedStatement.setNull(parameterIndex, Types.NULL);
                } else {
                    preparedStatement.setObject(parameterIndex, value);
                }
            }
        };
    }
}
