package com.wizarius.orm.database.mysql;

import com.wizarius.orm.database.actions.IDBDialect;
import com.wizarius.orm.database.handlers.ReadableHandlers;
import com.wizarius.orm.database.handlers.WritableHandlers;

/**
 * @author Vladyslav Shyshkin
 * Date: 23.03.2020
 * Time: 18:18
 */
public class MysqlDialect implements IDBDialect {
    private final ReadableHandlers readableHandlers;
    private final WritableHandlers writableHandlers;

    public MysqlDialect() {
        this.writableHandlers = new WritableHandlers();
        this.readableHandlers = new ReadableHandlers();
    }

    /**
     * Build limit query
     *
     * @param limit  limit
     * @param offset limit offset
     * @return limit string
     */
    @Override
    public String buildLimitQuery(long limit, long offset) {
        if (offset != 0) {
            return "LIMIT " + limit + " OFFSET " + offset;
        } else {
            return "LIMIT " + limit;
        }
    }

    /**
     * Return writable handlers
     *
     * @return writable handlers
     */
    @Override
    public WritableHandlers getWritableHandlers() {
        return writableHandlers;
    }

    /**
     * Returns readable handlers
     *
     * @return readable handlers
     */
    @Override
    public ReadableHandlers getReadableHandlers() {
        return readableHandlers;
    }
}
