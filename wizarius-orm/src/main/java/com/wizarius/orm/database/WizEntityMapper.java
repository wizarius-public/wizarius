package com.wizarius.orm.database;

import com.wizarius.orm.database.actions.IDBDialect;
import com.wizarius.orm.database.entityreader.DBParsedField;
import com.wizarius.orm.database.entityreader.DBParsedFieldsList;
import com.wizarius.orm.database.entityreader.DBSupportedTypes;
import com.wizarius.orm.database.entityreader.WizTypeReference;
import com.wizarius.orm.database.handlers.ReadableHandler;
import com.wizarius.orm.database.handlers.ReadableHandlers;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WizEntityMapper {
    /**
     * Initializes an entity of the specified class type by mapping data from the provided
     * database result set and using the given database dialect for handling specific dialect-related operations.
     *
     * @param <Entity>  the type of the entity to be initialized
     * @param dialect   the database dialect to be used for any dialect-specific operations
     * @param resultSet the result set from which data is read
     * @param clazz     the class type of the entity to be initialized
     * @return the initialized entity of type Entity
     * @throws DBException if an error occurs during initialization, such as database access issues,
     *                     missing handlers, or illegal field access
     */
    public static <Entity> Entity initializeEntity(ResultSet resultSet,
                                                   IDBDialect dialect,
                                                   Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return initializeEntity(resultSet, dialect, fields, false);
    }

    /**
     * Initializes an entity based on the provided database result set and type reference.
     *
     * @param <Entity>  the type of the entity to be initialized
     * @param resultSet the result set from which data is read
     * @param reference the type reference of the entity to be initialized
     * @return the initialized entity of type Entity
     * @throws DBException if an error occurs during initialization, such as database access issues
     *                     or illegal field access
     */
    public static <Entity> Entity initializeEntity(ResultSet resultSet,
                                                   IDBDialect dialect,
                                                   WizTypeReference<Entity> reference) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(reference);
        return initializeEntity(resultSet, dialect, fields, false);
    }

    /**
     * Initializes a list of entities based on the provided result set and class type.
     * Iterates through the result set, mapping each row to an entity instance.
     *
     * @param <Entity>  the type of the entity to be initialized
     * @param resultSet the result set from which data is read
     * @param clazz     the class type of the entity to be initialized
     * @return a list of initialized entities
     * @throws DBException if an error occurs during initialization, such as database access issues
     *                     or illegal field access
     */
    public static <Entity> List<Entity> initializeEntities(ResultSet resultSet,
                                                           IDBDialect dialect,
                                                           Class<Entity> clazz) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(clazz);
        return initializeEntities(resultSet, dialect, fields);
    }

    /**
     * Initializes a list of entities based on the provided result set and type reference.
     * Iterates through the result set, mapping each row to an entity instance.
     *
     * @param <Entity>  the type of the entity to be initialized
     * @param resultSet the result set from which data is read
     * @param reference the type reference of the entity to be initialized
     * @return a list of initialized entities
     * @throws DBException if an error occurs during initialization, such as database access issues
     *                     or illegal field access
     */
    public static <Entity> List<Entity> initializeEntities(ResultSet resultSet,
                                                           IDBDialect dialect,
                                                           WizTypeReference<Entity> reference) throws DBException {
        DBParsedFieldsList fields = WizEntityReader.parseEntity(reference);
        return initializeEntities(resultSet, dialect, fields);
    }

    /**
     * Initializes and populates a list of entities by processing the provided ResultSet.
     *
     * @param resultSet the ResultSet containing the data to initialize the entities
     * @param fields    the list of database fields that map to the entity's attributes
     * @return a list of initialized entities of the specified generic type
     * @throws DBException if an error occurs while processing the ResultSet
     */
    private static <Entity> List<Entity> initializeEntities(ResultSet resultSet,
                                                            IDBDialect dialect,
                                                            DBParsedFieldsList fields) throws DBException {
        List<Entity> entities = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Entity entity = initializeEntity(resultSet, dialect, fields, false);
                entities.add(entity);
            }
        } catch (SQLException e) {
            throw new DBException("Error reading result set for entity initialization", e);
        }
        return entities;
    }

    /**
     * Initializes an entity and its associated join fields based on a database result set and a list
     * of parsed fields.
     *
     * @param <Entity>     the type of the entity to be initialized
     * @param resultSet    the result set from which the entity data is read
     * @param parsedFields the list of parsed fields representing metadata and mappings for the entity
     * @return the initialized entity of type T with its associated join fields
     * @throws DBException if an error occurs during initialization, such as database access issues
     *                     or illegal field access
     */
    public static <Entity> Entity initializeEntityWithJoin(ResultSet resultSet,
                                                           IDBDialect dialect,
                                                           DBParsedFieldsList parsedFields) throws DBException {
        Entity instance = initializeEntity(resultSet, dialect, parsedFields, true);
        for (DBParsedField parsedField : parsedFields) {
            if (parsedField.isJoinField()) {
                parsedField.getField().setAccessible(true);
                try {
                    parsedField.getField().set(
                            instance,
                            initializeEntityWithJoin(resultSet, dialect, parsedField.getJoinField().getJoinFields())
                    );
                } catch (IllegalAccessException e) {
                    throw new DBException("Illegal access while setting field value", e);
                }
            }
        }
        return instance;
    }

    /**
     * Initializes an entity based on the provided database result set and parsed fields.
     *
     * @param <Entity>             the type of the entity to be initialized
     * @param resultSet            the result set from which data is read
     * @param fields               the list of parsed fields representing metadata and mappings for the entity
     * @param withUniqueIdentifier a flag indicating whether to use unique identifiers for field names during mapping
     * @return the initialized entity of type T
     * @throws DBException if an error occurs during initialization, such as missing handlers, database access issues,
     *                     or illegal field access
     */
    @SuppressWarnings("unchecked")
    public static <Entity> Entity initializeEntity(ResultSet resultSet,
                                                   IDBDialect dialect,
                                                   DBParsedFieldsList fields,
                                                   boolean withUniqueIdentifier) throws DBException {
        ReadableHandlers readableHandlers = dialect.getReadableHandlers();
        Object instance = createInstance(fields);
        for (DBParsedField entry : fields) {
            if (entry.isJoinField()) {
                continue;
            }
            DBSupportedTypes fieldType = entry.getFieldType();
            String dbFieldName = withUniqueIdentifier ? (fields.getUniqueIdentification() + "_" + entry.getDbFieldName()) : entry.getDbFieldName();
            Field field = entry.getField();
            field.setAccessible(true);
            ReadableHandler handler = readableHandlers.get(fieldType);
            if (handler == null) {
                throw new DBException("Handler for type " + fieldType + " not found " + entry);
            }
            try {
                handler.set(field, instance, resultSet, dbFieldName, entry);
            } catch (SQLException e) {
                throw new DBException("Error occurred while accessing the database", e);
            } catch (IllegalAccessException e) {
                throw new DBException("Illegal access while setting field value", e);
            } catch (IOException e) {
                throw new DBException("I/O error while setting field value", e);
            }
        }
        return (Entity) instance;
    }

    /**
     * Creates a new instance of the class associated with the provided DBParsedFieldsList.
     * The class must have a no-argument constructor.
     *
     * @param fields the parsed fields containing metadata about the class to instantiate
     * @return a new instance of the class associated with the provided fields
     * @throws DBException if the class cannot be instantiated due to missing constructors,
     *                     access violations, instantiation errors, or invocation errors
     */
    private static Object createInstance(DBParsedFieldsList fields) throws DBException {
        Object instance;
        Constructor<?> constructor;
        try {
            constructor = fields.getClazz().getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            throw new DBException("No such method found while trying to get declared constructor", e);
        }
        try {
            instance = constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw new DBException("Error occurred while invoking the constructor", e);
        } catch (InstantiationException e) {
            throw new DBException("Unable to create a new instance of the class", e);
        } catch (IllegalAccessException e) {
            throw new DBException("Illegal access while trying to instantiate the class", e);
        }
        return instance;
    }
}
