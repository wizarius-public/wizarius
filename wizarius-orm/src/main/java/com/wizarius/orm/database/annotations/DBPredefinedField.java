package com.wizarius.orm.database.annotations;

import com.wizarius.orm.database.entityreader.DBSupportedTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Vladyslav Shyshkin on 21.01.17.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DBPredefinedField {
    String fieldName();

    DBSupportedTypes type();
}
