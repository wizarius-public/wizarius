package com.wizarius.orm.database.connection;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Vladyslav Shyshkin on 09.04.17.
 */
@Getter
@AllArgsConstructor
public class ConnectionParameter {
    private final String key;
    private final String value;
}
