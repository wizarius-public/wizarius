package com.wizarius.orm.migrations;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
public class DBMigrationException extends Exception {
    public DBMigrationException() {
        super();
    }

    public DBMigrationException(String message) {
        super(message);
    }

    public DBMigrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBMigrationException(Throwable cause) {
        super(cause);
    }

    protected DBMigrationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
