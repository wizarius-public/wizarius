package com.wizarius.orm.migrations.savers;

import com.wizarius.orm.database.DBException;
import com.wizarius.orm.database.connection.DBConnection;
import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.migrations.DBMigrationException;
import com.wizarius.orm.migrations.entities.DBMigrationEntity;
import com.wizarius.orm.migrations.entities.DBMigrationEntityList;
import lombok.extern.slf4j.Slf4j;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-06-10
 * Time: 17:48
 */
@Slf4j
public class DBMigrationSaver implements IMigrationSaver {
    /**
     * Connection pool
     */
    private final DBConnectionPool pool;
    /**
     * Migration table name
     */
    private final String migrationTableName;

    /**
     * Initialize database saver
     *
     * @param pool database connection pool
     */
    public DBMigrationSaver(DBConnectionPool pool) {
        this.pool = pool;
        this.migrationTableName = "wizarius_orm_migrations";
    }

    /**
     * Initialize database saver
     *
     * @param pool               database connection pool
     * @param migrationTableName migration table name
     */
    public DBMigrationSaver(DBConnectionPool pool, String migrationTableName) {
        this.pool = pool;
        this.migrationTableName = migrationTableName;
    }

    /**
     * Save migration
     *
     * @param migration migration
     * @throws DBMigrationException on unable to save
     */
    @Override
    public void save(DBMigrationEntity migration) throws DBMigrationException {
        log.trace("Save database migration entity where level = " + migration.getLevel());
        try {
            try (DBConnection connection = pool.getConnection()) {
                PreparedStatement prepareStatement = connection.createPrepareStatement("INSERT INTO " + migrationTableName + " VALUES (?,?)");
                prepareStatement.setInt(1, migration.getLevel());
                prepareStatement.setString(2, migration.getName());
                prepareStatement.execute();
            }
        } catch (DBException | SQLException e) {
            throw new DBMigrationException("Unable to save database migration." + e.getMessage(), e);
        }
    }

    /**
     * Read migrations list
     *
     * @return read migrations list
     * @throws DBMigrationException on unable to read
     */
    @Override
    public DBMigrationEntityList read() throws DBMigrationException {
        log.trace("Read migrations from database");
        try {
            DBMigrationEntityList result = new DBMigrationEntityList();
            try (DBConnection connection = pool.getConnection()) {
                ResultSet rs = connection.executeSqlQuery("SELECT * FROM " + migrationTableName);
                while (rs.next()) {
                    int level = rs.getInt("level");
                    String name = rs.getString("name");
                    result.add(new DBMigrationEntity(level, name));
                }
            }
            return result;
        } catch (DBException | SQLException e) {
            throw new DBMigrationException("Unable to read from database. " + e.getMessage(), e);
        }
    }

    /**
     * Execute before migrations
     *
     * @throws DBMigrationException on unable to migrate
     */
    @Override
    public void beforeMigrations() throws DBMigrationException {
        log.trace("Create migration table for database");
        try (DBConnection connection = pool.getConnection()) {
            connection.executeQuery("create table if not exists " + migrationTableName + "\n" +
                    "(\n" +
                    "    level int not null unique,\n" +
                    "    name  varchar(500) default null\n" +
                    ");");
        } catch (DBException e) {
            throw new DBMigrationException("Unable to get connection . " + e.getMessage(), e);
        } catch (SQLException e) {
            throw new DBMigrationException("Unable to execute sql command . " + e.getMessage(), e);
        }
    }
}