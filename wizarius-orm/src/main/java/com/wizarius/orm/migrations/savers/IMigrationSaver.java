package com.wizarius.orm.migrations.savers;

import com.wizarius.orm.migrations.DBMigrationException;
import com.wizarius.orm.migrations.entities.DBMigrationEntity;
import com.wizarius.orm.migrations.entities.DBMigrationEntityList;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-19
 * Time: 11:02
 */
public interface IMigrationSaver {
    /**
     * Save migration
     *
     * @param migration migration
     * @throws DBMigrationException on unable to save
     */
    void save(DBMigrationEntity migration) throws DBMigrationException;

    /**
     * Read migrations list
     *
     * @return read migrations list
     * @throws DBMigrationException on unable to read
     */
    DBMigrationEntityList read() throws DBMigrationException;

    /**
     * Execute before migrations
     *
     * @throws DBMigrationException on unable to migrate
     */
    default void beforeMigrations() throws DBMigrationException {

    }
}
