package com.wizarius.orm.migrations.entities;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
@Slf4j
public class DBMigrationEntityList extends ArrayList<DBMigrationEntity> {
    private final HashMap<Integer, DBMigrationEntity> levelsMap = new HashMap<>();

    /**
     * Constructs an empty list with an initial capacity of ten.
     */
    public DBMigrationEntityList() {
    }

    /**
     * Constructs a list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     *
     * @param c the collection whose elements are to be placed into this list
     * @throws NullPointerException if the specified collection is null
     */
    public DBMigrationEntityList(Collection<? extends DBMigrationEntity> c) {
        this.addAll(c);
    }

    @Override
    public boolean add(DBMigrationEntity dbAbstractMigrationBean) {
        if (levelsMap.containsKey(dbAbstractMigrationBean.getLevel())) {
            log.error("Unable to add two migrations with level " + dbAbstractMigrationBean.getLevel());
            return false;
        }
        levelsMap.put(dbAbstractMigrationBean.getLevel(), dbAbstractMigrationBean);
        return super.add(dbAbstractMigrationBean);
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the
     * specified collection's Iterator.  The behavior of this operation is
     * undefined if the specified collection is modified while the operation
     * is in progress.  (This implies that the behavior of this call is
     * undefined if the specified collection is this list, and this
     * list is nonempty.)
     *
     * @param c collection containing elements to be added to this list
     * @return <tt>true</tt> if this list changed as a result of the call
     * @throws NullPointerException if the specified collection is null
     */
    @Override
    public boolean addAll(Collection<? extends DBMigrationEntity> c) {
        for (DBMigrationEntity entity : c) {
            add(entity);
        }
        return true;
    }

    /**
     * Get migration by level
     *
     * @param level migration level
     * @return migration bean
     */
    public DBMigrationEntity getByLevels(int level) {
        return levelsMap.get(level);
    }
}
