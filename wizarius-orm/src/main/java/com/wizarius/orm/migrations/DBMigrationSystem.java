package com.wizarius.orm.migrations;

import com.wizarius.orm.database.connection.DBConnectionPool;
import com.wizarius.orm.migrations.entities.DBMigrationEntity;
import com.wizarius.orm.migrations.entities.DBMigrationEntityList;
import com.wizarius.orm.migrations.savers.IMigrationSaver;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Vladyslav Shyshkin
 * Date: 2019-11-19
 * Time: 11:01
 */
@Slf4j
public class DBMigrationSystem {
    /**
     * Migration saver
     */
    private final IMigrationSaver saver;

    /**
     * Migrations list
     */
    private final DBMigrationList migrations;

    /**
     * Connection pool
     */
    private final DBConnectionPool pool;

    /**
     * Migration system
     *
     * @param pool       connection pool
     * @param migrations migrations list
     * @param saver      migration save
     */
    public DBMigrationSystem(DBConnectionPool pool,
                             DBMigrationList migrations,
                             IMigrationSaver saver) {
        this.saver = saver;
        this.migrations = migrations;
        this.pool = pool;
        this.setupPoolInMigrations();
    }

    /**
     * Setup pool in all migrations
     */
    private void setupPoolInMigrations() {
        for (DBMigration migration : migrations) {
            migration.setupConnectionPool(pool);
        }
    }

    /**
     * Setup migration system
     */
    public void setup() throws DBMigrationException {
        log.info("Start migrations setup");
        saver.beforeMigrations();
        log.info("Read current migrations");
        //get all record from dump file
        DBMigrationEntityList entities = saver.read();
        for (DBMigration migration : migrations) {
            if (entities.getByLevels(migration.getLevel()) == null) {
                log.info(migration.getClass().getCanonicalName() + " : " + "UP NEW MIGRATION WITH LEVEL " + migration.getLevel() + " AND NAME " + migration.getName());
                migration.up();
                migration.execute();
                saver.save(new DBMigrationEntity(migration.getLevel(), migration.getName()));
            } else {
                log.info(migration.getClass().getCanonicalName() + " : " + "SKIP MIGRATION WITH LEVEL " + migration.getLevel() + " - ALREADY EXIST");
            }
        }
    }

    /**
     * Read saved migrations
     *
     * @return save migrations
     * @throws DBMigrationException on unable to get saved migrations
     */
    public DBMigrationEntityList getSavedMigrations() throws DBMigrationException {
        return saver.read();
    }
}
