package com.wizarius.orm.migrations;


import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vladyslav Shyshkin on 24.11.2017.
 */
@Slf4j
public class DBMigrationList extends ArrayList<DBMigration> {
    private Map<Integer, DBMigration> levelsMap = new HashMap<>();

    @Override
    public boolean add(DBMigration dbMigration) {
        if (levelsMap.containsKey(dbMigration.getLevel())) {
            log.error("Unable to add two migrations with level " + dbMigration.getLevel());
            return false;
        }
        levelsMap.put(dbMigration.getLevel(), dbMigration);
        return super.add(dbMigration);
    }

    /**
     * Get last migration
     *
     * @return abstract migration bean
     */
    public DBMigration getLastMigration() {
        if (this.size() == 0) {
            return null;
        }
        return this.get(this.size() - 1);
    }

    /**
     * Get migration by level
     *
     * @param level migration level
     * @return migration bean
     */
    public DBMigration getByLevels(int level) {
        return levelsMap.get(level);
    }

    /**
     * Get new migration
     *
     * @param level last migration
     * @return list of new migrations
     */
    public DBMigrationList getNew(int level) {
        DBMigrationList list = new DBMigrationList();
        for (DBMigration dbMigration : this) {
            if (dbMigration.getLevel() > level) {
                list.add(dbMigration);
            }
        }
        return list;
    }
}
