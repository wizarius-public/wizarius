package com.wizarius.orm.utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladyslav Shyshkin
 * Date: 25.09.2019
 * Time: 15:32
 * <p>
 * final List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7);
 * <p>
 * System.out.println(Partition.ofSize(numbers, 3));
 * System.out.println(Partition.ofSize(numbers, 2));
 * <p>
 * [[1, 2, 3], [4, 5, 6], [7]]
 * [[1, 2], [3, 4], [5, 6], [7]]
 * <p>
 * https://e.printstacktrace.blog/divide-a-list-to-lists-of-n-size-in-Java-8/
 */
public class WizArrayPartition<T> extends AbstractList<List<T>> {
    private final List<T> list;
    private final int chunkSize;

    public WizArrayPartition(List<T> list, int chunkSize) {
        this.list = new ArrayList<>(list);
        this.chunkSize = chunkSize;
    }

    /**
     * Split array to partition of size
     *
     * @param list      source list
     * @param chunkSize max count elements in partition array
     * @param <T>       list
     * @return partitions
     */
    public static <T> WizArrayPartition<T> ofSize(List<T> list, int chunkSize) {
        return new WizArrayPartition<>(list, chunkSize);
    }

    /**
     * Get partition by index
     *
     * @param index index of partition
     * @return partition list
     */
    @Override
    public List<T> get(int index) {
        int start = index * chunkSize;
        int end = Math.min(start + chunkSize, list.size());
        if (start > end) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of the list range <0," + (size() - 1) + ">");
        }
        return new ArrayList<>(list.subList(start, end));
    }

    /**
     * Size of partition
     *
     * @return partition size
     */
    @Override
    public int size() {
        return (int) Math.ceil((double) list.size() / (double) chunkSize);
    }
}
