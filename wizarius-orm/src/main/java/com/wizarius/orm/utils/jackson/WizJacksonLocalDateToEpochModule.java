package com.wizarius.orm.utils.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class WizJacksonLocalDateToEpochModule extends SimpleModule {
    public WizJacksonLocalDateToEpochModule() {
        addSerializer(LocalDate.class, new WizLocalDateToEpochMillisSerializer());
        addSerializer(LocalDateTime.class, new WizLocalDateTimeToEpochMillisSerializer());
        addDeserializer(LocalDate.class, new WizLocalDateFromEpochMillisDeserializer());
        addDeserializer(LocalDateTime.class, new WizLocalDateTimeFromEpochMillisDeserializer());
    }
}
