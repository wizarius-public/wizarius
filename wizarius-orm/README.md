# Wizarius ORM
Wizarius ORM - это Java библиотека, обеспечивающая преобразование описанных объектов 
с помощью анотаций в SQL запрос.

Поддерживается следующее базы данных MySQL и Postgres.


## Initialization
Wizarius ORM поддерживает несколько вариантов инитиализации пула подключений


#### Spring Bean
Пример конфигурационного файла для MySQL:
```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--MAIN START CONFIG-->
    <bean id="start_config" class="com.wizarius.orm.database.connection.DBConnectionPool">
        <constructor-arg name="driver" ref="database_driver"/>
        <constructor-arg name="clazz" value="com.wizarius.orm.database.mysql.MysqlConnection"/>
    </bean>
    <!--END MAIN START CONFIG-->

    <!--DB DRIVER-->
    <bean id="database_driver" class="com.wizarius.orm.database.mysql.driver.MysqlSpringDriver">
        <constructor-arg name="driver" value="com.mysql.cj.jdbc.Driver"/>
        <constructor-arg name="username" value="root"/>
        <constructor-arg name="password" value="12345"/>
        <constructor-arg name="databaseName" value="wizarius_orm"/>
        <constructor-arg name="hostname" value="127.0.0.1"/>
        <constructor-arg name="port" value="3306"/>
        <constructor-arg name="params">
            <bean class="com.wizarius.orm.database.connection.ConnectionParametersList">
                <constructor-arg>
                    <list>
                        <ref bean="useUnicode"/>
                        <ref bean="characterEncoding"/>
                        <ref bean="useSSL"/>
                    </list>
                </constructor-arg>
            </bean>
        </constructor-arg>
    </bean>

    <bean id="useUnicode" class="com.wizarius.orm.database.connection.ConnectionParameter">
        <constructor-arg name="key" value="useUnicode"/>
        <constructor-arg name="value" value="true"/>
    </bean>
    <bean id="characterEncoding" class="com.wizarius.orm.database.connection.ConnectionParameter">
        <constructor-arg name="key" value="characterEncoding"/>
        <constructor-arg name="value" value="utf-8"/>
    </bean>
    <bean id="useSSL" class="com.wizarius.orm.database.connection.ConnectionParameter">
        <constructor-arg name="key" value="useSSL"/>
        <constructor-arg name="value" value="false"/>
    </bean>
    <!--END DB DRIVER-->
</beans>
```
Пример конфигурационного файла для Postgres:
```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--MAIN START CONFIG-->
    <bean id="start_config" class="com.wizarius.orm.database.connection.DBConnectionPool">
        <constructor-arg name="driver" ref="database_driver"/>
        <constructor-arg name="clazz" value="com.wizarius.orm.database.postgres.PostgresConnection"/>
    </bean>
    <!--END MAIN START CONFIG-->

    <!--DB DRIVER-->
    <bean id="database_driver" class="com.wizarius.orm.database.postgres.driver.PostgresSpringDriver">
        <constructor-arg name="driver" value="org.postgresql.Driver"/>
        <constructor-arg name="username" value="postgres"/>
        <constructor-arg name="password" value="12345"/>
        <constructor-arg name="databaseName" value="wizarius_orm"/>
        <constructor-arg name="hostname" value="127.0.0.1"/>
        <constructor-arg name="port" value="5432"/>
        <constructor-arg name="params">
            <null/>
        </constructor-arg>
    </bean>
    <!--END DB DRIVER-->
</beans>
```


#### Properties file
Пример конфигурационного файла для MySQL (mysql_database.properties):
```
jdbc.drivers=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/wizarius_orm?useUnicode=true&characterEncoding=utf-8&useSSL=false 
jdbc.username=root
jdbc.password=12345
```
Пример конфигурационного файла для Postgres SQL (postgres_database.properties):
```
jdbc.drivers=org.postgresql.Driver
jdbc.url=jdbc:postgresql://127.0.0.1:5432/wizarius_orm
jdbc.username=postgres
jdbc.password=12345
```

Чтобы открыть подключение необходимо выполнить:
```
try (MysqlPropertiesDriver connect = new MysqlPropertiesDriver()) {
    connect.openConnection();
} catch (IOException e) {
    throw new DBException(e);
}
```
Вы так-же можете использовать PostgresPropertiesDriver

**Не рекомендуется использовать постоянное открытие подключения, для более стабильной работы используйте Connection Pool**


#### Иницилазиация Connection Pool
Для использования Connection Pool в MySQL необходимо выполнить следующих код:
```
MysqlPropertiesDriver dbConnection = new MysqlPropertiesDriver();
DBConnectionPool mysqlConnection = new DBConnectionPool(dbConnection, MysqlConnection.class);
```
Для использования Connection Pool в Postgres SQL необходимо выполнить следующих код:
```
PostgresPropertiesDriver dbConnection = new PostgresPropertiesDriver();
DBConnectionPool postgresConnection = new DBConnectionPool(dbConnection, PostgresConnection.class);
```
Есть возможность иницилазирировать Connection Pool с помощью Spring Bean
```
ApplicationContext context = ApplicationSpringContext.getFromEmbedSource("/etc/mysql.xml");
ConnectionDriver driver = (ConnectionDriver) context.getBean("database_driver");
DBConnectionPool mysqlConnection = new DBConnectionPool(driver, MysqlConnection.class);      
```
## Migrations
Wizarius ORM имеет собственную систему миграций. 
```
DBMigrationSystem system = new DBMigrationSystem(mysqlConnection, new MysqlSystemMigrationsList(), "mysql.wizariusDBMigration");
```
расширениее файла *.wizariusDBMigration обязательно.

Для запуска миграции необходимо выполнить. Миграция которая произошла будет автоматически пропущена.
```
system.setup();
```
Migration List выглядит следующим образом:
```
public class MysqlSystemMigrationsList extends DBMigrationsList {
    public MysqlSystemMigrationsList() {
        add(new MysqlInitialMigration(1, "Initial migrations"));
    }
}
```
Bean для миграции должен наследовать класс DBAbstractMigrationBean:
```
public class MysqlInitialMigration extends DBAbstractMigrationBean {
    public MysqlInitialMigration(int level, String name) {
        super(level, name);
    }

    @Override
    public void up() throws DBMigrationException {
    }

    @Override
    public void down() throws DBMigrationException {
    }
}
```
При инициализации миграции вызывается метод up(), если SQL запрос не удалось выполнить, будет вызван метод down().
Рекомендуеться создавать по одному запросу в одной миграции.

Чтобы выполнить миграцию необходимо написать следующий код в методе up():
```
executeSQL("SQL TEXT");
```
При успешной миграции в файл *.wizariusDBMigration добавится JSON статус миграции. Если миграций еще не было, файл создаться автоматически.
```
[{
  "level" : 1,
  "name" : "Initial migration"
}]
```


## Code Examples
Для выполнения тестов мы будем использовать класс User, имеющий следующую структуру:
```
@DBModel(tableName = "users")
public class User {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBField(fieldName = "login")
    private String login;

    @DBField(fieldName = "password")
    private String password;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
```
**Класс должен иметь конструктор по умолчанию (конструктор без аргументов).**[Подробнее о конструкторе по умолчанию](https://en.wikipedia.org/wiki/Default_constructor)

Для работы с обьектом необходимо его преобразовать следующим образом
```
MysqlEntityInitializer mysqlSession = new MysqlEntityInitializer(User.class, mysqlConnection);
```
Для Postgres Sql
```
PostgresEntityInitializer postgresSession = new PostgresEntityInitializer(User.class, new DBConnectionPool(driver, PostgresConnection.class));                        
```


#### Select queries
Чтобы выполнить SQL запрос напрямую необходимо выполнить:
```
try (AbstractConnection connection = mysqlSession.getConnectionPool().getConnection()) {
    ResultSet resultSet = connection.executeSqlQuery("Select count(1) from users");
    resultSet.next();
    long count = resultSet.getLong(1);
    assertTrue(count == 1);
} catch (SQLException e) {
    log.trace("Unable to execute query " + e.getMessage(),e);
}
```
Пример запроса с where:
```
ArrayList<DBEntity> usersList = session.select.where("id", user1.getId()).execute();
```
Чтобы получить одну запись:
```
User user = (User) session.select.where("id", 1).getOne();
```
Получить количество записей в таблице можно следующим образом:
```
long count = session.select.getCount();
```
Добавить Order в запрос:
```
session.select.orderBy("id", OrderTypes.DESC).execute();
```
Wizarius ORM уммет выполнять агрегирующие фунции:
```
session.select
       .addAggregate(new AggregateField("id", "users", AggregateFunctionTypes.MAX))
       .getOne();
```
Добавить Group by в запрос можно следующим образом:
```
session.select
    .addAggregate(new AggregateField("id", "users", AggregateFunctionTypes.MAX))
    .addAggregate(new AggregateField("login", "users", AggregateFunctionTypes.MAX))
    .groupBy("password")
    .execute();
```
Wizarius ORM умеет строить Join запроссы, для этого необходимо добавить следующую аннотацию:
```
@DBJoinModel(parentField = "id", childField = "id_t1")
private JoinT2 table2;
```
Объект JoinT2 имеет следующую структуру:
```
@DBModel(tableName = "join_t2")
public class JoinT2 {
    @DBField(fieldName = "id", isAutoIncrement = true)
    private int id;

    @DBField(fieldName = "id_t1")
    private int id_t1;

    @DBField(fieldName = "name")
    private String name;

    public JoinT2() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_t1() {
        return id_t1;
    }

    public void setId_t1(int id_t1) {
        this.id_t1 = id_t1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "JoinT2{" +
                "id=" + id +
                ", id_t1=" + id_t1 +
                ", name='" + name + '\'' +
                '}';
    }
}
```
Для более удобного использования ORM рекомендуеться создавать хранилища (Storage).
```
public class UsersStorage extends DatabaseStorage<User> {
    public UsersStorage(DBConnectionPool pool) throws DBException {
        super(pool, User.class);
    }
}
```
Теперь нам доступны стандартные функции для работы с объектом User:
```
deleteByID(long id);
getAll();
clear();
getSession();
getByID(long id);
void update(T entity, long id);
long saveAndGetLastInsertID(T entity);
save(T entity);
```

#### Insert queries
Для вставки объекта в таблицу необходимо выполнить:
```
User user = new User();
user.setLogin("login");
user.setPassword("password");
long id = postgresSession.insert.executeQueryWithKey(user);      
```


#### Delete queries
Удалить все записи в таблице:
```
session.delete.execute();
```
Удалить пользователя с ID:
```
session.delete.where("id", 1).execute();
```


#### Update queries
Обновить объект типа User можно следующим образом
```
user.setPassword("NEW PASSWORD");
session.update.where("id", user.getId()).execute(user);
```
**Если не написать where условие, произойдет обновление всех полей внутри таблицы.**